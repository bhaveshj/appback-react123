import React from 'react';
import ReactDOM from 'react-dom'
import App from './components/Main';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import { AbModelReducer } from 'appback-react'
import thunk from 'redux-thunk'
import 'semantic-ui-css/semantic.css'

const initialState = {};
const middleware = [thunk]
const reducers = combineReducers({ abModels: AbModelReducer })
const enhancers = []
const devToolsExtension = window.devToolsExtension
if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
}

const store = createStore(
  reducers,
  initialState,
  compose(
     applyMiddleware(...middleware),
     ...enhancers
  )
)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
