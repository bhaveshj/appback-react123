exports.geoData = [
    { value: 'australian-capital-territory', label: 'Australian Capital Territory', className: 'State-ACT', countryName: "AU" },
    { value: 'new-south-wales', label: 'New South Wales', className: 'State-NSW', countryName: "AU" },
    { value: 'victoria', label: 'Victoria', className: 'State-Vic', countryName: "AU" },
    { value: 'queensland', label: 'Queensland', className: 'State-Qld', countryName: "AU" },
    { value: 'western-australia', label: 'Western Australia', className: 'State-WA', countryName: "AU" },
    { value: 'south-australia', label: 'South Australia', className: 'State-SA', countryName: "AU" },
    { value: 'tasmania', label: 'Tasmania', className: 'State-Tas', countryName: "AU" },
    { value: 'northern-territory', label: 'Northern Territory', className: 'State-NT', countryName: "AU" },
    { value: 'AL', label: 'Alabama', disabled: true, countryName: "US" },
    { value: 'AK', label: 'Alaska', countryName: "US" },
    { value: 'AS', label: 'American Samoa', countryName: "US" },
    { value: 'AZ', label: 'Arizona', countryName: "US" },
    { value: 'AR', label: 'Arkansas', countryName: "US" },
    { value: 'CA', label: 'California', countryName: "US" },
    { value: 'CO', label: 'Colorado', countryName: "US" },
    { value: 'CT', label: 'Connecticut', countryName: "US" },
    { value: 'DE', label: 'Delaware', countryName: "US" },
    { value: 'DC', label: 'District Of Columbia', countryName: "US" },
    { value: 'FM', label: 'Federated States Of Micronesia', countryName: "US" },
    { value: 'FL', label: 'Florida', countryName: "US" },
    { value: 'GA', label: 'Georgia', countryName: "US" },
    { value: 'GU', label: 'Guam', countryName: "US" },
    { value: 'HI', label: 'Hawaii', countryName: "US" },
    { value: 'ID', label: 'Idaho', countryName: "US" },
    { value: 'IL', label: 'Illinois', countryName: "US" },
    { value: 'IN', label: 'Indiana', countryName: "US" },
    { value: 'IA', label: 'Iowa', countryName: "US" },
    { value: 'KS', label: 'Kansas', countryName: "US" },
    { value: 'KY', label: 'Kentucky', countryName: "US" },
    { value: 'LA', label: 'Louisiana', countryName: "US" },
    { value: 'ME', label: 'Maine', countryName: "US" },
    { value: 'MH', label: 'Marshall Islands', countryName: "US" },
    { value: 'MD', label: 'Maryland', countryName: "US" },
    { value: 'MA', label: 'Massachusetts', countryName: "US" },
    { value: 'MI', label: 'Michigan', countryName: "US" },
    { value: 'MN', label: 'Minnesota', countryName: "US" },
    { value: 'MS', label: 'Mississippi', countryName: "US" },
    { value: 'MO', label: 'Missouri', countryName: "US" },
    { value: 'MT', label: 'Montana', countryName: "US" },
    { value: 'NE', label: 'Nebraska', countryName: "US" },
    { value: 'NV', label: 'Nevada', countryName: "US" },
    { value: 'NH', label: 'New Hampshire', countryName: "US" },
    { value: 'NJ', label: 'New Jersey', countryName: "US" },
    { value: 'NM', label: 'New Mexico', countryName: "US" },
    { value: 'NY', label: 'New York', countryName: "US" },
    { value: 'NC', label: 'North Carolina', countryName: "US" },
    { value: 'ND', label: 'North Dakota', countryName: "US" },
    { value: 'MP', label: 'Northern Mariana Islands', countryName: "US" },
    { value: 'OH', label: 'Ohio', countryName: "US" },
    { value: 'OK', label: 'Oklahoma', countryName: "US" },
    { value: 'OR', label: 'Oregon', countryName: "US" },
    { value: 'PW', label: 'Palau', countryName: "US" },
    { value: 'PA', label: 'Pennsylvania', countryName: "US" },
    { value: 'PR', label: 'Puerto Rico', countryName: "US" },
    { value: 'RI', label: 'Rhode Island', countryName: "US" },
    { value: 'SC', label: 'South Carolina', countryName: "US" },
    { value: 'SD', label: 'South Dakota', countryName: "US" },
    { value: 'TN', label: 'Tennessee', countryName: "US" },
    { value: 'TX', label: 'Texas', countryName: "US" },
    { value: 'UT', label: 'Utah', countryName: "US" },
    { value: 'VT', label: 'Vermont', countryName: "US" },
    { value: 'VI', label: 'Virgin Islands', countryName: "US" },
    { value: 'VA', label: 'Virginia', countryName: "US" },
    { value: 'WA', label: 'Washington', countryName: "US" },
    { value: 'WV', label: 'West Virginia', countryName: "US" },
    { value: 'WI', label: 'Wisconsin', countryName: "US" },
    { value: 'WY', label: 'Wyoming', countryName: "US" }
]

exports.companies=[
  {
    "logo": "",
    "name": "Real Estate Back Office",
    "updatedAt": "2017-03-01T10:37:43.006Z",
    "branches": [
      {
        "name": "dept 1",
        "branchHead": "58b6a47aea310832c8e93b8f",
        "createdBy": "58b6a478ea310832c8e93b89",
        "companyId": "58b6a477ea310832c8e93b84",
        "id": "58b7bbc31ad6d814e23915f8",
        "createdAt": "2017-03-01T10:37:47.757Z",
        "updatedAt": "2017-03-01T10:37:47.757Z"
      },
      {
        "name": "dept 2",
        "branchHead": "58b6a47aea310832c8e93b8f",
        "createdBy": "58b6a478ea310832c8e93b89",
        "companyId": "58b6a477ea310832c8e93b84",
        "id": "58b7bbc31ad6d814e23915f9",
        "createdAt": "2017-03-01T10:37:47.759Z",
        "updatedAt": "2017-03-01T10:37:47.759Z"
      }
    ],
    "defaultPhoneNumber": {
      "id": "19d9d1c0-fe6b-11e6-a2d9-bdcd1b676a49",
      "phoneNumber": "+10000000001",
      "ext": 1,
      "type": "WORK",
      "default": true,
      "createdAt": "2017-03-01T10:37:43.003Z",
      "updatedAt": "2017-03-01T10:37:43.003Z"
    },
    "emails": [
      {
        "id": "19d9f8d0-fe6b-11e6-a2d9-bdcd1b676a49",
        "email": "company1@company1.com",
        "default": true,
        "createdAt": "2017-03-01T10:37:43.005Z",
        "updatedAt": "2017-03-01T10:37:43.005Z"
      }
    ],
    "id": "58b7bbb81ad6d814e23915dc",
    "createdAt": "2017-03-01T10:37:43.005Z",
    "defaultEmail": {
      "id": "19d9f8d0-fe6b-11e6-a2d9-bdcd1b676a49",
      "email": "company1@company1.com",
      "default": true,
      "createdAt": "2017-03-01T10:37:43.005Z",
      "updatedAt": "2017-03-01T10:37:43.005Z"
    },
    "phoneNumbers": [
      {
        "id": "19d9d1c0-fe6b-11e6-a2d9-bdcd1b676a49",
        "phoneNumber": "+10000000001",
        "ext": 1,
        "type": "WORK",
        "default": true,
        "createdAt": "2017-03-01T10:37:43.003Z",
        "updatedAt": "2017-03-01T10:37:43.003Z"
      }
    ]
  },
  {
    "logo": "",
    "name": "Cornerstone Investments",
    "updatedAt": "2017-03-01T10:37:43.032Z",
    "branches": [],
    "defaultPhoneNumber": {
      "id": "19ddf070-fe6b-11e6-a2d9-bdcd1b676a49",
      "phoneNumber": "+10000000002",
      "ext": 2,
      "type": "WORK",
      "default": true,
      "createdAt": "2017-03-01T10:37:43.031Z",
      "updatedAt": "2017-03-01T10:37:43.031Z"
    },
    "emails": [
      {
        "id": "19de1780-fe6b-11e6-a2d9-bdcd1b676a49",
        "email": "company2@company2.com",
        "default": true,
        "createdAt": "2017-03-01T10:37:43.032Z",
        "updatedAt": "2017-03-01T10:37:43.032Z"
      }
    ],
    "id": "58b7bbb81ad6d814e23915dd",
    "createdAt": "2017-03-01T10:37:43.032Z",
    "defaultEmail": {
      "id": "19de1780-fe6b-11e6-a2d9-bdcd1b676a49",
      "email": "company2@company2.com",
      "default": true,
      "createdAt": "2017-03-01T10:37:43.032Z",
      "updatedAt": "2017-03-01T10:37:43.032Z"
    },
    "phoneNumbers": [
      {
        "id": "19ddf070-fe6b-11e6-a2d9-bdcd1b676a49",
        "phoneNumber": "+10000000002",
        "ext": 2,
        "type": "WORK",
        "default": true,
        "createdAt": "2017-03-01T10:37:43.031Z",
        "updatedAt": "2017-03-01T10:37:43.031Z"
      }
    ]
  }
]