import React, { Component } from 'react';
import AppbackApi from "appback-api";
import { AbModelConstants, AbModelForm } from 'appback-react'
import { Segment, Container, Grid } from 'semantic-ui-react'
import { geoData } from "../data.js";
const { ACTIONBUTTON, ABCHECKBOXTYPE, ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_CONDITION_ACTION, FORM_CONDITION_TYPE } = AbModelConstants;

class AbFormWithArray extends Component {
  constructor(props) {
    super(props);
  }

  submitButtonClick() {
    this.refs.myModelForm.getWrappedInstance().submitFormClick();
  }

  cancelButtonClick() {
    this.refs.myModelForm.getWrappedInstance().cancelFormClick();
  }

  formErrorCallback(action, error) {
    console.log("error From form action", action);
    console.log("error From form error", error);
  }
  onFormChange(data) {
    console.log("form data change event callback from AbModelForm", data);
  }

  render() {
    let self = this;
    const { appbackApi } = this.props;
    let isLoggedIn = appbackApi.isLoggedIn();

    const actionButtonHandler = (event, rowData) => {
      console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
    }

    const saveSuccessCallBack = (event, data) => {
      console.log("save and edit callback data EVENT", event);
      console.log("save and edit callback data DATA", data);
    }

    const schema = {
      title: "TodoLists",
      type: "object",
      properties: {
        name: {
          type: "string",
          title: "Task Name",
          "minLength": 5,
          "required": true,
          "errorMsgs": {
            "required": "Task name is required",
            "minLength": "Please enter more than 5 character."
          }
        },
        description: {
          type: "string",
          title: "Task Description",
          "required": true,
          "errorMsgs": {
            "required": "Task description is required"
          }
        },
        userId: { type: "string", title: "Task By User" },
        phoneNumbers: {
          "type": "array",
          "title": "Phone Numbers",
          "compact": true,     // For compact version of array list. Don't give title in this case
          "items": {
            "type": "object",
            "segmentTitle":"JUMPBYTE",
            "required": [

            ],
            "properties": {
              "phoneNumber": {
                "type": "string",
                "title": " "
              },
              "ext": {
                "type": "number",
                "title": " "
              },
              "type": {
                "type": "string",
                "title": " "
              },
              "default": {
                "type": "boolean",
                "title": " ",
                "default": false
              }
            }
          }
        }
      }
    };

    const uiSchema = {
      userId: {    //it will load user list from database(prefilled from database when it load) To make it auto complete set async option to true
        "ui:widget": "AbSyncSelect",
        "ui:options": {
          optionModel: "todousers",
          labelKey: "email",
          valueKey: "id",
          searchable: true,
          selectPlaceholder: "Select User..",
          noResultsText: "No User found",
          async: true,
          appbackApi: appbackApi,
          customFilter: { "disabled": false },
          matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
          filterOnFields: ["email", "id"],
          cache: false,
        }
      },
      phoneNumbers: {
        "ui:options": {
          "addable": true,        //for add or remove add button(which is adding new item in list)
          "orderable": true,     //for add or remove ordering(up and down) buttons
          "removable": true   //for add or remove delete button from list
        },
        "items": {
          "default": {
            "ui:widget": "AbCheckbox",
            "ui:options": {
              "label": "Default"
            }
          }
        }
      }
    }
    return (
      <div>
        <Segment color='red'>
          <h3>EXAMPLE INCLUDES:</h3>
          <ul>
            <li>We can have mutliple phone numbers</li>
            <li> You can control visbility of add, order & remove button, Provide below options in <b>ui:options</b></li>
            <ol>
                <li><b>addable</b>:true or false</li>
                <li><b>orderable</b>:true or false</li>
                <li><b>removable</b>:true or false</li>
            </ol>
            <li>You can set custom text for segement in array fields </ li>
          </ul>
        </Segment>
        <Segment color="red">
          <AbModelForm modelName="TodoLists" ref="myModelForm" schema={schema} uiSchema={uiSchema} appbackApi={appbackApi} showDefaultButtons={false} saveCallback={saveSuccessCallBack} errorCallback={self.formErrorCallback} onformDataChange={(data) => self.onFormChange(data)} />
        </Segment>
      </div>
    );

  }
}

export default AbFormWithArray