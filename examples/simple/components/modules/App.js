import React, { Component } from 'react';
import FormWithWidget from '../FormComponent'
import _ from 'underscore';
import AppbackApi from "appback-api";
import { AbModelGrid, AbModelConstants, AbModelForm } from 'appback-react'
import { Segment, Container, Grid } from 'semantic-ui-react'
import { geoData } from "../data.js";
// import Select from 'react-select';
// import 'react-select/dist/react-select.css';
const { COLUMNTYPE, ACTIONBUTTON, ABCHECKBOXTYPE, ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_CONDITION_ACTION, FORM_CONDITION_TYPE, SELECT_MULTI_OPTIONS } = AbModelConstants;

export default class FullApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appbackApi: new AppbackApi('http://localhost:4000/api'),
      showTodoUser: false
    }
  }

  submitButtonClick() {
    this.refs.myModelForm.getWrappedInstance().submitFormClick();
  }

  cancelButtonClick() {
    this.refs.myModelForm.getWrappedInstance().cancelFormClick();
  }
  showAbUserComp() {
    this.setState({ showTodoUser: true });
  }

  errorCallback(error) {
    console.log("Error from component", error);
  }

  deleteCallback(data) {
    console.log("delete callback from grid", data);
  }

  formErrorCallback(action, error) {
    console.log("error From form action", action);
    console.log("error From form error", error);
  }
  onFormChange(data) {
    console.log("form data change event callback from AbModelForm", data);
  }

  render() {
    const country = [
      { value: 'AU', label: "Australia" },
      { value: 'US', label: "United States of America" },
    ]
    let self = this;
    const state = [
      { value: 'australian-capital-territory', label: 'Australian Capital Territory', className: 'State-ACT' },
      { value: 'new-south-wales', label: 'New South Wales', className: 'State-NSW' },
      { value: 'victoria', label: 'Victoria', className: 'State-Vic' },
      { value: 'queensland', label: 'Queensland', className: 'State-Qld' },
      { value: 'western-australia', label: 'Western Australia', className: 'State-WA' },
      { value: 'south-australia', label: 'South Australia', className: 'State-SA' },
      { value: 'tasmania', label: 'Tasmania', className: 'State-Tas' },
      { value: 'northern-territory', label: 'Northern Territory', className: 'State-NT' },
    ]
    const { appbackApi} = this.state;
    let isLoggedIn = appbackApi.isLoggedIn();
    if (!isLoggedIn) {
      appbackApi.login({ "email": "admin@appback.com", "password": "admin@appback" }).
        then(() => {
          isLoggedIn = true;
          alert("login success")
        });
    }
    if (isLoggedIn) {
      const actionButtonHandler = (event, rowData) => {
        console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
        // alert("Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
      }
      const saveSuccessCallBack = (event, data) => {
        console.log("save and edit callback data EVENT", event);
        console.log("save and edit callback data DATA", data);
      }
      const renderValue = (option) => {
        return (<div><strong >{option.name}</strong>-{option.description}</div>)
      }
      let todoColumnMetaData = [
        {
          "columnName": "name",
          "order": 1,
          "locked": false,
          "visible": true,
          "displayName": "Task Name",
          "filter": true,
          "advanceFilter": true,
          "advanceFilterOptions": {
            fieldType: "string",
            multiSelect: false,
            clearable: false,
            valueKey: "value",
            labelKey: "label",
            placeholder: "Transaction Type",
            dataSet: [
              {
                value: "Type A",
                label: "Type A"
              },
              {
                value: "Type B",
                label: "Type B"
              },
              {
                value: "Type C",
                label: "Type C"
              },
              {
                value: "Type D",
                label: "Type D"
              }
            ]
          }
        },
        {
          "columnName": "description",
          "order": 2,
          "locked": false,
          "visible": true,
          "displayName": "Description",
          "advanceFilter": true,
          "advanceFilterOptions": {
            fieldType: "string",
            multiSelect: false,
            clearable: false,
            placeholder: "Transaction Status",
            valueKey: "value",
            labelKey: "label",
            value: "OPEN",
            dataSet: [
              {
                value: "OPEN",
                label: "OPEN"
              },
              {
                value: "CLOSE",
                label: "CLOSE"
              }
            ]
          }
        },
        {
          "columnName": "createdAt",
          "order": 4,
          "locked": false,
          "visible": true,
          "displayName": "Created At",
          "type": COLUMNTYPE.DATE,
          "typeOptions": {
            "format": "DD MMM YYYY hh:mm:ss A" //format is from moment js, url: http://momentjs.com/docs/#/parsing/string-format/ 
          }
        },
        {
          "columnName": "updatedAt",
          "order": 5,
          "locked": true,
          "visible": true,
          "sortable": false,
          "displayName": "Operation",
          "type": COLUMNTYPE.ACTIONS,
          "typeOptions": {
            "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete'],
            // "deleteConfirmModalOptions":{
            //   "header":"Please Confirm",
            //   "content":"Are sure you want to delete?",
            //   "confirmButton":{
            //     text:"Yes",
            //     icon:"checkmark",
            //     callback:(data)=>console.log("confirm callback from grid metaData",data)
            //   },
            //   "cancelButton":{
            //     text:"No",
            //     icon:"remove",
            //     callback:(data)=>console.log("cancel callback from grid metaData",data)
            //   }
            // },
            "clickHandler": actionButtonHandler,
            "buttons": [
              {
                "label": "Archive",
                "key": "archive",
                "icon": "archive"
              }
            ]
          }
        },
        {
          "columnName": "todouser.email",
          "order": 3,
          "locked": true,
          "visible": true,
          "sortable": false,
          "displayName": "Task By User",
          "includeModel": "todouser",
          "type": COLUMNTYPE.LINK,
          "typeOptions": {
            "path": "/todouser" //make it path
          }
        }
      ]
      let todoUserColumnMetaData = [
        {
          "columnName": "email",
          "order": 1,
          "locked": false,
          "visible": true,
          "displayName": "Model Name",
          "filter": true
        },
        {
          "columnName": "updatedAt",
          "order": 2,
          "locked": true,
          "visible": true,
          "sortable": false,
          "displayName": "Operation",
          "type": COLUMNTYPE.ACTIONS,
          "typeOptions": {
            "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete']
            "clickHandler": actionButtonHandler,
            "buttons": [
              {
                "label": "Archive",
                "key": "archive",
                "icon": "archive"
              }
            ]
          }
        }
      ]
      const schema = {
        title: "TodoLists",
        type: "object",
        // required: ["stateName"],
        properties: {
          name: {
            type: "string",
            title: "Task Name",
            "minLength": 5,
            "required": true,
            "errorMsgs": {
              "required": "Task name is required",
              "minLength": "Please enter more than 5 character."
            }
          },
          description: {
            type: "string",
            title: "Task Description",
            "required": true,
            "errorMsgs": {
              "required": "Task description is required"
            }
          },
          userId: { type: "string", title: "Task By User" },
          disabledToggle: {
            type: "boolean",
            title: " ",
            default: true
          },
          sliderCheckbox: {
            type: "boolean",
            title: " ",
            default: false,
          },
          normalCheckbox: {
            type: "boolean",
            title: " ",
            default: false,
          },
          "multipleChoicesList": {
            "type": "array",
            "title": "A multiple choices list",
            "required": true,
            "errorMsgs": {
              "required": "Please select atlease one option"
            },
            "condition": {
              type: FORM_CONDITION_TYPE.VISIBILITY, //condition to disable or hide this control based on "normalCheckbox" field value
              dependentField: "normalCheckbox", //Field name that we given in form schema on which our field is dependent for action
              valueCheck: true, //value that we will be comparing with dependent field value for perform action on this field
              action: FORM_CONDITION_ACTION.DISABLED, //DISABLED: will disable field, HIDDEN: will hide field
            },
            "items": {
              "type": "string",
              "enum": [
                "foo",
                "bar",
                "fuzz",
                "qux"
              ],
              enumNames: ["one", "two", "three", "Four"]
            },
            "uniqueItems": true
          },
          "numberEnumRadio": {
            "type": "number",
            "title": "Number enum",
            "enum": [
              1,
              2,
              3
            ],
            enumNames: ["Apple", "Mango", "Orange"]
          },
          "countryName": {
            "type": "string",
            "title": "Country Name"
          },
          "stateName": {
            "type": "string",
            "title": "State Name",
            "required": true,
            "errorMsgs": {
              "required": "State name is required"
            },
            "condition": {
              type: FORM_CONDITION_TYPE.DEPENDENCY, //condition to fill state name dropdown based on country field value
              dependentField: "countryName", //Field name that we given in form schema
              fieldCheck: "countryName", //Field name that we will be comparing in state dataset for perticular country value
              action: FORM_CONDITION_ACTION.HIDDEN,
            },
          },
          "abModel": {
            "type": "string",
            "title": "Models from Data base"
          },
          "abField": {
            "type": "string",
            "title": "fields of selected model",
            "condition": {
              type: FORM_CONDITION_TYPE.DEPENDENCY, //condition to fill state name dropdown based on country field value
              dependentField: "abModel", //Field name that we given in form schema
              fieldCheck: "modelId", //Field name that we will be comparing in state dataset for perticular country value
              action: FORM_CONDITION_ACTION.HIDDEN,
            },
          },
          "phoneNumbers": {
            "type": "array",
            "title": "Phone Numbers",
            "items": {
              "type": "object",
              "required": [

              ],
              "properties": {
                // "stateName": {
                //   "type": "string",
                //   "title": "States"
                // },
                "phoneNumber": {
                  "type": "string",
                  "title": "Company Phone"
                },
                "ext": {
                  "type": "number",
                  "title": "Ext."
                },
                "type": {
                  "type": "string",
                  "title": "Type"
                },
                "default": {
                  "type": "boolean",
                  "title": " ",
                  "default": false
                }
              }
            }
          },
        }
      };
      const uiSchema = {
        "disabledToggle": {
          "ui:widget": "AbCheckbox",
          "ui:disabled": true,
          "ui:options": {
            "label": "Disabled Toggle checkbox?",
            "checkboxType": ABCHECKBOXTYPE.TOGGLE //if don't provided checkboxType will take default
          }
        },
        "sliderCheckbox": {
          "ui:widget": "AbCheckbox",
          "ui:options": {
            "label": "Slider checkbox?",
            "checkboxType": ABCHECKBOXTYPE.SLIDER //if don't provided checkboxType will take default
          }
        },
        "normalCheckbox": {
          "ui:widget": "AbCheckbox",
          "ui:options": {
            "label": "Default checkbox?"
          }
        },
        "multipleChoicesList": {
          "ui:widget": "AbCheckboxes",
          "ui:options": {
            inline: false,
            checkboxType: ABCHECKBOXTYPE.TOGGLE //if don't provided checkboxType will take default
          }
        },
        "numberEnumRadio": {
          "ui:widget": "AbRadio",
          "ui:options": {
            inline: true,
            radioType: ABCHECKBOXTYPE.SLIDER,   //if don't provided radioType will take default
            radioName: "numradio"
          }
        },
        countryName: {
          "ui:widget": "AbSelect",
          "ui:options": {
            selectOptions: country,
            labelKey: "label",
            valueKey: "value",
            searchable: true,
            selectPlaceholder: "Select country..",
            noResultsText: "No states found",
          }
        },
        stateName: {
          "ui:widget": "AbSelect",
          "ui:disabled": false,
          "ui:options": {
            selectOptions: geoData,
            labelKey: "label",
            valueKey: "value",
            searchable: true,
            selectPlaceholder: "Select states..",
            noResultsText: "No states found",
            multi: true,
            multiOptions: {
              outputAs: SELECT_MULTI_OPTIONS.ARRAY
            }
          }
        },
        userId: {    //it will load user list from database(prefilled from database when it load) To make it auto complete set async option to true
          "ui:widget": "AbAsyncSelect",
          "ui:options": {
            optionModel: "todousers",
            labelKey: "email",
            valueKey: "id",
            searchable: true,
            selectPlaceholder: "Select User..",
            noResultsText: "No User found",
            async: true,
            appbackApi: appbackApi,
            customFilter: { "disabled": false },
            matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
            filterOnFields: ["email", "id"],
            cache: false
          }
        },
        // userId: {    //Example of dropdown source of data from custom api
        //   "ui:widget": "AbSyncSelect",
        //   "ui:options": {
        //     optionModel: "TodoLists",
        //     labelKey: "name",
        //     valueKey: "id",
        //     searchable: true,
        //     selectPlaceholder: "Select User..",
        //     noResultsText: "No User found",
        //     async: true,
        //     appbackApi: appbackApi,
        //     // customFilter: { "disabled": false },
        //     matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
        //     filterOnFields: ["name", "description"],
        //     dataSourceType:GRID_DATA_SOURCE_TYPE.METHOD,
        //     dataSourceOptions:{ methodName: "getLists", data: {}, params: {}, options: { type: "get" } },
        //   }
        // },
        abModel: {
          "ui:widget": "AbSyncSelect",
          "ui:options": {
            optionModel: "abmodels",
            labelKey: "name",
            valueKey: "id",
            searchable: true,
            selectPlaceholder: "Select Model..",
            noResultsText: "No model found",
            appbackApi: appbackApi,
            optionRenderer: renderValue,
            valueRenderer: renderValue
          }
        },
        abField: {
          "ui:widget": "AbSyncSelect",
          "ui:options": {
            optionModel: "abfields",
            labelKey: "name",
            valueKey: "id",
            searchable: false,
            selectPlaceholder: "Select Model..",
            noResultsText: "No model found",
            appbackApi: appbackApi,
            autoload: true,
          }
        },
        phoneNumbers: {
          "ui:options": {
            "addable": true,        //for add or remove add button(which is adding new item in list)
            "orderable": false,     //for add or remove ordering(up and down) buttons
            // "removable": false   //for add or remove delete button from list
          },
          "items": {
            // "stateName": {
            //   "ui:widget": "AbSelect",
            //   "ui:options": {
            //     selectOptions: state,
            //     labelKey: "label",
            //     valueKey: "value",
            //     searchable: true,
            //     selectPlaceholder: "Select state..",
            //     noResultsText: "No states found",
            //     multi: true,
            //     multiOptions: {
            //       outputAs: SELECT_MULTI_OPTIONS.ARRAY
            //     }
            //   }
            // },
            "default": {
              "ui:widget": "AbCheckbox",
              "ui:options": {
                "label": "Default"
              }
            }
          }
        },
      }



      let todoScheme = {
        title: "TodoLists",
        type: "object",
        properties: {
          email: { type: "string", title: "Todo user email" },
          // phoneNumbers: {
          //   "type": "array",
          //   "title": "Phone Numbers",
          //   "items": {
          //     "type": "object",
          //     "required": [

          //     ],
          //     "properties": {
          //       "phoneNumber": {
          //         "type": "string",
          //         "title": "Company Phone"
          //       },
          //       "ext": {
          //         "type": "number",
          //         "title": "Ext."
          //       },
          //       "type": {
          //         "type": "string",
          //         "title": "Type"
          //       },
          //       "default": {
          //         "type": "boolean",
          //         "title": " ",
          //         "default": false
          //       }
          //     }
          //   }
          // },
          personalDetail: {
            title: " ",
            type: "object",
            properties: {
              firstName: { type: "string", title: "Todo user first name" },
              middleName: { type: "string", title: "Todo user middle name" },
              lastName: { type: "string", title: "Todo user last name" },
            }
          }
        }
      }

      let todoUiSchema = {
        email: {
          classNames: "field"
        },
        personalDetail: {
          classNames: "fields",
          firstName: {
            classNames: "six wide field"
          },
          middleName: {
            classNames: "two wide field"
          },
          lastName: {
            classNames: "six wide field"
          }
        },
        phoneNumbers: {
          classNames: "fields bhavesh",
          phoneNumber: {
            classNames: "field"
          },
          ext: {
            classNames: "field"
          },
          type: {
            classNames: "field"
          },
          default:{
            classNames: "field"
          }

        }
      }

      const CustomFieldTemplate = (props) => {
        const {id, classNames, label, help, required, description, errors, children} = props;
        let finalClass = classNames;
        if (props.schema && props.schema.type !=="object" && props.uiSchema.classNames) {
          finalClass = props.uiSchema.classNames
        }
        return (
          <div className={finalClass}>
            <label htmlFor={id}>{label}{required ? "*" : null}</label>
            {description}
            {children}
            {errors}
            {help}
          </div>
        );
      }

      const CustomRowComponent = (rowMetaData) => {
        let rowData = rowMetaData.data;
        return (<div style={{
          backgroundColor: "#EDEDED",
          border: "1px solid #777",
          padding: 5,
          margin: 10,
        }}>
          <h1>{rowData.name}</h1>
          <ul>
            <li><strong>description</strong>: {rowData.description}</li>
            <li><strong>State</strong>: {rowData.stateName}</li>
          </ul>
        </div>)
      }

      // const getUsers = (input) => {
      //   if (!input) {
      //     return Promise.resolve({ options: [] });
      //   }
      //   return fetch(`https://api.github.com/search/users?q=${input}`)
      //     .then((response) => response.json())
      //     .then((json) => {
      //       return { options: json.items };
      //     });
      // }
      // const onChangeSelect = (data) => {

      // }
      let abuserComp = null;
      if (this.state.showTodoUser) {
        abuserComp = (<div><AbModelGrid modelName="todoUsers" columnMetadata={todoUserColumnMetaData} appbackApi={appbackApi} errorCallback={self.errorCallback} />
          <AbModelForm modelName="todoUsers" schema={todoScheme} uiSchema={todoUiSchema} appbackApi={appbackApi} errorCallback={self.formErrorCallback}  FieldTemplate={CustomFieldTemplate}/></div>)
      }
      return (
        <Grid padded><Grid.Row><Grid.Column>
          {/*2 + 2 = {add(2, 2)}, isLoggedIn: {appbackApi.isLoggedIn() + ""}*/}
          {/*<FormWithWidget />*/}
          {/*<AbModelGrid modelName="AbSettings" modelCols={["name", "description", "value"]} />
          <AbModelGrid modelName="AbModels" modelCols={["name", "description", "type"]} columnMetadata={abmodelColumnMetaData}/>*/}
          <Segment color="blue">
            {<AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} dataSourceType={GRID_DATA_SOURCE_TYPE.METHOD} dataSourceOptions={{ methodName: "getLists", data: {}, params: {}, options: { type: "get" } }} customGridFilter={{ "name": "Type C" }} deleteCallback={self.deleteCallback} />}
          </Segment>
          <Segment color="green">
            <AbModelForm modelName="TodoLists" ref="myModelForm" theme={{ color: 'violet' }} schema={schema} uiSchema={uiSchema} appbackApi={appbackApi} showDefaultButtons={false} saveCallback={saveSuccessCallBack} errorCallback={self.formErrorCallback} onformDataChange={(data) => self.onFormChange(data)} />
          </Segment>
          <Segment color="orange">
            <button onClick={() => this.submitButtonClick()}>Save</button>
            <button onClick={() => this.cancelButtonClick()}>Cancel</button>
            <button onClick={() => this.showAbUserComp()}>Show AbuserComponent</button>
            {abuserComp}
          </Segment>
        </Grid.Column></Grid.Row></Grid>

      );
    } else {
      return null;
    }
  }
}
// customGridFilter={{"userId":"586cf69bce7b6c9ca6993ebe"}}
          // <Select.Async multi={false} simpleValue value={this.state.value} onChange={onChangeSelect} valueKey={"id"} labelKey={"login"} loadOptions={getUsers} backspaceRemoves={true} autoload={true} />


//Grid with custom method example
//  {<AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} dataSourceType={GRID_DATA_SOURCE_TYPE.METHOD} dataSourceOptions={{methodName:"getLists",data:{},params:{},options:{type:"get"}}}/>}         


//grid with abmodel
//  {<AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback}/>} 

//preload data in form on page reload using dev's param passing approach
//<AbModelForm modelName="TodoLists" ref="myModelForm" FieldTemplate={CustomFieldTemplate} theme={{ color: 'violet' }} schema={schema} uiSchema={uiSchema} appbackApi={appbackApi} showDefaultButtons={false} saveCallback={saveSuccessCallBack} errorCallback={self.formErrorCallback} onformDataChange={(data) => self.onFormChange(data)} loadData={{id:"58a545cbb58ee51343ec2172"}}/>


//grid with custom row renderer
  // <AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} dataSourceType={GRID_DATA_SOURCE_TYPE.METHOD} dataSourceOptions={{ methodName: "getLists", data: {}, params: {}, options: { type: "get" } }} customGridFilter={{ "name": "Type C" }} deleteCallback={self.deleteCallback} customRowComponent={CustomRowComponent} />