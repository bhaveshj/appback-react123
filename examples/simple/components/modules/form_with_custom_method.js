import React, { Component } from 'react';
import AppbackApi from "appback-api";
import { AbModelConstants, AbModelForm, AbModelGrid } from 'appback-react'
import { Segment } from 'semantic-ui-react'
import { geoData, companies } from "../data.js";
const { COLUMNTYPE, ACTIONBUTTON, ABCHECKBOXTYPE, ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_CONDITION_ACTION, FORM_CONDITION_TYPE, SELECT_MULTI_OPTIONS, FORM_DATA_SOURCE_OPTIONS_TYPE } = AbModelConstants;

class FormWithCustomMethod extends Component {
  constructor(props) {
    super(props);
  }

  errorCallback(error) {
    console.log("Error from component", error);
  }

  submitButtonClick() {
    this.refs.myModelForm.getWrappedInstance().submitFormClick();
  }

  cancelButtonClick() {
    this.refs.myModelForm.getWrappedInstance().cancelFormClick();
  }

  formErrorCallback(action, error) {
    console.log("error From form action", action);
    console.log("error From form error", error);
  }
  onFormChange(data) {
    console.log("form data change event callback from AbModelForm", data);
  }

  render() {
    let self = this;
    const { appbackApi } = this.props;
    let isLoggedIn = appbackApi.isLoggedIn();

    const actionButtonHandler = (event, rowData) => {
      console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
    }

    const saveSuccessCallBack = (event, data) => {
      console.log("save and edit callback data EVENT", event);
      console.log("save and edit callback data DATA", data);
    }

    let todoColumnMetaData = [
      {
        "columnName": "firstName",
        "order": 1,
        "locked": false,
        "visible": true,
        "displayName": "First Name",
        "filter": true,
      },
      {
        "columnName": "lastName",
        "order": 2,
        "locked": false,
        "visible": true,
        "displayName": "Last Name",
      },
      {
        "columnName": "email",
        "order": 2,
        "locked": false,
        "visible": true,
        "displayName": "Email Address",
      },
      {
        "columnName": "updatedAt",
        "order": 5,
        "locked": true,
        "visible": true,
        "sortable": false,
        "displayName": "Operation",
        "type": COLUMNTYPE.ACTIONS,
        "typeOptions": {
          "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete'],
          "clickHandler": actionButtonHandler,
          "buttons": [
            {
              "label": "Archive",
              "key": "archive",
              "icon": "archive"
            }
          ]
        }
      },
    ]

    const schema = {
      "type": "object",
      "required": [
        "firstName",
        "lastName",
        "email"
      ],
      "properties": {
        "firstName": {
          "type": "string",
          "title": "First Name"
        },
        "lastName": {
          "type": "string",
          "title": "Last Name"
        },
        "email": {
          "type": "string",
          "title": "Email"
        },
        "password": {
          "type": "string",
          "title": "Password"
        },
        "companyDetails": {
          "type": "array",
          "title": "Company details",
          "items": []
        }
      }
    };

    const uiSchema = {
      "firstName": {
        "ui:autofocus": true
      },
      "email": {
        "ui:widget": "email"
      },
      "companyDetails": {
        "items": []
      }
    }

    const companySchema = companies.map((company) => {
      var i = 0;
      let branchIds = [];
      let branchNames = [];
      for (i = 0; i < company.branches.length; ++i) {
        branchIds.push(company.branches[i].id);
        branchNames.push(company.branches[i].name);
      }
      let companiesProperties = {
        "type": "object",
        "segmentTitle": company.name,
        "properties": {
          "companyId": {
            "type": "string",
            "title": company.name,
            "default": company.id,
          },
          "role": {
            "type": "string",
            "title": "Role",
          },
          "branchIds": {
            "type": "string",
            "title": "Branches",
          }
        }
      }
      schema.properties.companyDetails.items.push(companiesProperties);
      uiSchema.companyDetails.items.push({
        "branchIds": {
          "ui:widget": "AbSelect",
          "ui:options": {
            selectOptions: company.branches || [],
            labelKey: "name",
            valueKey: "id",
            searchable: true,
            selectPlaceholder: "Select branches..",
            noResultsText: "No branches found",
            multi: true,
            multiOptions: { outputAs: SELECT_MULTI_OPTIONS.ARRAY }
          }
        },
        "companyId": {
          "ui:widget": "hidden"
        }
      });
      return companiesProperties;
    });

    let dataSourceOptions= {
      [FORM_DATA_SOURCE_OPTIONS_TYPE.CREATE] :{ methodName: "createOrUpdate", data: {}, params: {}, options: { type: "POST" } },
      [FORM_DATA_SOURCE_OPTIONS_TYPE.EDIT] :{ methodName: "createOrUpdate", data: {}, params: {}, options: { type: "POST" } },
      [FORM_DATA_SOURCE_OPTIONS_TYPE.LOAD] :{ methodName: "getUser", data: {}, params: {}, options: { type: "GET" } },
    }
    return (
      <div>
        <Segment color='red'>
          <h3>EXAMPLE INCLUDES:</h3>
          <ul>
            <li>We can provide custom methos as configuraion to store data in database instead of modelName</li>
            <li><b>This example is working with cornerstone csUsers model</b></li>
          </ul>
        </Segment>
        <Segment color="red">
            <AbModelGrid modelName="CsUsers" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} deleteCallback={self.deleteCallback} />
        </Segment>
        <Segment color="red">
          <AbModelForm modelName="CsUsers" ref="myModelForm" schema={schema} uiSchema={uiSchema} appbackApi={appbackApi} showDefaultButtons={false} saveCallback={saveSuccessCallBack} errorCallback={self.formErrorCallback} onformDataChange={(data) => self.onFormChange(data)}
            dataSourceType={GRID_DATA_SOURCE_TYPE.METHOD} dataSourceOptions={dataSourceOptions} loadData={{id:"58b7fc8d811b6708ce31c613"}}/>
        </Segment>
        <Segment color="red">
          <button onClick={() => this.submitButtonClick()}>Save</button>
          <button onClick={() => this.cancelButtonClick()}>Cancel</button>
        </Segment>
      </div>
    );

  }
}

export default FormWithCustomMethod