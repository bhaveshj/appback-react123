import React, { Component } from 'react';
import { AbModelGrid, AbModelConstants } from 'appback-react'
import { Segment } from 'semantic-ui-react'
const { COLUMNTYPE, ACTIONBUTTON } = AbModelConstants;

class GridWithAdvanceFilter extends Component {
    constructor(props) {
        super(props);
        this.errorCallback = this.errorCallback.bind(this);
        this.deleteCallback = this.deleteCallback.bind(this);
        this.exportStartCallback = this.exportStartCallback.bind(this);
        this.exportFinishCallback = this.exportFinishCallback.bind(this);
        this.exportErrorCallback = this.exportErrorCallback.bind(this);
    }
    errorCallback(error) {
        console.log("Error from component", error);
    }

    deleteCallback(data) {
        console.log("delete callback from grid", data);
    }

    exportStartCallback() {
        console.log("Export Start Callback");
    }

    exportFinishCallback() {
        console.log("Export Finish Callback");
    }
    exportErrorCallback(error) {
        console.log("Export Error Callback: ", error);
    }

    render() {
        const { appbackApi } = this.props;
        let self = this;

        const actionButtonHandler = (event, rowData) => {
            console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
        }
        const saveSuccessCallBack = (event, data) => {
            console.log("save and edit callback data EVENT", event);
            console.log("save and edit callback data DATA", data);
        }
        let todoColumnMetaData = [
            {
                "columnName": "name",
                "order": 1,
                "locked": false,
                "visible": true,
                "displayName": "Task Name",
                "filter": true,
                "advanceFilter": true,
                "advanceFilterOptions": {
                    fieldType: "string",
                    multiSelect: false,
                    clearable: false,
                    valueKey: "value",
                    labelKey: "label",
                    placeholder: "Transaction Type",
                    dataSet: [
                        {
                            value: "Type A",
                            label: "Type A"
                        },
                        {
                            value: "Type B",
                            label: "Type B"
                        },
                        {
                            value: "Type C",
                            label: "Type C"
                        },
                        {
                            value: "Type D",
                            label: "Type D"
                        }
                    ]
                }
            },
            {
                "columnName": "description",
                "order": 2,
                "locked": false,
                "visible": true,
                "displayName": "Description",
                "advanceFilter": true,
                "advanceFilterOptions": {
                    fieldType: "string",
                    multiSelect: false,
                    clearable: false,
                    placeholder: "Transaction Status",
                    valueKey: "value",
                    labelKey: "label",
                    dataSet: [
                        {
                            value: "OPEN",
                            label: "OPEN"
                        },
                        {
                            value: "CLOSE",
                            label: "CLOSE"
                        }
                    ],
                    onChange: (value) => { console.log("onChange Value: ", value) } //onChange callback function
                }
            },
            {
                "columnName": "createdAt",
                "order": 4,
                "locked": false,
                "visible": true,
                "displayName": "Created At",
                "type": COLUMNTYPE.DATE,
                "typeOptions": {
                    "format": "DD MMM YYYY hh:mm:ss A" //format is from moment js, url: http://momentjs.com/docs/#/parsing/string-format/ 
                },
                "includeModel": {
                    "items": "todouser"
                },
            },
            {
                "columnName": "updatedAt",
                "order": 5,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Operation",
                "type": COLUMNTYPE.ACTIONS,
                "typeOptions": {
                    "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete'],
                    // "deleteConfirmModalOptions":{
                    //   "header":"Please Confirm",
                    //   "content":"Are sure you want to delete?",
                    //   "confirmButton":{
                    //     text:"Yes",
                    //     icon:"checkmark",
                    //     callback:(data)=>console.log("confirm callback from grid metaData",data)
                    //   },
                    //   "cancelButton":{
                    //     text:"No",
                    //     icon:"remove",
                    //     callback:(data)=>console.log("cancel callback from grid metaData",data)
                    //   }
                    // },
                    "clickHandler": actionButtonHandler,
                    "buttons": [
                        {
                            "label": "Archive",
                            "key": "archive",
                            "icon": "archive"
                        }
                    ]
                }
            },
            {
                "columnName": "todouser.email",
                "order": 3,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Task By User",
                "includeModel": "todouser",
                "type": COLUMNTYPE.LINK,
                "typeOptions": {
                    "path": "/todouser" //make it path
                }
            }
        ]
        let userData = JSON.parse(localStorage.getItem('userData'));
        let token = userData.id;
        let apiUrl = "http://localhost:4000/api";
        // let expression= "data"; //needed when AbmodelGrid using Cutom api
        let configObj = {
            "s3": {
                "bucket": "dumbo-s3-bucket",
                "key": "test/report.pdf"
            },
            "pageHeader": {
                "name": "To Do Lists",
                "style": {
                    "align": "center",
                    "fontSize": 16,
                }
            },
            "tableHeader": {
                "afterNewline": 1,
                "columnTitle": true,
                "columnTitleStyle": {
                    "border": 1,
                    "fontBold": true,
                    "padding": 5,
                    "valign": "center"
                }
            },
            "detailOutput": {
                "column": [
                    {
                        "title": "Name",
                        "expression": "name"
                    },
                    {
                        "title": "Description",
                        "expression": "description"
                    },
                    {
                        "title": "Created By",
                        "expression": "todouser.email"
                    },
                    {
                        "title": "Involved Users",
                        "expression": "items.todouser.email"
                    },
                    {
                        "title": "Created At",
                        "expression": "createdAt"
                    },
                    {
                        "title": "Updated At",
                        "expression": "updatedAt"
                    },
                    {
                        "title": "IsDeleted",
                        "expression": "_isDeleted"
                    },
                    {
                        "title": "Id",
                        "expression": "id"
                    },
                ],
                "style": {
                    "border": 1,
                    "wrap": 1,
                    "padding": 5
                }
            },
            "groupBy": {
                "name": ""
            }
        }
        let fileName = "dynamic_name" //desired file name for excel export

        return (
            <div>
                <Segment color='red'>
                    <h3>EXAMPLE INCLUDES:</h3>
                    <ul>
                        <li>Grid with advance filter configs</li>
                    </ul>
                </Segment>
                <AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} deleteCallback={self.deleteCallback} exportData={true} exportConfig={{ token, apiUrl, configObj, fileName }} exportStartCallback={this.exportStartCallback} exportFinishCallback={this.exportFinishCallback} exportErrorCallback={this.exportErrorCallback} />
            </div>
        );
    }
}



export default GridWithAdvanceFilter