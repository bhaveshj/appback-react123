"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ACTION_HANDLERS;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = modelsReducer;

var _constants = require("./constants");

var _underscore = require("underscore");

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  "results": [],
  "recordCount": 0,
  "currentPage": 0,
  "maxPages": 0,
  "resultsPerPage": 10,
  "externalSortColumn": "createdAt",
  "externalSortAscending": true,
  "externalFilter": "",
  "advanceFilter": {},
  "advanceFilterConfig": [],
  "filterCols": [],
  "includes": [],
  "form": {
    "currentActiveItem": {},
    "action": _constants.ACTIONBUTTON.CREATE
  },
  "showLoader": false,
  "exportData": false,
  "exportConfig": null

  // ------------------------------------
  // Reducer handler functions
  // ------------------------------------

};var showHideLoader = function showHideLoader(state, action, loaderDisplayStatus) {
  var payload = action.payload;

  if (payload && payload.AbModelName) {
    var AbModelName = payload.AbModelName;

    return Object.assign({}, state, _defineProperty({}, AbModelName, _extends({}, state[AbModelName], { showLoader: loaderDisplayStatus })));
  } else {
    return state;
  }
};

var setModelsData = function setModelsData(state, action) {
  // const modelData = _.pluck(action.payload, ["results", "" ]);
  var _action$payload = action.payload,
      results = _action$payload.results,
      currentPage = _action$payload.currentPage,
      maxPages = _action$payload.maxPages,
      recordCount = _action$payload.recordCount,
      resultsPerPage = _action$payload.resultsPerPage,
      externalSortColumn = _action$payload.externalSortColumn,
      externalSortAscending = _action$payload.externalSortAscending,
      externalFilter = _action$payload.externalFilter,
      modelName = _action$payload.modelName,
      filters = _action$payload.filters,
      includes = _action$payload.includes,
      advanceFilter = _action$payload.advanceFilter;

  var modelData = {
    results: results,
    currentPage: currentPage,
    maxPages: maxPages,
    recordCount: recordCount,
    resultsPerPage: resultsPerPage,
    externalSortColumn: externalSortColumn,
    externalSortAscending: externalSortAscending,
    externalFilter: externalFilter,
    advanceFilter: advanceFilter,
    filterCols: filters,
    includes: includes
  };
  var finalModelData = _extends({}, state[modelName], modelData);
  return Object.assign({}, state, _defineProperty({}, modelName, finalModelData));
};

var setModelIntialData = function setModelIntialData(state, action) {
  if (state[action.payload.modelName]) {
    return state;
  } else {
    var initialConfigData = action.payload.initialConfigData;

    return Object.assign({}, state, _defineProperty({}, action.payload.modelName, _extends({}, initialState, initialConfigData)));
  }
};

var setCurrentActiveItem = function setCurrentActiveItem(state, action) {
  var currentStateData = state[action.payload.modelName];
  currentStateData.form = _extends({}, currentStateData.form, action.payload.form);
  return Object.assign({}, state, _defineProperty({}, action.payload.modelName, _extends({}, currentStateData)));
};

var saveModelFormDataSuccess = function saveModelFormDataSuccess(state, action) {
  var _action$payload2 = action.payload,
      modelName = _action$payload2.modelName,
      data = _action$payload2.data,
      saveCallback = _action$payload2.saveCallback;

  var latestData = _underscore2.default.map(state[modelName].results, function (item) {
    if (item.id === data.id) {
      return _extends({}, item, data);
    } else {
      return item;
    }
  });
  var finalstate = _extends({}, state[modelName], _extends({ results: latestData }, { form: { currentActiveItem: {}, action: _constants.ACTIONBUTTON.CREATE } }));
  var actionType = state[modelName].form.action;
  // if(saveCallback){
  //   saveCallback(actionType,data);
  // }
  return Object.assign({}, state, _defineProperty({}, modelName, finalstate));
};

var createModelFormDataSuccess = function createModelFormDataSuccess(state, action) {
  var _action$payload3 = action.payload,
      modelName = _action$payload3.modelName,
      data = _action$payload3.data,
      saveCallback = _action$payload3.saveCallback;

  var finalstate = _extends({}, state[modelName], { form: { currentActiveItem: {}, action: _constants.ACTIONBUTTON.CREATE } });
  var actionType = state[modelName].form.action;
  // if(saveCallback){
  //   saveCallback(actionType,data);
  // }
  return Object.assign({}, state, _defineProperty({}, modelName, finalstate));
};

var clearFormData = function clearFormData(state, action) {
  var modelName = action.payload.modelName;

  var finalState = state[modelName];
  finalState.form.currentActiveItem = {};
  finalState.form.action = _constants.ACTIONBUTTON.CREATE;
  return Object.assign({}, state, _defineProperty({}, modelName, finalState));
};

var mergeGridConfig = function mergeGridConfig(state, action) {
  var _action$payload4 = action.payload,
      modelName = _action$payload4.modelName,
      data = _action$payload4.data;

  var finalState = state[modelName];
  finalState = _extends({}, finalState, data);
  return Object.assign({}, state, _defineProperty({}, modelName, _extends({}, finalState)));
};

var resetGridInitialData = function resetGridInitialData(state, action) {
  var modelName = action.payload.modelName;

  if (state[modelName]) {
    var gridInitialData = _extends({}, initialState);
    delete gridInitialData["form"];
    return Object.assign({}, state, _defineProperty({}, modelName, _extends({}, state[modelName], gridInitialData)));
  }
};

// ------------------------------------
// Action Handlers
// ------------------------------------
var ACTION_HANDLERS = (_ACTION_HANDLERS = {}, _defineProperty(_ACTION_HANDLERS, _constants.MODELS_DATA_REQUEST_SUCCESS, function (state, action) {
  return setModelsData(state, action);
}), _defineProperty(_ACTION_HANDLERS, _constants.SET_MODEL_INITIAL_STATE, function (state, action) {
  return setModelIntialData(state, action);
}), _defineProperty(_ACTION_HANDLERS, _constants.SET_CURRENT_ACTIVE_ITEM, function (state, action) {
  return setCurrentActiveItem(state, action);
}), _defineProperty(_ACTION_HANDLERS, _constants.SAVE_MODEL_FORM_DATA_SUCCESS, function (state, action) {
  return saveModelFormDataSuccess(state, action);
}), _defineProperty(_ACTION_HANDLERS, _constants.CREATE_MODEL_FORM_DATA_SUCCESS, function (state, action) {
  return createModelFormDataSuccess(state, action);
}), _defineProperty(_ACTION_HANDLERS, _constants.CLEAR_FORM_DATA, function (state, action) {
  return clearFormData(state, action);
}), _defineProperty(_ACTION_HANDLERS, _constants.MERGE_GRID_CONFIG, function (state, action) {
  return mergeGridConfig(state, action);
}), _defineProperty(_ACTION_HANDLERS, _constants.RESET_GRID_DATA_ONLY, function (state, action) {
  return resetGridInitialData(state, action);
}), _defineProperty(_ACTION_HANDLERS, _constants.SHOW_LOADER, function (state, action) {
  return showHideLoader(state, action, true);
}), _defineProperty(_ACTION_HANDLERS, _constants.HIDE_LOADER, function (state, action) {
  return showHideLoader(state, action, false);
}), _ACTION_HANDLERS);

// ------------------------------------
// Reducer
// ------------------------------------
function modelsReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments[1];

  var handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}