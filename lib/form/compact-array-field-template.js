'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = CompactArrayFieldTemplate;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require('semantic-ui-react');

require('./ArrayField.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function CompactArrayFieldTemplate(props) {
    var theme = props.theme;

    var ArrayFieldItem = function ArrayFieldItem(element) {
        var segmentTitle = element.index + 1;
        var children = element.children;

        if (children) {
            if (children.props && children.props.schema && children.props.schema.segmentTitle) {
                segmentTitle = children.props.schema.segmentTitle;
            }
        }
        return _react2.default.createElement(
            _semanticUiReact.Grid,
            { columns: 'equal', key: element.index },
            _react2.default.createElement(
                _semanticUiReact.Grid.Row,
                { className: 'padding-0' },
                _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    null,
                    _react2.default.createElement(
                        'div',
                        null,
                        element.children
                    )
                ),
                (element.hasRemove || element.hasMoveDown || element.hasMoveUp) && _react2.default.createElement(
                    _semanticUiReact.Grid.Column,
                    { width: '1', textAlign: 'right', className: 'margin-top-4' },
                    element.hasRemove && _react2.default.createElement(_semanticUiReact.Button, { onClick: element.onDropIndexClick(element.index), basic: true, size: 'small', type: 'button', icon: 'trash', color: 'red' }),
                    _react2.default.createElement(
                        _semanticUiReact.Button.Group,
                        { basic: true, size: 'small' },
                        element.hasMoveDown && _react2.default.createElement(_semanticUiReact.Button, { onClick: element.onReorderClick(element.index, element.index + 1), type: 'button', icon: 'arrow down' }),
                        element.hasMoveUp && _react2.default.createElement(_semanticUiReact.Button, { onClick: element.onReorderClick(element.index, element.index - 1), type: 'button', icon: 'arrow up' })
                    )
                )
            )
        );
    };

    return _react2.default.createElement(
        'div',
        { className: props.className },
        _react2.default.createElement(
            'label',
            { className: 'padding-bottom-10' },
            props.title
        ),
        props.items && props.items.map(ArrayFieldItem),
        props.canAdd && _react2.default.createElement(_semanticUiReact.Button, { onClick: props.onAddClick, color: theme.color ? theme.color : null, basic: true, icon: 'plus', content: 'Add', size: 'tiny', className: 'margin-top-4' })
    );
}