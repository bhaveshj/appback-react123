'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _jbReactJsonschemaForm = require('jb-react-jsonschema-form');

var _jbReactJsonschemaForm2 = _interopRequireDefault(_jbReactJsonschemaForm);

var _constants = require('../../constants');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

require('./form.css');

var _semanticUiReact = require('semantic-ui-react');

var _custom_widgets = require('../custom_widgets');

var _arrayFieldTemplate = require('../array-field-template');

var _arrayFieldTemplate2 = _interopRequireDefault(_arrayFieldTemplate);

var _compactArrayFieldTemplate = require('../compact-array-field-template');

var _compactArrayFieldTemplate2 = _interopRequireDefault(_compactArrayFieldTemplate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AbModelForm = function (_Component) {
  _inherits(AbModelForm, _Component);

  function AbModelForm(props) {
    _classCallCheck(this, AbModelForm);

    var _this = _possibleConstructorReturn(this, (AbModelForm.__proto__ || Object.getPrototypeOf(AbModelForm)).call(this, props));

    _initialiseProps.call(_this);

    _this.getFormSelectTransformList = _this.getFormSelectTransformList.bind(_this);
    var conditionsObj = {};
    var transformConditionObject = function transformConditionObject(schemaProperties) {
      _lodash2.default.forEach(schemaProperties, function (properties, key) {
        if (properties['condition']) {
          conditionsObj[key] = properties.condition;
        } else if (properties['type'] == 'object') {
          transformConditionObject(properties['properties']);
        }
      });
    };
    if (props.schema.properties) {
      transformConditionObject(props.schema.properties);
    }

    var mainSchema = _lodash2.default.cloneDeep(props.schema);
    var mainUiScheme = _lodash2.default.cloneDeep(props.uiSchema);
    var transformSelectDetail = _this.getFormSelectTransformList(mainUiScheme);

    var _this$getFormTransfor = _this.getFormTransformList(mainSchema),
        transformFieldsDetail = _this$getFormTransfor.transformFieldsDetail,
        transformFlagDetail = _this$getFormTransfor.transformFlagDetail;

    _this.state = {
      formData: props.formData,
      actualSchema: _extends({}, props.schema),
      actualUiSchema: _extends({}, props.uiSchema),
      schema: _extends({}, mainSchema),
      uiSchema: _extends({}, mainUiScheme),
      conditionsObj: conditionsObj,
      key: new Date().getTime(),
      transformSelectDetail: transformSelectDetail,
      transformFieldsDetail: transformFieldsDetail,
      transformFlagDetail: transformFlagDetail
    };
    var modifiedData = _this.modifySchemeBasedOnCondition(_lodash2.default.cloneDeep(props.formData), true);
    _this.state = _extends({}, _this.state, modifiedData);

    console.log("form-component constructor called");
    return _this;
  }

  _createClass(AbModelForm, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _props = this.props,
          formData = _props.formData,
          saveCallback = _props.saveCallback,
          errorCallback = _props.errorCallback,
          dataSourceType = _props.dataSourceType,
          dataSourceOptions = _props.dataSourceOptions,
          loadData = _props.loadData,
          getPreLoadFormData = _props.getPreLoadFormData,
          AbModelInstance = _props.AbModelInstance,
          formAction = _props.formAction;
      var _state = this.state,
          transformSelectDetail = _state.transformSelectDetail,
          actualSchema = _state.actualSchema;

      if (dataSourceType && dataSourceType === _constants.GRID_DATA_SOURCE_TYPE.METHOD) {
        if (dataSourceOptions && dataSourceOptions.LOAD && dataSourceOptions.LOAD.methodName) {
          if (loadData && loadData.id !== undefined) {
            getPreLoadFormData({ "filter": loadData, AbModelInstance: AbModelInstance, saveCallback: saveCallback, errorCallback: errorCallback, dataSourceType: dataSourceType, "dataSourceOptions": dataSourceOptions.LOAD, transformSelectDetail: transformSelectDetail });
          }
        }
      } else {
        if (!formData || Object.keys(formData).length <= 0 || formAction !== _constants.ACTIONBUTTON.EDIT) {
          if (loadData && loadData.id !== undefined) {
            getPreLoadFormData({ "filter": loadData, AbModelInstance: AbModelInstance, saveCallback: saveCallback, errorCallback: errorCallback });
          }
        }
      }

      if (formData) {
        var transformedFormData = _lodash2.default.cloneDeep(formData);
        // this.transformSelectValue(transformSelectDetail, transformedFormData);
        var finalData = {};
        this.transformFieldsValueBefore(actualSchema, transformedFormData, finalData);
        finalData = JSON.stringify(finalData, function (k, v) {
          if (v === null) {
            return undefined;
          }return v;
        });
        finalData = JSON.parse(finalData);
        if (transformSelectDetail) {
          this.transformSelectValue(transformSelectDetail, finalData);
        }
        transformedFormData = _extends({}, transformedFormData, _lodash2.default.cloneDeep(finalData));
        this.modifySchemeBasedOnCondition(transformedFormData, false);
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var _state2 = this.state,
          transformSelectDetail = _state2.transformSelectDetail,
          actualSchema = _state2.actualSchema;
      // let transformedFormData = _.cloneDeep(nextProps.formData);

      var transformedFormData = void 0;
      if (!_lodash2.default.isEqual(this.props.formData, nextProps.formData)) {
        transformedFormData = _lodash2.default.cloneDeep(nextProps.formData);
      } else {
        transformedFormData = _lodash2.default.cloneDeep(this.state.formData);
      }
      if (Object.keys(this.state.formData).length === 0 && Object.keys(transformedFormData).length > 0) {
        this.transformSelectValue(transformSelectDetail, transformedFormData);
        // let finalData = {};
        // transformFieldsValueBefore(actualSchema,transformedFormData,finalData);
        // transformedFormData = { ..._.cloneDeep(finalData)}
      }
      if (!_lodash2.default.isEqual(this.state.formData, transformedFormData)) {
        var finalData = {};
        this.transformFieldsValueBefore(actualSchema, transformedFormData, finalData);
        finalData = JSON.stringify(finalData, function (k, v) {
          if (v === null) {
            return undefined;
          }return v;
        });
        finalData = JSON.parse(finalData);
        if (transformSelectDetail) {
          this.transformSelectValue(transformSelectDetail, finalData);
        }
        // transformedFormData = { ..._.cloneDeep(finalData) }
        transformedFormData = _extends({}, transformedFormData, _lodash2.default.cloneDeep(finalData));
        this.modifySchemeBasedOnCondition(transformedFormData, false);
        // this.setState({ formData: { ...nextProps.formData } });
      }
    }
  }, {
    key: 'submitFormClick',
    value: function submitFormClick() {
      this.refs.formSubmitBtn.click();
    }
  }, {
    key: 'cancelFormClick',
    value: function cancelFormClick() {
      var clearFormData = this.props.clearFormData;

      this.setState({ key: new Date().getTime() });
      clearFormData();
    }
  }, {
    key: 'setData',
    value: function setData(newFields) {
      var tempState = _lodash2.default.cloneDeep(this.state.formData);
      this.setState({
        formData: _extends({}, tempState, newFields)
      });
    }
  }, {
    key: 'getFormCurrentData',
    value: function getFormCurrentData() {
      return this.state.formData;
    }
  }, {
    key: 'getDependentFieldFormValue',
    value: function getDependentFieldFormValue(formData, dependentField) {
      var dependentValue = undefined;
      if (dependentField.constructor === String) {
        if (formData[dependentField] !== undefined && formData[dependentField].toString().length > 0) {
          dependentValue = formData[dependentField];
        }
      } else if (dependentField.constructor === Object) {
        var getdependentValue = function getdependentValue(form, dependentFieldKey) {
          var key = _lodash2.default.keys(dependentFieldKey);
          if (key.length > 0 && dependentFieldKey[key[0]].constructor === Object) {
            if (form[key[0]]) {
              getdependentValue(form[key], dependentFieldKey[key[0]]);
            }
          } else if (key.length > 0 && dependentFieldKey[key[0]].constructor === String) {
            if (form[key[0]]) {
              dependentValue = form[key[0]][dependentFieldKey[key[0]]];
            }
          }
        };
        getdependentValue(formData, dependentField);
      }
      return dependentValue;
    }
  }, {
    key: 'modifySchemeBasedOnCondition',
    value: function modifySchemeBasedOnCondition(formData, firstLoad) {
      var _this2 = this;

      var _state3 = this.state,
          conditionsObj = _state3.conditionsObj,
          actualSchema = _state3.actualSchema,
          actualUiSchema = _state3.actualUiSchema;

      var tempSchema = _lodash2.default.cloneDeep(actualSchema);
      var tempUiSchema = _lodash2.default.cloneDeep(actualUiSchema);
      _lodash2.default.forOwn(conditionsObj, function (conditionProps, key) {
        var _conditionProps$type = conditionProps.type,
            type = _conditionProps$type === undefined ? null : _conditionProps$type,
            _conditionProps$depen = conditionProps.dependentField,
            dependentField = _conditionProps$depen === undefined ? null : _conditionProps$depen,
            fieldCheck = conditionProps.fieldCheck,
            action = conditionProps.action,
            _conditionProps$value = conditionProps.valueCheck,
            valueCheck = _conditionProps$value === undefined ? null : _conditionProps$value,
            _conditionProps$multi = conditionProps.multiValueCheck,
            multiValueCheck = _conditionProps$multi === undefined ? null : _conditionProps$multi;

        if (type) {
          if (dependentField) {
            if (formData && type === _constants.FORM_CONDITION_TYPE.DEPENDENCY) {
              var dependentValue = _this2.getDependentFieldFormValue(formData, dependentField);
              if (dependentValue) {
                if (tempUiSchema) {
                  var findFieldInUiSchema = function findFieldInUiSchema(temporaryUiSchema) {
                    if (temporaryUiSchema[key]) {
                      var widgetObject = temporaryUiSchema[key];
                      if (widgetObject["ui:widget"] === "AbSelect" || widgetObject["ui:widget"] === "AbSyncSelect") {
                        if (widgetObject["ui:options"]) {
                          if (widgetObject["ui:options"].customFilter) {
                            var customFilterToSend = widgetObject["ui:options"].customFilter;
                            if (customFilterToSend.where) {
                              customFilterToSend.where[fieldCheck] = dependentValue;
                            } else {
                              customFilterToSend['where'] = _defineProperty({}, fieldCheck, dependentValue);
                            }
                            widgetObject["ui:options"].customFilter = customFilterToSend;
                          } else {
                            widgetObject["ui:options"]["customFilter"] = { where: _defineProperty({}, fieldCheck, dependentValue) };
                          }
                        }
                      }
                    } else {
                      _lodash2.default.forEach(temporaryUiSchema, function (tUiSchema) {
                        if (!_lodash2.default.isEmpty(tUiSchema) && tUiSchema.constructor === Object) {
                          findFieldInUiSchema(tUiSchema);
                        }
                      });
                    }
                  };
                  findFieldInUiSchema(tempUiSchema);
                }
              }
            } else {
              if (type === _constants.FORM_CONDITION_TYPE.VISIBILITY) {
                var fieldValue = _this2.getDependentFieldFormValue(formData, dependentField);
                if (valueCheck) {
                  if (fieldValue !== undefined && fieldValue === valueCheck) {
                    return;
                  }
                } else if (multiValueCheck) {
                  if (fieldValue !== undefined && _lodash2.default.includes(multiValueCheck, fieldValue)) {
                    return;
                  }
                } else {
                  if (fieldValue !== undefined && fieldValue.toString().length > 0) {
                    return;
                  }
                }
              }
              if (action === _constants.FORM_CONDITION_ACTION.HIDDEN) {
                if (tempSchema.properties[key]) {
                  delete tempSchema.properties[key];
                  delete tempUiSchema[key];
                  delete formData[key];
                } else {
                  var findProperty = function findProperty(schemaProperties, uiSchema, dataObject) {
                    _lodash2.default.forEach(schemaProperties, function (schemaProperty, schemaKey) {
                      if (schemaKey == key) {
                        delete schemaProperties[key];
                        delete dataObject[key];
                        delete uiSchema[key];
                      } else if (schemaProperty['type'] == 'object') {
                        var uiSchemaToSend = _lodash2.default.cloneDeep(uiSchema);
                        if (uiSchema[schemaKey]) {
                          uiSchemaToSend = uiSchema[schemaKey];
                        }
                        if (dataObject[schemaKey]) {
                          findProperty(schemaProperty.properties, uiSchemaToSend, dataObject[schemaKey]);
                        } else {
                          findProperty(schemaProperty.properties, uiSchemaToSend, dataObject);
                        }
                      }
                    });
                  };
                  findProperty(tempSchema.properties, tempUiSchema, formData);
                }
              } else if (action === _constants.FORM_CONDITION_ACTION.DISABLED) {
                if (tempSchema.properties[key]) {
                  if (tempUiSchema[key]) {
                    tempUiSchema[key]["ui:disabled"] = true;
                  } else {
                    tempUiSchema[key] = { "ui:disabled": true };
                  }
                } else {
                  var _findProperty = function _findProperty(schemaProperties, uiSchema) {
                    _lodash2.default.forEach(schemaProperties, function (schemaProperty, schemaKey) {
                      if (schemaKey == key) {
                        // uiSchema[schemaKey] = { "ui:disabled": true };
                        uiSchema[schemaKey] = _extends({}, uiSchema[schemaKey], { "ui:disabled": true });
                      } else if (schemaProperty['type'] == 'object') {
                        if (!uiSchema[schemaKey]) {
                          uiSchema[schemaKey] = {};
                        }
                        _findProperty(schemaProperty.properties, uiSchema[schemaKey]);
                      }
                    });
                  };
                  _findProperty(tempSchema.properties, tempUiSchema);
                }
              } else if (action === _constants.FORM_CONDITION_ACTION.ENABLED) {
                var finalAction = false;
                var _fieldValue = _this2.getDependentFieldFormValue(formData, dependentField);
                if (valueCheck) {
                  if (_fieldValue && _fieldValue === valueCheck) {
                    finalAction = true;
                  }
                } else if (multiValueCheck) {
                  if (_fieldValue && _lodash2.default.includes(multiValueCheck, _fieldValue)) {
                    finalAction = true;
                  }
                } else {
                  if (_fieldValue && _fieldValue.toString().length > 0) {
                    finalAction = true;
                  }
                }
                if (tempSchema.properties[key]) {
                  if (tempUiSchema[key]) {
                    tempUiSchema[key]["ui:disabled"] = finalAction;
                  } else {
                    tempUiSchema[key] = { "ui:disabled": finalAction };
                  }
                } else {
                  var _findProperty2 = function _findProperty2(schemaProperties, uiSchema) {
                    _lodash2.default.forEach(schemaProperties, function (schemaProperty, schemaKey) {
                      if (schemaKey == key) {
                        uiSchema[schemaKey] = _extends({}, uiSchema[schemaKey], { "ui:disabled": finalAction });
                      } else if (schemaProperty['type'] == 'object') {
                        if (!uiSchema[schemaKey]) {
                          uiSchema[schemaKey] = {};
                        }
                        _findProperty2(schemaProperty.properties, uiSchema[schemaKey]);
                      }
                    });
                  };
                  _findProperty2(tempSchema.properties, tempUiSchema);
                }
              }
            }
          }
        }
      });
      if (firstLoad) {
        return { schema: tempSchema, uiSchema: tempUiSchema };
      } else {
        this.setState({
          formData: _extends({}, formData),
          schema: _extends({}, tempSchema),
          uiSchema: _extends({}, tempUiSchema)
        });
      }
    }
  }, {
    key: 'onChange',
    value: function onChange(data) {
      var onformDataChange = this.props.onformDataChange;

      // if (data.errors && data.errors.length === 0) {

      this.modifySchemeBasedOnCondition(data.formData, false);
      // }

      if (onformDataChange) {
        onformDataChange(data);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var self = this;
      if (!this.props.abModelData) {
        return null;
      }
      var _props2 = this.props,
          AbModelInstance = _props2.AbModelInstance,
          showDefaultButtons = _props2.showDefaultButtons,
          saveCallback = _props2.saveCallback,
          FieldTemplate = _props2.FieldTemplate,
          liveValidate = _props2.liveValidate,
          validate = _props2.validate,
          showErrorList = _props2.showErrorList,
          noHtml5Validate = _props2.noHtml5Validate,
          dataSourceType = _props2.dataSourceType,
          dataSourceOptions = _props2.dataSourceOptions;
      var _props3 = this.props,
          filterCols = _props3.filterCols,
          includes = _props3.includes,
          externalFilter = _props3.externalFilter,
          formData = _props3.formData,
          formAction = _props3.formAction,
          _props3$theme = _props3.theme,
          theme = _props3$theme === undefined ? {} : _props3$theme,
          widgets = _props3.widgets;
      var _props4 = this.props,
          createModelFormData = _props4.createModelFormData,
          saveModelFormData = _props4.saveModelFormData,
          clearFormData = _props4.clearFormData;
      var _state4 = this.state,
          schema = _state4.schema,
          uiSchema = _state4.uiSchema,
          key = _state4.key,
          transformSelectDetail = _state4.transformSelectDetail,
          transformFieldsDetail = _state4.transformFieldsDetail;

      var log = function log(type) {/*console.log.bind(console, type)*/};

      var beformSubmitTransform = function beformSubmitTransform(metaData) {
        var formData = metaData.formData,
            uiSchema = metaData.uiSchema;

        var transformSelectValue = function transformSelectValue(transformArray, dataObject) {
          _lodash2.default.map(_lodash2.default.uniq(transformArray), function (fieldName) {
            if (dataObject && dataObject.constructor === Array) {
              _lodash2.default.map(dataObject, function (data) {
                if (data && data[fieldName]) {
                  if (transformArray[fieldName]) {
                    transformSelectValue(transformArray[fieldName], data[fieldName]);
                  } else {
                    if (data[fieldName].constructor === String) {
                      data[fieldName] = data[fieldName].split(",");
                    }
                  }
                }
              });
            } else if (dataObject && dataObject.constructor === Object) {
              if (dataObject && dataObject[fieldName]) {
                if (transformArray[fieldName]) {
                  transformSelectValue(transformArray[fieldName], dataObject[fieldName]);
                } else {
                  if (dataObject[fieldName].constructor === String) {
                    dataObject[fieldName] = dataObject[fieldName].split(",");
                  }
                }
              }
            }
          });
        };

        // const transformInner = (transformArray, transformedFormData) => {
        // 	_.map(_.uniq(transformArray), function (fieldName) {
        // 		if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
        // 			transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
        // 			delete transformedFormData[fieldName];
        // 		} else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
        // 			_.map(transformedFormData[fieldName], function (fieldObject) {
        // 				if (transformArray[fieldName]) {
        // 					transformInner(transformArray[fieldName], fieldObject);
        // 				}
        // 			});
        // 		}
        // 	});
        // 	return transformedFormData;
        // }
        // const transformFieldsValue = (transformArray, transformedFormData) => {
        // 	// let transformedFormData = _.cloneDeep(transformedFormData);
        // 	_.map(_.uniq(transformArray), function (fieldName) {
        // 		if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
        // 			transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
        // 			delete transformedFormData[fieldName];
        // 		} else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
        // 			_.map(transformedFormData[fieldName], function (fieldObject, index) {
        // 				if (transformArray[fieldName]) {
        // 					let tempData = transformInner(transformArray[fieldName], fieldObject);
        // 					transformedFormData[fieldName][index] = { ..._.cloneDeep(tempData) }
        // 					// fieldObject = {...tempData};
        // 				}
        // 			});
        // 		}
        // 	});
        // 	return transformedFormData;
        // }
        transformSelectValue(transformSelectDetail, formData);
        // let finalData = {..._.cloneDeep(formData)};
        // let finalData = transformFieldsValue(transformFieldsDetail, _.cloneDeep(formData));
        // transformFieldsValue(transformFieldsDetail, finalData);
        metaData.formData = _extends({}, formData);
        return metaData;
      };
      var formSubmit = function formSubmit(formMetaData) {
        var transformedFormData = beformSubmitTransform(_lodash2.default.cloneDeep(formMetaData));
        var _state5 = _this3.state,
            transformFieldsDetail = _state5.transformFieldsDetail,
            transformFlagDetail = _state5.transformFlagDetail;

        var payloadData = {
          formMetaData: transformedFormData,
          AbModelInstance: AbModelInstance,
          filterCols: filterCols,
          includes: includes,
          externalFilter: externalFilter,
          saveCallback: saveCallback,
          dataSourceType: dataSourceType,
          dataSourceOptions: dataSourceOptions,
          transformFieldsDetail: transformFieldsDetail,
          transformFlagDetail: transformFlagDetail
        };

        if (formAction === _constants.ACTIONBUTTON.CREATE) {
          createModelFormData(payloadData);
        } else if (formAction === _constants.ACTIONBUTTON.EDIT) {
          if (!_lodash2.default.isEqual(formData, formMetaData.formData)) {
            saveModelFormData(payloadData);
          } else {
            if (saveCallback) {
              saveCallback(formData);
            }
          }
        } else {}
      };
      var getSubmitButtonText = function getSubmitButtonText() {
        if (formAction === _constants.ACTIONBUTTON.EDIT) {
          return "Save";
        } else {
          return "Create";
        }
      };
      var customWidgets = {
        AbCheckbox: _custom_widgets.AbCheckboxWidget,
        AbCheckboxes: _custom_widgets.AbCheckboxesWidget,
        AbRadio: _custom_widgets.AbRadioWidget,
        AbSelect: _custom_widgets.AbSelectWidget,
        AbAsyncSelect: _custom_widgets.AbAsyncSelectWidget,
        AbSyncSelect: _custom_widgets.AbSyncSelectWidget,
        AbDateTimePicker: _custom_widgets.AbDateTimePickerWidget,
        AbMaskedInput: _custom_widgets.AbMaskedInputWidget,
        AbEditableDateTimePicker: _custom_widgets.AbEditableDateTimePickerWidget,
        AbModularDatePicker: _custom_widgets.AbModularDatePickerWidget,
        AbFileUploader: _custom_widgets.AbFileUploaderWidget,
        AbMultiFileUploader: _custom_widgets.AbMultiFileUploaderWidget,
        AbPhoneInput: _custom_widgets.AbPhoneInputWidget
      };
      if (widgets) {
        customWidgets = _extends({}, customWidgets, widgets);
      }

      var transformErrors = function transformErrors(errors) {
        for (var index = 0; index < errors.length; index++) {
          var _errors$index = errors[index],
              _schema = _errors$index.schema,
              name = _errors$index.name,
              argument = _errors$index.argument,
              instance = _errors$index.instance;

          if (_schema && _schema.errorMsgs && _schema.errorMsgs[name]) {
            errors[index].message = _schema.errorMsgs[name];
          }
          if (name === 'type' && Array.isArray(argument) && argument.includes('number') && !isNaN(instance)) {
            errors.splice(index, 1);
            index--;
          }
        }
        return errors;
      };
      var _onError = function _onError(error) {
        var errorCallback = _this3.props.errorCallback;

        if (errorCallback) {
          errorCallback('validation', error);
        }
      };
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _jbReactJsonschemaForm2.default,
          { schema: schema,
            uiSchema: uiSchema,
            widgets: customWidgets,
            onChange: function onChange(data) {
              return self.onChange(data);
            },
            onSubmit: function onSubmit(data) {
              return formSubmit(data);
            },
            onError: function onError(error) {
              return _onError(error);
            },
            formData: self.state.formData,
            ArrayFieldTemplate: function ArrayFieldTemplate(props) {
              return props['schema'] && props['schema']['compact'] ? (0, _compactArrayFieldTemplate2.default)(_extends({}, props, { theme: theme })) : (0, _arrayFieldTemplate2.default)(_extends({}, props, { theme: theme }));
            },
            className: 'ui form',
            showErrorList: showErrorList,
            noHtml5Validate: noHtml5Validate,
            FieldTemplate: FieldTemplate,
            transformErrors: transformErrors,
            liveValidate: liveValidate,
            validate: validate,
            key: key
          },
          _react2.default.createElement(
            'div',
            { className: showDefaultButtons ? 'show' : 'hide' },
            _react2.default.createElement(
              'button',
              { type: 'submit', ref: 'formSubmitBtn', className: 'ui button' },
              getSubmitButtonText()
            ),
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'ui button', onClick: function onClick(e) {
                  return _this3.cancelFormClick(e);
                } },
              'Cancel'
            )
          )
        )
      );
    }
  }]);

  return AbModelForm;
}(_react.Component);

var _initialiseProps = function _initialiseProps() {
  var _this4 = this;

  this.getFormSelectTransformList = function (mainUiScheme) {
    var self = _this4;
    var selectCountToBeTransform = [];
    var fetchValues = function fetchValues(uiSchema, parent) {
      if (uiSchema.constructor === Array) {
        _lodash2.default.map(uiSchema, function (fieldObj) {
          fetchValues(fieldObj, parent);
        });
      } else if (uiSchema.constructor === Object) {
        _lodash2.default.forOwn(uiSchema, function (schemeObj, key) {
          if (schemeObj && key) {
            if (schemeObj["ui:widget"] && schemeObj["ui:widget"] === 'AbMultiFileUploader') {
              parent.push(key);
            } else if (schemeObj["ui:options"] && schemeObj["ui:options"].multi && schemeObj["ui:widget"] && (schemeObj["ui:widget"] === "AbSelect" || schemeObj["ui:widget"] === "AbAsyncSelect" || schemeObj["ui:widget"] === "AbSyncSelect")) {
              parent.push(key);
            } else if (schemeObj["items"]) {
              parent.push(key);
              parent[key] = [];
              fetchValues(schemeObj["items"], parent[key]);
            } else {
              if (!schemeObj["ui:options"]) {
                parent.push(key);
                parent[key] = [];
                fetchValues(schemeObj, parent[key]);
              }
            }
          }
        });
      }
    };
    fetchValues(mainUiScheme, selectCountToBeTransform);
    return selectCountToBeTransform;
  };

  this.getFormTransformList = function (mainSchema) {
    var self = _this4;
    var fieldTobeTransform = [];
    var transformFlagDetail = {};
    var fetchFields = function fetchFields(schema, parent) {
      // if (schema.constructor === Array) {
      // 	// _.map(schema, function (fieldObj) {
      // 	// 	fetchFields(fieldObj, parent)
      // 	// });
      // } else if (schema.type === "object" && schema.properties) {
      _lodash2.default.forOwn(schema.properties, function (schemeObj, key) {
        if (schemeObj && key) {
          if (schemeObj.type && schemeObj.type === "object" && schemeObj.transform) {
            if (schemeObj.transform !== "NOT") {
              transformFlagDetail[key] = true;
            } else {
              transformFlagDetail[key] = false;
            }
            parent.push(key);
            if (schemeObj.properties) {
              parent[key] = [];
              fetchFields(schemeObj, parent[key]);
            }
          } else if (schemeObj.type && schemeObj.type === "array" && schemeObj.items && schemeObj.items.constructor === Object && (!schemeObj.items.transform || schemeObj.items.transform !== 'NOT')) {
            parent.push(key);
            parent[key] = [];
            fetchFields(schemeObj.items, parent[key]);
          }
        }
      });
    };
    fetchFields(mainSchema, fieldTobeTransform);
    return { transformFieldsDetail: fieldTobeTransform, transformFlagDetail: transformFlagDetail };
  };

  this.transformSelectValue = function (transformArray, dataObject) {
    _lodash2.default.map(_lodash2.default.uniq(transformArray), function (fieldName) {
      if (dataObject && dataObject.constructor === Array) {
        _lodash2.default.map(dataObject, function (data) {
          if (data && data[fieldName]) {
            if (transformArray[fieldName]) {
              _this4.transformSelectValue(transformArray[fieldName], data[fieldName]);
            } else {
              if (data[fieldName].constructor === Array) {
                data[fieldName] = data[fieldName].join(",");
              }
            }
          }
        });
      } else if (dataObject && dataObject.constructor === Object) {

        if (dataObject && dataObject[fieldName]) {
          if (transformArray[fieldName]) {
            _this4.transformSelectValue(transformArray[fieldName], dataObject[fieldName]);
          } else {
            if (dataObject[fieldName].constructor === Array) {
              dataObject[fieldName] = dataObject[fieldName].join(",");
            }
          }
        }
      }
    });
  };

  this.transformFieldsValueBefore = function (schema, dataObject, finalData) {
    _lodash2.default.forOwn(schema.properties, function (schemaObj, key) {
      if (schemaObj.type === "object") {
        if (!finalData[key]) {
          finalData[key] = {};
        }
        if (dataObject[key]) {
          _this4.transformFieldsValueBefore(schemaObj, dataObject[key], finalData[key]);
        } else {
          _this4.transformFieldsValueBefore(schemaObj, dataObject, finalData[key]);
        }
      } else if (schemaObj.type === "array" && schemaObj.items && schemaObj.items.type !== 'string') {
        if (!finalData[key]) {
          finalData[key] = [];
        }
        _lodash2.default.map(dataObject[key], function (fieldObj, index) {
          if (!finalData[key][index]) {
            finalData[key][index] = {};
          }
          _this4.transformFieldsValueBefore(schemaObj.items, fieldObj, finalData[key][index]);
        });
      } else {
        if (key in dataObject) {
          finalData[key] = _lodash2.default.cloneDeep(dataObject[key]);
          delete dataObject[key];
        }
      }
    });
  };
};

exports.default = AbModelForm;


AbModelForm.propTypes = {
  modelName: _react.PropTypes.string.isRequired,
  schema: _react.PropTypes.object.isRequired,
  uiSchema: _react.PropTypes.object,
  appbackApi: _react.PropTypes.object.isRequired,
  showDefaultButtons: _react.PropTypes.bool,
  saveCallback: _react.PropTypes.func,
  errorCallback: _react.PropTypes.func.isRequired,
  onformDataChange: _react.PropTypes.func,
  liveValidate: _react.PropTypes.bool,
  validate: _react.PropTypes.func,
  showErrorList: _react.PropTypes.bool,
  noHtml5Validate: _react.PropTypes.bool,
  widgets: _react.PropTypes.object
};

AbModelForm.defaultProps = {
  showDefaultButtons: true,
  saveCallback: null,
  uiSchema: {},
  liveValidate: false,
  validate: null,
  showErrorList: false,
  noHtml5Validate: false
};