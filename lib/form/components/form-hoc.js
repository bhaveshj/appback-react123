'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _actions = require('../../actions');

var AbModelFormAction = _interopRequireWildcard(_actions);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormWrapper = function FormWrapper(props) {
    return function (Component) {
        var mapStateToProps = function mapStateToProps(state, ownProps) {
            var appbackApi = props.appbackApi,
                modelName = props.modelName;

            var mapped = {
                abModelData: state.abModels[modelName],
                formData: state.abModels[modelName] ? state.abModels[modelName].form.currentActiveItem : {},
                formAction: state.abModels[modelName] ? state.abModels[modelName].form.action : null,
                filterCols: state.abModels[modelName] ? state.abModels[modelName].filterCols : [],
                includes: state.abModels[modelName] ? state.abModels[modelName].includes : [],
                AbModelInstance: appbackApi.createModelRest(modelName, null)
            };
            return mapped;
        };
        var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
            dispatch(AbModelFormAction.setModelInitialData(props.modelName));
            return {
                saveModelFormData: function saveModelFormData(payloadData) {
                    var modelName = props.modelName,
                        errorCallback = props.errorCallback;

                    payloadData["AbModelName"] = modelName;
                    payloadData["isFormWrapper"] = true;
                    return dispatch(AbModelFormAction.saveModelFormData(payloadData));
                },
                createModelFormData: function createModelFormData(payloadData) {
                    var modelName = props.modelName,
                        errorCallback = props.errorCallback;

                    payloadData["AbModelName"] = props.modelName;
                    payloadData["isFormWrapper"] = true;
                    return dispatch(AbModelFormAction.createModelFormData(payloadData));
                },
                clearFormData: function clearFormData() {
                    dispatch(AbModelFormAction.clearFormData({ modelName: props.modelName }));
                },
                getPreLoadFormData: function getPreLoadFormData(loadData) {
                    var filter = loadData.filter,
                        AbModelInstance = loadData.AbModelInstance,
                        saveCallback = loadData.saveCallback,
                        errorCallback = loadData.errorCallback;

                    dispatch(AbModelFormAction.getPreLoadFormData(_extends({ AbModelName: props.modelName }, _extends({}, loadData))));
                }
            };
        };

        var FormHoc = function (_React$Component) {
            _inherits(FormHoc, _React$Component);

            function FormHoc() {
                _classCallCheck(this, FormHoc);

                return _possibleConstructorReturn(this, (FormHoc.__proto__ || Object.getPrototypeOf(FormHoc)).apply(this, arguments));
            }

            _createClass(FormHoc, [{
                key: 'render',
                value: function render() {
                    return _react2.default.createElement(Component, _extends({}, this.props, props));
                }
            }]);

            return FormHoc;
        }(_react2.default.Component);

        return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(FormHoc);
    };
};

exports.default = FormWrapper;