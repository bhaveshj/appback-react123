"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var wildcard = require('wildcard');

var filterFormDataBasedOnSchema = exports.filterFormDataBasedOnSchema = function filterFormDataBasedOnSchema(schema, formData) {
    var finalFormData = {};
    var fields = Object.keys(schema.properties);
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = fields[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var fieldName = _step.value;

            if (formData.hasOwnProperty(fieldName)) {
                finalFormData[fieldName] = formData[fieldName];
            }
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }

    return finalFormData;
};

var getFormTransformList = exports.getFormTransformList = function getFormTransformList(mainSchema) {
    var self = undefined;
    var fieldTobeTransform = [];
    var transformFlagDetail = {};
    var fetchFields = function fetchFields(schema, parent) {
        _.forOwn(schema.properties, function (schemeObj, key) {
            if (schemeObj && key) {
                if (schemeObj.type && schemeObj.type === "object" && schemeObj.transform) {
                    if (schemeObj.transform !== "NOT") {
                        transformFlagDetail[key] = true;
                    } else {
                        transformFlagDetail[key] = false;
                    }
                    parent.push(key);
                    if (schemeObj.properties) {
                        parent[key] = [];
                        fetchFields(schemeObj, parent[key]);
                    }
                } else if (schemeObj.type && schemeObj.type === "array" && schemeObj.items && schemeObj.items.constructor === Object) {
                    parent.push(key);
                    parent[key] = [];
                    fetchFields(schemeObj.items, parent[key]);
                }
            }
        });
    };
    fetchFields(mainSchema, fieldTobeTransform);
    return { transformFieldsDetail: fieldTobeTransform, transformFlagDetail: transformFlagDetail };
};

var transformFieldsValueBefore = exports.transformFieldsValueBefore = function transformFieldsValueBefore(schema, dataObject) {
    var finalData = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    // let finalData = {}
    _.forOwn(schema.properties, function (schemaObj, key) {
        if (schemaObj.type === "object") {
            if (!finalData[key]) {
                finalData[key] = {};
            }
            if (dataObject[key]) {
                transformFieldsValueBefore(schemaObj, dataObject[key], finalData[key]);
            } else {
                transformFieldsValueBefore(schemaObj, dataObject, finalData[key]);
            }
        } else if (schemaObj.type === "array") {
            if (!finalData[key]) {
                finalData[key] = [];
            }
            _.map(dataObject[key], function (fieldObj, index) {
                if (!finalData[key][index]) {
                    finalData[key][index] = {};
                }
                transformFieldsValueBefore(schemaObj.items, fieldObj, finalData[key][index]);
            });
        } else {
            if (dataObject[key]) {
                finalData[key] = _.cloneDeep(dataObject[key]);
                delete dataObject[key];
            }
        }
    });
    return finalData;
};

var compareFileWithMimeType = exports.compareFileWithMimeType = function compareFileWithMimeType(target, pattern) {
    var reMimePartSplit = /[\/\+\.]/;
    function test(pattern) {
        var result = wildcard(pattern, target, reMimePartSplit);

        // ensure that we have a valid mime type (should have two parts)
        return result && result.length >= 2;
    }

    return pattern ? test(pattern.split(';')[0]) : test;
};