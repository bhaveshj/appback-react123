'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require('semantic-ui-react');

var _constants = require('../../constants');

var _reactDatetime = require('react-datetime');

var _reactDatetime2 = _interopRequireDefault(_reactDatetime);

require('./css/ab-datetimpicker.css');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AbDateTimePickerWidget = function AbDateTimePickerWidget(props) {
    var id = props.id,
        value = props.value,
        onChange = props.onChange,
        disabled = props.disabled,
        options = props.options,
        readonly = props.readonly,
        placeholder = props.placeholder,
        required = props.required;
    var _options$targetTimeZo = options.targetTimeZone,
        targetTimeZone = _options$targetTimeZo === undefined ? null : _options$targetTimeZo,
        _options$displayDateF = options.displayDateFormat,
        displayDateFormat = _options$displayDateF === undefined ? "MMM DD YYYY" : _options$displayDateF,
        _options$displayTimeF = options.displayTimeFormat,
        displayTimeFormat = _options$displayTimeF === undefined ? "hh:mm A" : _options$displayTimeF,
        className = options.className,
        _options$minDate = options.minDate,
        minDate = _options$minDate === undefined ? null : _options$minDate,
        _options$maxDate = options.maxDate,
        maxDate = _options$maxDate === undefined ? null : _options$maxDate,
        clearable = options.clearable;

    var defaultFormat = null; // = "MMM DD YYYY hh:mm A";
    if (displayDateFormat !== false) {
        defaultFormat = displayDateFormat;
    }
    if (displayTimeFormat !== false) {
        if (defaultFormat !== null) {
            defaultFormat = defaultFormat + " " + displayTimeFormat;
        } else {
            defaultFormat = displayTimeFormat;
        }
    }
    var finalDate;
    if (value) {
        var dateValue = (0, _moment2.default)(value);
        var parsedDate;
        if (targetTimeZone) {
            finalDate = (0, _moment2.default)(value).utcOffset(targetTimeZone).format(defaultFormat);
        } else {
            finalDate = dateValue.format(defaultFormat);
        }
    }
    var parseDate = function parseDate(dateMomentObj) {
        var finalDateObj = _.cloneDeep(dateMomentObj);
        if (!dateMomentObj) {
            onChange(undefined);
            return;
        }
        if (typeof dateMomentObj === 'string' || dateMomentObj instanceof String) {
            if ((0, _moment2.default)(dateMomentObj, defaultFormat, true).isValid()) {
                finalDateObj = (0, _moment2.default)(dateMomentObj, defaultFormat);
            } else {
                return false;
            }
        }
        if (targetTimeZone) {
            onChange((0, _moment2.default)(finalDateObj.utcOffset(targetTimeZone, true)).toString());
        } else {
            onChange((0, _moment2.default)(finalDateObj).toString());
        }
    };

    var valid = function valid(current) {
        var flag = 1;
        if (minDate && current < (0, _moment2.default)(minDate).startOf('day')) {
            flag = 0;
        }
        if (maxDate && current > maxDate) {
            flag = 0;
        }
        return flag;
    };
    var onBlur = function onBlur(dateMomentObj) {
        if (typeof dateMomentObj === 'string' || dateMomentObj instanceof String) {
            if (!(0, _moment2.default)(dateMomentObj, defaultFormat, true).isValid()) {
                onChange(dateMomentObj);
            }
        }
    };
    return _react2.default.createElement(_reactDatetime2.default, { value: finalDate, onChange: parseDate, onBlur: onBlur, isValidDate: valid, input: true, clearable: clearable, dateFormat: displayDateFormat, timeFormat: displayTimeFormat, className: className, inputProps: { required: required, readOnly: readonly, disabled: disabled, className: "form-control", placeholder: placeholder } });
};

AbDateTimePickerWidget.defaultProps = {};

AbDateTimePickerWidget.propTypes = {
    schema: _react.PropTypes.object.isRequired,
    id: _react.PropTypes.string.isRequired,
    options: _react.PropTypes.shape({
        targetTimeZone: _react.PropTypes.string,
        displayDateFormat: _react.PropTypes.any,
        displayTimeFormat: _react.PropTypes.any,
        minDate: _react.PropTypes.Date,
        maxDate: _react.PropTypes.Date
    }).isRequired
};

exports.default = AbDateTimePickerWidget;