'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

require('react-select/dist/react-select.css');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AbSyncSelectWidget = function (_React$Component) {
    _inherits(AbSyncSelectWidget, _React$Component);

    function AbSyncSelectWidget(props) {
        _classCallCheck(this, AbSyncSelectWidget);

        var _this = _possibleConstructorReturn(this, (AbSyncSelectWidget.__proto__ || Object.getPrototypeOf(AbSyncSelectWidget)).call(this, props));

        _this.state = {
            "showLoader": false,
            "results": []
        };
        _this.customFilterOption = _this.customFilterOption.bind(_this);
        return _this;
    }

    _createClass(AbSyncSelectWidget, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.loadOptions(this.props);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            if (!_lodash2.default.isEqual(this.props.options, nextProps.options)) {
                this.loadOptions(nextProps);
            }
        }
    }, {
        key: 'customFilterOption',
        value: function customFilterOption(option, filter) {
            var filterOnFields = this.props.options.filterOnFields;

            var resultValue = false;
            if (filter.toString().length > 0) {
                _lodash2.default.map(filterOnFields, function (fieldName) {
                    if (option[fieldName] && option[fieldName].toString().toLowerCase().includes(filter)) {
                        resultValue = true;
                    }
                });
                return resultValue;
            } else {
                resultValue = true;
                return resultValue;
            }
        }
    }, {
        key: 'loadOptions',
        value: function loadOptions(props) {
            var self = this;
            self.setState({
                showLoader: true
            });
            var AbModelInstance = props.AbModelInstance,
                getDataFromServer = props.getDataFromServer,
                options = props.options;
            var customFilter = options.customFilter,
                labelKey = options.labelKey,
                matchInputFrom = options.matchInputFrom,
                dataSourceType = options.dataSourceType,
                dataSourceOptions = options.dataSourceOptions;

            var filter = {};
            if (customFilter) {
                filter = _extends({}, customFilter);
            }
            if (dataSourceType && dataSourceOptions && dataSourceType === _constants.GRID_DATA_SOURCE_TYPE.METHOD) {
                var methodName = dataSourceOptions.methodName,
                    data = dataSourceOptions.data,
                    params = dataSourceOptions.params,
                    _options = dataSourceOptions.options;

                var finalParams = { "filter": filter };
                if (params) {
                    finalParams = _extends({}, finalParams, params);
                }
                AbModelInstance.custom(methodName, data, finalParams, _options).then(function (response) {
                    if (response) {
                        if (response instanceof Array) {
                            self.setState({
                                results: response,
                                showLoader: false
                            });
                        } else {
                            if (response instanceof Object && response.data) {
                                self.setState({
                                    results: response.data,
                                    showLoader: false
                                });
                            } else {
                                self.setState({
                                    showLoader: false
                                });
                            }
                        }
                    }
                }, function (error) {
                    console.log("error", error);
                    // errorCallback(error);
                    // return { result: "fail", data: error };
                });
            } else {
                getDataFromServer({ AbModelInstance: AbModelInstance, filter: filter }).then(function (response) {
                    if (response) {
                        self.setState({
                            results: response,
                            showLoader: false
                        });
                    }
                    // return { options: response };
                }, function (error) {
                    console.log("error", error);
                    // return { options: [] };
                });
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var self = this;
            var _props = this.props,
                id = _props.id,
                value = _props.value,
                onChange = _props.onChange,
                disabled = _props.disabled,
                options = _props.options,
                required = _props.required;
            var _state = this.state,
                showLoader = _state.showLoader,
                results = _state.results;
            var backspaceRemoves = options.backspaceRemoves,
                labelKey = options.labelKey,
                multi = options.multi,
                valueKey = options.valueKey,
                searchable = options.searchable,
                clearable = options.clearable,
                noResultsText = options.noResultsText,
                selectPlaceholder = options.selectPlaceholder,
                searchPromptText = options.searchPromptText,
                autofocus = options.autofocus,
                customFilter = options.customFilter,
                optionRenderer = options.optionRenderer,
                valueRenderer = options.valueRenderer,
                filterOnFields = options.filterOnFields,
                creatable = options.creatable,
                delimiter = options.delimiter;

            var valueChanged = function valueChanged(formData) {
                var sendData = formData == null || formData.toString().length === 0 ? undefined : formData;
                onChange(sendData);
            };
            if (filterOnFields && filterOnFields.length > 0) {
                if (creatable && multi) {
                    return _react2.default.createElement(
                        'div',
                        { style: required && !searchable ? { height: '36px' } : {} },
                        _react2.default.createElement(_reactSelect2.default.Creatable, { key: id, multi: multi, options: results, simpleValue: true, clearable: clearable, disabled: disabled, valueKey: valueKey, labelKey: labelKey, value: value, onChange: valueChanged, searchable: searchable, noResultsText: noResultsText, placeholder: selectPlaceholder, searchPromptText: searchPromptText, autofocus: autofocus, required: required, isLoading: showLoader, optionRenderer: optionRenderer, valueRenderer: valueRenderer, filterOption: self.customFilterOption, delimiter: delimiter }),
                        required && !searchable && _react2.default.createElement('input', { tabIndex: -1, value: value, required: true, style: { width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' } })
                    );
                } else {
                    return _react2.default.createElement(
                        'div',
                        { style: required && !searchable ? { height: '36px' } : {} },
                        _react2.default.createElement(_reactSelect2.default, { key: id, multi: multi, options: results, simpleValue: true, clearable: clearable, disabled: disabled, valueKey: valueKey, labelKey: labelKey, value: value, onChange: valueChanged, searchable: searchable, noResultsText: noResultsText, placeholder: selectPlaceholder, searchPromptText: searchPromptText, autofocus: autofocus, required: required, isLoading: showLoader, optionRenderer: optionRenderer, valueRenderer: valueRenderer, filterOption: self.customFilterOption, delimiter: delimiter }),
                        required && !searchable && _react2.default.createElement('input', { tabIndex: -1, value: value, required: true, style: { width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' } })
                    );
                }
            } else {
                if (creatable && multi) {
                    return _react2.default.createElement(
                        'div',
                        { style: required && !searchable ? { height: '36px' } : {} },
                        _react2.default.createElement(_reactSelect2.default.Creatable, { key: id, multi: multi, options: results, simpleValue: true, clearable: clearable, disabled: disabled, valueKey: valueKey, labelKey: labelKey, value: value, onChange: valueChanged, searchable: searchable, noResultsText: noResultsText, placeholder: selectPlaceholder, searchPromptText: searchPromptText, autofocus: autofocus, required: required, isLoading: showLoader, optionRenderer: optionRenderer, valueRenderer: valueRenderer, delimiter: delimiter }),
                        required && !searchable && _react2.default.createElement('input', { tabIndex: -1, value: value, required: true, style: { width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' } })
                    );
                } else {
                    return _react2.default.createElement(
                        'div',
                        { style: required && !searchable ? { height: '36px' } : {} },
                        _react2.default.createElement(_reactSelect2.default, { key: id, multi: multi, options: results, simpleValue: true, clearable: clearable, disabled: disabled, valueKey: valueKey, labelKey: labelKey, value: value, onChange: valueChanged, searchable: searchable, noResultsText: noResultsText, placeholder: selectPlaceholder, searchPromptText: searchPromptText, autofocus: autofocus, required: required, isLoading: showLoader, optionRenderer: optionRenderer, valueRenderer: valueRenderer, delimiter: delimiter }),
                        required && !searchable && _react2.default.createElement('input', { tabIndex: -1, value: value, required: true, style: { width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' } })
                    );
                }
            }
        }
    }]);

    return AbSyncSelectWidget;
}(_react2.default.Component);

AbSyncSelectWidget.defaultProps = {
    options: {
        multi: false,
        clearable: true,
        searchable: false,
        backspaceRemoves: true,
        noResultsText: "No results found",
        selectPlaceholder: "Select ...",
        searchPromptText: "Type to search",
        autofocus: false,
        customFilter: {},
        optionRenderer: null,
        valueRenderer: null,
        creatable: false,
        delimiter: _constants.FORM_UTILS.DELIMITER
    }
};

AbSyncSelectWidget.propTypes = {
    schema: _react.PropTypes.object.isRequired,
    id: _react.PropTypes.string.isRequired,
    value: _react.PropTypes.any,
    required: _react.PropTypes.bool,
    autofocus: _react.PropTypes.bool,
    disabled: _react.PropTypes.bool,
    onChange: _react.PropTypes.func,
    options: _react.PropTypes.shape({
        labelKey: _react.PropTypes.string.required,
        valueKey: _react.PropTypes.string.required,
        multi: _react.PropTypes.bool,
        clearable: _react.PropTypes.bool,
        searchable: _react.PropTypes.bool,
        backspaceRemoves: _react.PropTypes.bool,
        noResultsText: _react.PropTypes.string,
        selectPlaceholder: _react.PropTypes.string,
        searchPromptText: _react.PropTypes.string,
        autofocus: _react.PropTypes.bool,
        customFilter: _react.PropTypes.object,
        optionRenderer: _react.PropTypes.func,
        valueRenderer: _react.PropTypes.func,
        filterOnFields: _react.PropTypes.array,
        creatable: _react.PropTypes.bool,
        delimiter: _react.PropTypes.string
    }).isRequired
};
exports.default = AbSyncSelectWidget;