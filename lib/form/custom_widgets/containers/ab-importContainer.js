'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _abImport = require('../ab-import');

var _abImport2 = _interopRequireDefault(_abImport);

var _actions = require('../../../actions');

var AbModelAction = _interopRequireWildcard(_actions);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state, ownProps) {
  //   const { appbackApi, importModel} = ownProps.options;
  var mapped = {
    // AbModelInstance: appbackApi.createModelRest(importModel, null)
  };
  return mapped;
};
function mapDispatchToProps(dispatch, ownProps) {
  return {
    importFile: function importFile(payloadData) {
      //   const { importModel } = ownProps.options;
      //   payloadData["AbModelName"] = importModel;
      return dispatch(AbModelAction.importFile(payloadData));
    }
  };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_abImport2.default);