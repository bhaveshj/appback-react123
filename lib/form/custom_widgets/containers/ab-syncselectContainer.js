'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _abSyncselect = require('../ab-syncselect');

var _abSyncselect2 = _interopRequireDefault(_abSyncselect);

var _actions = require('../../../actions');

var AbModelAction = _interopRequireWildcard(_actions);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state, ownProps) {
  var _ownProps$options = ownProps.options,
      appbackApi = _ownProps$options.appbackApi,
      optionModel = _ownProps$options.optionModel;

  var mapped = {
    AbModelInstance: appbackApi.createModelRest(optionModel, null)
  };
  return mapped;
};
function mapDispatchToProps(dispatch, ownProps) {
  return {
    getDataFromServer: function getDataFromServer(payloadData) {
      var optionModel = ownProps.options.optionModel;

      payloadData["AbModelName"] = optionModel;
      return dispatch(AbModelAction.getSelectModelData(payloadData));
    }
  };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_abSyncselect2.default);