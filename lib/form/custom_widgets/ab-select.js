'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

require('react-select/dist/react-select.css');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AbSelectWidget = function AbSelectWidget(props) {
    var id = props.id,
        value = props.value,
        onChange = props.onChange,
        disabled = props.disabled,
        options = props.options,
        required = props.required;
    var backspaceRemoves = options.backspaceRemoves,
        labelKey = options.labelKey,
        multi = options.multi,
        valueKey = options.valueKey,
        selectOptions = options.selectOptions,
        searchable = options.searchable,
        clearable = options.clearable,
        noResultsText = options.noResultsText,
        selectPlaceholder = options.selectPlaceholder,
        searchPromptText = options.searchPromptText,
        autofocus = options.autofocus,
        customFilter = options.customFilter,
        optionRenderer = options.optionRenderer,
        valueRenderer = options.valueRenderer,
        delimiter = options.delimiter;

    var finalOptions = selectOptions;
    if (customFilter) {
        finalOptions = _lodash2.default.filter(selectOptions, customFilter);
    }
    var valueChanged = function valueChanged(formData) {
        var sendData = formData == null || formData.toString().length === 0 ? undefined : formData;
        onChange(sendData);
    };
    return _react2.default.createElement(
        'div',
        { style: required && !searchable ? { height: '36px' } : {} },
        _react2.default.createElement(_reactSelect2.default, { multi: multi, options: finalOptions, simpleValue: true, clearable: clearable, disabled: disabled, valueKey: valueKey, labelKey: labelKey, value: value, onChange: valueChanged, searchable: searchable, noResultsText: noResultsText, placeholder: selectPlaceholder, searchPromptText: searchPromptText, autofocus: autofocus, required: required, optionRenderer: optionRenderer, valueRenderer: valueRenderer, delimiter: delimiter }),
        required && !searchable && _react2.default.createElement('input', { tabIndex: -1, value: value, required: true, style: { width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' } })
    );
};

AbSelectWidget.defaultProps = {
    options: {
        multi: false,
        clearable: true,
        searchable: false,
        backspaceRemoves: true,
        noResultsText: "No results found",
        selectPlaceholder: "Select ...",
        searchPromptText: "Type to search",
        autofocus: false,
        customFilter: {},
        optionRenderer: null,
        valueRenderer: null,
        delimiter: _constants.FORM_UTILS.DELIMITER
    }
};

AbSelectWidget.propTypes = {
    schema: _react.PropTypes.object.isRequired,
    id: _react.PropTypes.string.isRequired,
    value: _react.PropTypes.any,
    required: _react.PropTypes.bool,
    autofocus: _react.PropTypes.bool,
    disabled: _react.PropTypes.bool,
    onChange: _react.PropTypes.func,
    options: _react.PropTypes.shape({
        selectOptions: _react.PropTypes.array.required,
        labelKey: _react.PropTypes.string.required,
        valueKey: _react.PropTypes.string.required,
        multi: _react.PropTypes.bool,
        clearable: _react.PropTypes.bool,
        searchable: _react.PropTypes.bool,
        backspaceRemoves: _react.PropTypes.bool,
        noResultsText: _react.PropTypes.string,
        selectPlaceholder: _react.PropTypes.string,
        searchPromptText: _react.PropTypes.string,
        autofocus: _react.PropTypes.bool,
        customFilter: _react.PropTypes.object,
        optionRenderer: _react.PropTypes.func,
        valueRenderer: _react.PropTypes.func,
        delimiter: _react.PropTypes.string
    }).isRequired
};
exports.default = AbSelectWidget;