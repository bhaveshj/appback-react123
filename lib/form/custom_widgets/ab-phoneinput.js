'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactPhoneInput = require('react-phone-input-2');

var _reactPhoneInput2 = _interopRequireDefault(_reactPhoneInput);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AbPhoneInputWidget = function (_Component) {
	_inherits(AbPhoneInputWidget, _Component);

	function AbPhoneInputWidget() {
		_classCallCheck(this, AbPhoneInputWidget);

		var _this = _possibleConstructorReturn(this, (AbPhoneInputWidget.__proto__ || Object.getPrototypeOf(AbPhoneInputWidget)).call(this));

		_this.state = { value: undefined };
		return _this;
	}

	_createClass(AbPhoneInputWidget, [{
		key: 'componentWillMount',
		value: function componentWillMount() {
			var value = this.props.value;

			if (value) {
				this.setState({ phone: value });
			} else {
				this.setState({ phone: undefined });
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props = this.props,
			    id = _props.id,
			    value = _props.value,
			    onChange = _props.onChange,
			    options = _props.options,
			    placeholder = _props.placeholder,
			    _props$required = _props.required,
			    required = _props$required === undefined ? false : _props$required,
			    _props$disabled = _props.disabled,
			    disabled = _props$disabled === undefined ? false : _props$disabled;
			var _options$autoFormat = options.autoFormat,
			    autoFormat = _options$autoFormat === undefined ? false : _options$autoFormat,
			    _options$onlyCountrie = options.onlyCountries,
			    onlyCountries = _options$onlyCountrie === undefined ? undefined : _options$onlyCountrie,
			    _options$excludeCount = options.excludeCountries,
			    excludeCountries = _options$excludeCount === undefined ? undefined : _options$excludeCount,
			    _options$preferredCou = options.preferredCountries,
			    preferredCountries = _options$preferredCou === undefined ? undefined : _options$preferredCou,
			    _options$regions = options.regions,
			    regions = _options$regions === undefined ? undefined : _options$regions,
			    _options$defaultCount = options.defaultCountry,
			    defaultCountry = _options$defaultCount === undefined ? undefined : _options$defaultCount,
			    _options$disableAreaC = options.disableAreaCodes,
			    disableAreaCodes = _options$disableAreaC === undefined ? false : _options$disableAreaC,
			    _options$disableCount = options.disableCountryCode,
			    disableCountryCode = _options$disableCount === undefined ? false : _options$disableCount,
			    _options$disableDropd = options.disableDropdown,
			    disableDropdown = _options$disableDropd === undefined ? false : _options$disableDropd,
			    _options$inputStyle = options.inputStyle,
			    inputStyle = _options$inputStyle === undefined ? { width: '100%' } : _options$inputStyle,
			    _options$buttonStyle = options.buttonStyle,
			    buttonStyle = _options$buttonStyle === undefined ? undefined : _options$buttonStyle,
			    _options$dropdownStyl = options.dropdownStyle,
			    dropdownStyle = _options$dropdownStyl === undefined ? undefined : _options$dropdownStyl;
			var phone = this.state.phone;

			var valueChanged = function valueChanged(value) {
				var sendData = value == null || value.toString().length === 0 ? undefined : value;
				_this2.setState({ phone: sendData });
				onChange(sendData);
			};
			return _react2.default.createElement(_reactPhoneInput2.default, {
				disabled: disabled,
				autoFormat: autoFormat,
				defaultCountry: defaultCountry,
				onChange: valueChanged,
				value: phone,
				id: id,
				required: required,
				onlyCountries: onlyCountries,
				excludeCountries: excludeCountries,
				preferredCountries: preferredCountries,
				regions: regions,
				disableAreaCodes: disableAreaCodes,
				disableCountryCode: disableCountryCode,
				inputStyle: inputStyle,
				buttonStyle: buttonStyle,
				dropdownStyle: dropdownStyle
			});
		}
	}]);

	return AbPhoneInputWidget;
}(_react.Component);

exports.default = AbPhoneInputWidget;