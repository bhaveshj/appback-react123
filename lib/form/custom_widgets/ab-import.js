'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AbImportWidget = function AbImportWidget(props) {
    var importUrl = props.importUrl,
        successCallback = props.successCallback,
        errorCallback = props.errorCallback;

    var processFile = function processFile(files) {
        var f = files[0];
        return new Promise(function (resolve, reject) {
            // const reader = new FileReader();
            // reader.onload = (event) => {
            //     let dataURL = event.target.result;
            //     dataURL = dataURL.replace(";base64", ";name=" + f.name + ";base64");
            //     resolve(dataURL);
            // }
            // reader.readAsDataURL(f);
            f;
        });
    };
    var importFile = function importFile(files) {
        var importFile = props.importFile,
            successCallback = props.successCallback,
            errorCallback = props.errorCallback;

        var file = files[0];
        importFile({ file: file, importUrl: importUrl, successCallback: successCallback, errorCallback: errorCallback });
    };
    return _react2.default.createElement('input', { type: 'file',
        accept: 'text/csv',
        onChange: function onChange(event) {
            return importFile(event.target.files);
        } });
};

AbImportWidget.propTypes = {
    importUrl: _react.PropTypes.string.isRequired,
    successCallback: _react.PropTypes.func,
    errorCallback: _react.PropTypes.func
};
exports.default = AbImportWidget;