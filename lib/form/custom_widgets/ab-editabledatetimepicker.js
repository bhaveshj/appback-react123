'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDatepicker = require('react-datepicker');

var _reactDatepicker2 = _interopRequireDefault(_reactDatepicker);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

require('./css/react-datepicker.css');

require('./css/datepicker-overrides.css');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// import 'react-datepicker/react-datepicker-cssmodules.css'


var AbEditableDateTimePickerWidget = function (_Component) {
	_inherits(AbEditableDateTimePickerWidget, _Component);

	function AbEditableDateTimePickerWidget(props) {
		_classCallCheck(this, AbEditableDateTimePickerWidget);

		var _this = _possibleConstructorReturn(this, (AbEditableDateTimePickerWidget.__proto__ || Object.getPrototypeOf(AbEditableDateTimePickerWidget)).call(this, props));

		_this.state = {
			actualDate: props.value && (0, _moment2.default)(props.value),
			parseDate: props.value
		};
		return _this;
	}

	_createClass(AbEditableDateTimePickerWidget, [{
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			this.setState({
				actualDate: nextProps.value && (0, _moment2.default)(nextProps.value),
				parseDate: nextProps.value
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props = this.props,
			    onChange = _props.onChange,
			    options = _props.options,
			    placeholder = _props.placeholder,
			    _props$required = _props.required,
			    required = _props$required === undefined ? false : _props$required,
			    _props$disabled = _props.disabled,
			    disabled = _props$disabled === undefined ? false : _props$disabled;
			var _options$displayDateF = options.displayDateFormat,
			    displayDateFormat = _options$displayDateF === undefined ? 'MM/DD/YYYY' : _options$displayDateF,
			    _options$clearable = options.clearable,
			    clearable = _options$clearable === undefined ? false : _options$clearable,
			    _options$showMonthDro = options.showMonthDropdown,
			    showMonthDropdown = _options$showMonthDro === undefined ? false : _options$showMonthDro,
			    _options$showYearDrop = options.showYearDropdown,
			    showYearDropdown = _options$showYearDrop === undefined ? false : _options$showYearDrop,
			    _options$dropdownMode = options.dropdownMode,
			    dropdownMode = _options$dropdownMode === undefined ? undefined : _options$dropdownMode,
			    _options$minDate = options.minDate,
			    minDate = _options$minDate === undefined ? undefined : _options$minDate,
			    _options$maxDate = options.maxDate,
			    maxDate = _options$maxDate === undefined ? undefined : _options$maxDate;

			var parseDate = function parseDate(dtValue) {
				var parseDateVal = dtValue != null ? _lodash2.default.clone((0, _moment2.default)(dtValue).toString()) : undefined;
				_this2.setState({
					actualDate: dtValue,
					parseDate: parseDateVal
				}, function () {
					onChange(parseDateVal);
				});
			};
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(_reactDatepicker2.default, {
					selected: this.state.actualDate,
					dateFormat: displayDateFormat,
					placeholderText: placeholder,
					isClearable: clearable,
					disabled: disabled,
					onChange: parseDate,
					onSelect: parseDate,
					required: required,
					showMonthDropdown: showMonthDropdown,
					showYearDropdown: showYearDropdown,
					dropdownMode: dropdownMode,
					minDate: minDate,
					maxDate: maxDate
				}),
				_react2.default.createElement('input', { type: 'hidden', value: this.state.parseDate })
			);
		}
	}]);

	return AbEditableDateTimePickerWidget;
}(_react.Component);

exports.default = AbEditableDateTimePickerWidget;