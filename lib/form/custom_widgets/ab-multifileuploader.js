'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _events = require('events');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _semanticUiReact = require('semantic-ui-react');

require('./css/ab-fileuploader.css');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _formHelper = require('../form-helper');

var _base64images = require('../../base64images');

var _async = require('async');

var _async2 = _interopRequireDefault(_async);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var mime = require('mime-types');

var getFileCategory = function getFileCategory(filePath) {
  var selectedFileCategory = mime.lookup(filePath);
  if (selectedFileCategory) {
    var selectedFileCategoryArray = selectedFileCategory.split('/')[0];
    if (selectedFileCategoryArray === 'application') {
      selectedFileCategoryArray = filePath.substring(filePath.lastIndexOf('.') + 1, filePath.length) || filePath;
    }
    return selectedFileCategoryArray;
  }
  return null;
};

var AbMultiFileUploaderWidget = function (_React$Component) {
  _inherits(AbMultiFileUploaderWidget, _React$Component);

  function AbMultiFileUploaderWidget(props) {
    _classCallCheck(this, AbMultiFileUploaderWidget);

    var _this = _possibleConstructorReturn(this, (AbMultiFileUploaderWidget.__proto__ || Object.getPrototypeOf(AbMultiFileUploaderWidget)).call(this, props));

    var value = props.value;

    var fileCategory = null;

    _this.proxy = new _events.EventEmitter();
    _this.state = {
      progress: -1,
      hasError: false,
      selectedFiles: null,
      selectedFilesCategory: null,
      signedUrlObjects: null,
      previewImgUrls: null,
      hasOtherError: false,
      errorMsg: null
    };
    _this.onFileSelect = _this.onFileSelect.bind(_this);
    _this.deleteFile = _this.deleteFile.bind(_this);
    _this.downloadFile = _this.downloadFile.bind(_this);
    return _this;
  }

  _createClass(AbMultiFileUploaderWidget, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.value && this.props.value.length > 0) {
        var files = this.props.value.split(',');
        var filesCategory = [];
        _lodash2.default.forEach(files, function (fileName, index) {
          var fileCategory = getFileCategory(fileName);
          filesCategory.push(fileCategory);
        });
        this.setState({
          selectedFilesCategory: filesCategory
        });
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var files = null;
      if (this.props.value !== nextProps.value && this.props.value !== null) {
        if (nextProps.value && nextProps.value.length > 0) {
          files = nextProps.value.split(',');
        }
        var filesCategory = [];
        _lodash2.default.forEach(files, function (fileName, index) {
          var fileCategory = getFileCategory(fileName);
          filesCategory.push(fileCategory);
        });
        this.setState({
          selectedFilesCategory: filesCategory
        });
      }
    }
  }, {
    key: 'onSubmit',
    value: function onSubmit(e) {
      var _this2 = this;

      var self = this;
      if (e) {
        e.preventDefault();
      }
      if (this.state.selectedFiles === null) {
        return;
      }
      var folderPath = this.props.options.folderPath;

      this.setState({
        progress: 0,
        hasError: false
      }, function () {
        var signedUrlObjects = [];
        _async2.default.eachSeries(_this2.state.selectedFiles, function (file, callback) {
          self.getSignedUrl(file, folderPath, function (error, data) {
            if (data) {
              signedUrlObjects.push(data);
            } else {
              signedUrlObjects.push(null);
            }
            callback();
          });
        }, function (err) {
          self.setState({ signedUrlObjects: signedUrlObjects }, function () {});
          self._doUpload(signedUrlObjects, self.state.selectedFiles);
        });
      });
    }
  }, {
    key: 'onFileSelect',
    value: function onFileSelect(e) {
      var _this3 = this;

      var self = this;
      var _props = this.props,
          id = _props.id,
          options = _props.options;
      var acceptFileType = options.acceptFileType,
          typeErrorMsg = options.typeErrorMsg,
          fileSize = options.fileSize,
          fileSizeErrorMsg = options.fileSizeErrorMsg,
          startUpLoadOnSelect = options.startUpLoadOnSelect,
          onFileChange = options.onFileChange;

      var selectedFiles = e.target.files;
      var isError = false;
      this.setState({
        hasOtherError: false,
        errorMsg: null
      }, function () {
        if (selectedFiles && selectedFiles.length > 0) {
          _lodash2.default.forEach(selectedFiles, function (file, index) {
            if (acceptFileType.filter((0, _formHelper.compareFileWithMimeType)(file.type)).length === 0) {
              isError = true;
              self.setState({
                hasOtherError: true,
                errorMsg: typeErrorMsg ? typeErrorMsg : 'Valid file type are ' + acceptFileType.join(',')
              });
              return false;
            } else if (fileSize) {
              var fileSizeInMb = file.size / 1048576;
              if (fileSizeInMb > fileSize) {
                isError = true;
                self.setState({
                  hasOtherError: true,
                  errorMsg: fileSizeErrorMsg ? fileSizeErrorMsg : 'Upload file having less then ' + fileSize + ' MB'
                });
                return false;
              }
            }
          });
          if (!isError) {
            if (selectedFiles && selectedFiles.length > 0) {
              onFileChange(selectedFiles);
              var selectedFilesCategory = self.state.selectedFilesCategory ? self.state.selectedFilesCategory : [];
              var previewImgUrls = self.props.value ? self.props.value.split(',') : [];
              _lodash2.default.forEach(selectedFiles, function (file, index) {
                var fileType = file.type.split('/');
                if (fileType[0] === 'application') {
                  fileType[0] = file.name.substring(file.name.lastIndexOf('.') + 1, file.name.length) || file.name;
                }
                selectedFilesCategory.push(fileType[0]);
                previewImgUrls.push(window.URL.createObjectURL(file));
              });
              _this3.setState({
                selectedFiles: selectedFiles,
                selectedFilesCategory: selectedFilesCategory,
                previewImgUrls: previewImgUrls
              }, function () {
                _lodash2.default.forEach(selectedFilesCategory, function (fileType, index) {
                  if (fileType === 'video') {
                    self.refs['video_' + index].load();
                  }
                });
                if (startUpLoadOnSelect) {
                  _this3.onSubmit();
                }
              });
            }
          }
        }
      });
    }
  }, {
    key: 'getSignedUrl',
    value: function getSignedUrl(file, folderPath, callback) {
      var appbackApi = this.props.options.appbackApi;

      var params = {
        key: folderPath + '/' + file.name.substr(0, file.name.lastIndexOf('.') - 1) + '_' + (0, _moment2.default)(Date.now()).format('YYYY-MM-DDTHH:mm:ss') + file.name.substr(file.name.lastIndexOf('.')),
        extension: file.name.substr(file.name.lastIndexOf('.')),
        contentType: file.type
      };
      appbackApi.getUploadUrl(params).then(function (data) {
        if (data && data.signedRequest) {
          callback(null, data);
        } else {
          callback(data, null);
        }
      }).catch(function (error) {
        console.error('Error from getUploadUrl', error);
        callback(error, null);
      });
    }
  }, {
    key: 'cancelUpload',
    value: function cancelUpload() {
      this.proxy.emit('abort');
      this.setState({
        progress: -1,
        hasError: false
      });
    }
  }, {
    key: 'progressRenderer',
    value: function progressRenderer(progress, totalFile, hasError, cancelHandler) {
      var message = null;
      if (hasError || progress > -1) {
        if (hasError) {
          message = _react2.default.createElement(
            'span',
            { style: { color: '#a94442' } },
            'Failed to upload ...'
          );
        }
        return _react2.default.createElement(
          _semanticUiReact.Progress,
          { value: progress, total: totalFile, progress: true, label: 'ratio', indicating: progress !== totalFile, error: hasError ? true : false, color: 'violet', success: progress === totalFile },
          message
        );
      }
      return '';
    }
  }, {
    key: 'previewFiles',
    value: function previewFiles(values, previewImgUrls, selectedFilesCategory, selectedFiles) {
      var _this4 = this;

      return selectedFilesCategory.map(function (selectedFileCategory, index) {
        var value = values ? values[index] : null;
        var previewImgUrl = previewImgUrls ? previewImgUrls[index] : null;
        // let selectedFileCategory = selectedFilesCategory[index];
        var selectedFile = selectedFiles ? selectedFiles[index] : null;
        return _react2.default.createElement(
          'span',
          { key: 'span_' + index, style: { padding: '5px' } },
          (value || previewImgUrl) && selectedFileCategory === 'image' && _react2.default.createElement(_semanticUiReact.Image, { src: value && !previewImgUrl ? '' + value : previewImgUrl, size: 'tiny', shape: 'rounded' }),
          (value || previewImgUrl) && selectedFileCategory === 'video' && _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'video',
              { id: 'video_' + index, width: '200', ref: 'video_' + index },
              _react2.default.createElement('source', { src: value && !previewImgUrl ? '' + value : previewImgUrl, type: 'video/mp4' }),
              'Your browser does not support HTML5 video.'
            )
          ),
          (value || previewImgUrl) && selectedFileCategory === 'pdf' && _react2.default.createElement(_semanticUiReact.Image, { src: _base64images.pdfFile, size: 'tiny', shape: 'rounded' }),
          (value || previewImgUrl) && (selectedFileCategory === 'xlsx' || selectedFile === 'xls' || selectedFileCategory === 'csv') && _react2.default.createElement(_semanticUiReact.Image, { src: _base64images.xlsFile, size: 'tiny', shape: 'rounded' }),
          (value || previewImgUrl) && (selectedFileCategory === 'docx' || selectedFile === 'doc') && _react2.default.createElement(_semanticUiReact.Image, { src: _base64images.docFile, size: 'tiny', shape: 'rounded' }),
          (value || previewImgUrl) && (selectedFileCategory === 'pptx' || selectedFile === 'ppt') && _react2.default.createElement(_semanticUiReact.Image, { src: _base64images.pptFile, size: 'tiny', shape: 'rounded' }),
          value && !previewImgUrl && _react2.default.createElement(
            _semanticUiReact.Button.Group,
            { basic: true, size: 'small', color: 'violet' },
            _react2.default.createElement(_semanticUiReact.Button, { icon: 'download', onClick: function onClick(e) {
                return _this4.downloadFile(e, value);
              } }),
            _react2.default.createElement(_semanticUiReact.Button, { icon: 'delete', onClick: function onClick(e) {
                return _this4.deleteFile(e, index);
              } })
          )
        );
      });
    }
  }, {
    key: 'deleteFile',
    value: function deleteFile(e, index) {
      if (e) {
        e.preventDefault();
      }
      if (this.props.value) {
        var valueArray = this.props.value.split(',');
        valueArray.splice(index, 1);
        var filesString = valueArray.join(',');
        this.props.onChange(filesString.length > 0 ? filesString : undefined);
      }
    }
  }, {
    key: 'downloadFile',
    value: function downloadFile(e, url) {
      if (e) {
        e.preventDefault();
      }
      window.open(url);
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          previewImgUrls = _state.previewImgUrls,
          hasOtherError = _state.hasOtherError,
          errorMsg = _state.errorMsg,
          selectedFilesCategory = _state.selectedFilesCategory,
          selectedFiles = _state.selectedFiles;
      var _props2 = this.props,
          value = _props2.value,
          options = _props2.options,
          id = _props2.id;
      var inputRef = options.inputRef,
          buttonRef = options.buttonRef,
          _options$showProgress = options.showProgress,
          showProgress = _options$showProgress === undefined ? true : _options$showProgress,
          acceptFileType = options.acceptFileType;
      // const formElement = this.props.formRenderer(this.onSubmit.bind(this), this.onFileSelect);

      var progressElement = null;
      if (showProgress) {
        var totalFile = -1;
        if (this.state.selectedFiles && this.state.selectedFiles.length > 0) {
          totalFile = this.state.selectedFiles.length;
        }
        progressElement = this.progressRenderer(this.state.progress, totalFile, this.state.hasError, this.cancelUpload.bind(this));
      }

      return _react2.default.createElement(
        'div',
        { className: 'form-control customClass' },
        _react2.default.createElement('input', { type: 'file', name: 'file', onChange: this.onFileSelect, ref: inputRef, accept: acceptFileType.join(','), multiple: true }),
        _react2.default.createElement('input', { type: 'button', style: { display: 'none' }, ref: buttonRef, onClick: this.onSubmit.bind(this) }),
        hasOtherError && _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            'ul',
            { className: 'error-detail bs-callout bs-callout-info' },
            _react2.default.createElement(
              'li',
              { className: 'text-danger' },
              errorMsg
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { id: 'progress-container' },
          progressElement
        ),
        _react2.default.createElement(
          'div',
          { className: 'preview-container' },
          selectedFilesCategory && this.previewFiles(value ? value.split(',') : null, previewImgUrls, selectedFilesCategory, selectedFiles)
        )
      );
    }
  }, {
    key: '_getFormData',
    value: function _getFormData() {
      if (this.props.formGetter) {
        return this.props.formGetter();
      }
      return this.state.selectedFiles;
    }
  }, {
    key: '_doUpload',
    value: function _doUpload(signedObjs, selectedFiles) {
      var self = this;
      var finalFilesUrl = [];
      if (this.props.value && this.props.value.length > 0) {
        finalFilesUrl = this.props.value.split(',');
      }
      _async2.default.eachOfSeries(signedObjs, function (signObj, index, callback) {
        var form = selectedFiles[index];
        var url = signObj.signedRequest;
        var fileObj = selectedFiles[index];
        var req = new XMLHttpRequest();
        req.open('PUT', url);
        req.setRequestHeader('X-Amz-ACL', 'public-read');
        req.setRequestHeader('Content-Type', fileObj.type);
        var options = self.props.options;


        req.addEventListener('load', function (e) {
          self.proxy.removeAllListeners(['abort']);
          var newState = { progress: self.state.progress + 1 };
          if (req.status >= 200 && req.status <= 299) {

            // options.onUpload(signObj.url)
            finalFilesUrl.push(signObj.url);
            // onChange(this.state.signedUrlObject.url)
            self.setState(newState, function () {
              // options.onLoad(e, req);
            });
          } else {
            newState.hasError = true;
            self.setState(newState, function () {
              options.onError(e, req);
            });
          }
          callback();
        }, false);

        req.addEventListener('error', function (e) {
          self.setState({
            hasError: true
          }, function () {
            options.onError(e, req);
          });
        }, false);

        req.upload.addEventListener('progress', function (e) {
          var progress = 0;
          if (e.total !== 0) {
            progress = parseInt(e.loaded / e.total * 100, 10);
          }
          // this.setState({
          //   progress
          // }, () => {
          //   options.onProgress(e, req, progress);
          // });
        }, false);

        req.addEventListener('abort', function (e) {
          self.setState({
            progress: -1
          }, function () {
            options.onAbort(e, req);
          });
        }, false);

        self.proxy.once('abort', function () {
          req.abort();
        });

        self.props.beforeSend(req).send(form);
      }, function (err) {
        var _self$props = self.props,
            onChange = _self$props.onChange,
            options = _self$props.options;

        if (err) {
          console.error(err.message);
          return false;
        }
        options.onUpload(finalFilesUrl);
        var filesString = finalFilesUrl.join(',');
        onChange(filesString.length > 0 ? filesString : undefined);
      });
    }
  }]);

  return AbMultiFileUploaderWidget;
}(_react2.default.Component);

AbMultiFileUploaderWidget.propTypes = {
  url: _propTypes2.default.string,
  formGetter: _propTypes2.default.func,
  formRenderer: _propTypes2.default.func,
  progressRenderer: _propTypes2.default.func,
  formCustomizer: _propTypes2.default.func,
  beforeSend: _propTypes2.default.func,
  onProgress: _propTypes2.default.func,
  onLoad: _propTypes2.default.func,
  onError: _propTypes2.default.func,
  onAbort: _propTypes2.default.func,
  onChange: _propTypes2.default.func
};

AbMultiFileUploaderWidget.defaultProps = {
  formRenderer: function formRenderer(onSubmit, onFileSelect) {
    return _react2.default.createElement(
      'form',
      { className: '_react_fileupload_form_content', ref: 'form', method: 'post', onSubmit: onSubmit },
      _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement('input', { type: 'file', name: 'file', onChange: onFileSelect, ref: function ref(_ref) {
            return undefined.inputRef = _ref;
          } })
      ),
      _react2.default.createElement('input', { type: 'submit' })
    );
  },
  formCustomizer: function formCustomizer(form) {
    return form;
  },
  beforeSend: function beforeSend(request) {
    return request;
  },
  options: {
    onProgress: function onProgress(e, request, progress) {},
    onLoad: function onLoad(e, request) {},
    onError: function onError(e, request) {},
    onAbort: function onAbort(e, request) {},
    onUpload: function onUpload(url) {},
    onFileChange: function onFileChange(file) {},
    folderPath: 'other',
    acceptFileType: ['image/*'],
    fileSize: 2,
    startUpLoadOnSelect: true
  }
};

exports.default = AbMultiFileUploaderWidget;