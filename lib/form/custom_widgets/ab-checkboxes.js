'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require('semantic-ui-react');

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function selectValue(value, selected, all) {
    var at = all.indexOf(value);
    var updated = selected.slice(0, at).concat(value, selected.slice(at));
    // As inserting values at predefined index positions doesn't work with empty
    // arrays, we need to reorder the updated selection to match the initial order
    return updated.sort(function (a, b) {
        return all.indexOf(a) > all.indexOf(b);
    });
}

function deselectValue(value, selected) {
    return selected.filter(function (v) {
        return v !== value;
    });
}

function AbCheckboxesWidget(props) {
    var id = props.id,
        disabled = props.disabled,
        options = props.options,
        value = props.value,
        autofocus = props.autofocus,
        _onChange = props.onChange;
    var enumOptions = options.enumOptions,
        inline = options.inline,
        checkboxType = options.checkboxType,
        _options$radio = options.radio,
        radio = _options$radio === undefined ? false : _options$radio;

    return _react2.default.createElement(
        'div',
        { className: inline ? 'fields inline' : 'fields grouped', id: id },
        enumOptions.map(function (option, index) {
            var checked = value.indexOf(option.value) !== -1;
            var checkbox = _react2.default.createElement(
                'div',
                { key: index, className: "field" },
                _react2.default.createElement(_semanticUiReact.Checkbox, {
                    id: id + '_' + index,
                    checked: checked,
                    radio: radio,
                    slider: _constants.ABCHECKBOXTYPE.SLIDER === checkboxType,
                    toggle: _constants.ABCHECKBOXTYPE.TOGGLE === checkboxType,
                    label: _react2.default.createElement(
                        'label',
                        null,
                        option.label
                    ),
                    disabled: disabled,
                    autoFocus: autofocus && index === 0,
                    onChange: function onChange(event, data) {
                        var all = enumOptions.map(function (_ref) {
                            var value = _ref.value;
                            return value;
                        });
                        if (data.checked) {
                            _onChange(selectValue(option.value, value, all));
                        } else {
                            _onChange(deselectValue(option.value, value));
                        }
                    } })
            );
            return checkbox;
        })
    );
}

AbCheckboxesWidget.defaultProps = {
    autofocus: false,
    options: {
        inline: false
    }
};

AbCheckboxesWidget.propTypes = {
    schema: _react.PropTypes.object.isRequired,
    id: _react.PropTypes.string.isRequired,
    options: _react.PropTypes.shape({
        enumOptions: _react.PropTypes.array,
        inline: _react.PropTypes.bool
    }).isRequired,
    value: _react.PropTypes.any,
    required: _react.PropTypes.bool,
    disabled: _react.PropTypes.bool,
    multiple: _react.PropTypes.bool,
    autofocus: _react.PropTypes.bool,
    onChange: _react.PropTypes.func
};

exports.default = AbCheckboxesWidget;