'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _gridContainer = require('./containers/grid-container');

Object.defineProperty(exports, 'AbModelGrid', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_gridContainer).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }