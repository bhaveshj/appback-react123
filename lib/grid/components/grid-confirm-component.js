'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

require('react-select/dist/react-select.css');

var _semanticUiReact = require('semantic-ui-react');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GridConfirmComponent = function (_React$Component) {
    _inherits(GridConfirmComponent, _React$Component);

    function GridConfirmComponent(props) {
        _classCallCheck(this, GridConfirmComponent);

        var _this = _possibleConstructorReturn(this, (GridConfirmComponent.__proto__ || Object.getPrototypeOf(GridConfirmComponent)).call(this, props));

        _this.state = {
            "showModal": false
        };
        _this.showModal = _this.showModal.bind(_this);
        _this.confirmBtnCallback = _this.confirmBtnCallback.bind(_this);
        _this.canceBtnCallback = _this.canceBtnCallback.bind(_this);
        return _this;
    }

    _createClass(GridConfirmComponent, [{
        key: 'showModal',
        value: function showModal() {
            this.setState({ showModal: true });
        }
    }, {
        key: 'confirmBtnCallback',
        value: function confirmBtnCallback() {
            var _props = this.props,
                confirmCallback = _props.confirmCallback,
                modalOptions = _props.modalOptions;

            confirmCallback();
            if (modalOptions) {
                var confirmButton = modalOptions.confirmButton;

                if (confirmButton && confirmButton.callback) {
                    confirmButton.callback("confirm");
                }
            }
            this.setState({ showModal: false });
        }
    }, {
        key: 'canceBtnCallback',
        value: function canceBtnCallback() {
            var modalOptions = this.props.modalOptions;

            if (modalOptions) {
                var cancelButton = modalOptions.cancelButton;

                if (cancelButton && cancelButton.callback) {
                    cancelButton.callback("cancel");
                }
            }
            this.setState({ showModal: false });
        }
    }, {
        key: 'render',
        value: function render() {
            var self = this;
            var _props2 = this.props,
                index = _props2.index,
                triggerProps = _props2.triggerProps,
                modalOptions = _props2.modalOptions;
            var icon = triggerProps.icon;

            var yesBtnColor = 'green';
            var noBtnColor = 'red';
            var header = null;
            var content = null;
            var confirmButton = null;
            var cancelButton = null;
            if (modalOptions) {
                header = modalOptions.header;
                content = modalOptions.content;
                confirmButton = modalOptions.confirmButton;
                cancelButton = modalOptions.cancelButton;
                yesBtnColor = modalOptions.yesBtnColor;
                noBtnColor = modalOptions.noBtnColor;
            }
            return _react2.default.createElement(
                _semanticUiReact.Modal,
                { size: 'small', open: self.state.showModal, trigger: _react2.default.createElement(_semanticUiReact.Popup, { key: index,
                        trigger: _react2.default.createElement(_semanticUiReact.Button, { icon: icon, onClick: self.showModal }),
                        content: 'Delete'
                    }) },
                header && _react2.default.createElement(
                    _semanticUiReact.Modal.Header,
                    null,
                    header
                ),
                _react2.default.createElement(
                    _semanticUiReact.Modal.Content,
                    null,
                    content ? _react2.default.createElement(
                        'p',
                        null,
                        content
                    ) : _react2.default.createElement(
                        'p',
                        null,
                        'Are you sure want to delete it?'
                    )
                ),
                _react2.default.createElement(
                    _semanticUiReact.Modal.Actions,
                    null,
                    _react2.default.createElement(
                        _semanticUiReact.Button,
                        { color: yesBtnColor, onClick: function onClick() {
                                return self.confirmBtnCallback();
                            } },
                        _react2.default.createElement(_semanticUiReact.Icon, { name: confirmButton && confirmButton.icon ? confirmButton.icon : 'checkmark' }),
                        ' ',
                        confirmButton && confirmButton.text ? confirmButton.text : 'Yes'
                    ),
                    _react2.default.createElement(
                        _semanticUiReact.Button,
                        { color: noBtnColor, onClick: function onClick() {
                                return self.canceBtnCallback();
                            } },
                        _react2.default.createElement(_semanticUiReact.Icon, { name: cancelButton && cancelButton.icon ? cancelButton.icon : 'remove' }),
                        ' ',
                        cancelButton && cancelButton.text ? cancelButton.text : 'No'
                    )
                )
            );
        }
    }]);

    return GridConfirmComponent;
}(_react2.default.Component);

GridConfirmComponent.contextTypes = {
    triggerProps: _react.PropTypes.object,
    index: _react.PropTypes.any,
    confirmCallback: _react.PropTypes.func,
    cancelCallback: _react.PropTypes.func,
    modalOptions: _react.PropTypes.object
};
exports.default = GridConfirmComponent;