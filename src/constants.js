// ------------------------------------
// Constants
// ------------------------------------

export const MODELS_DATA_REQUEST_SUCCESS ='MODELS_DATA_REQUEST_SUCCESS'
export const SET_MODEL_INITIAL_STATE = 'SET_MODEL_INITIAL_STATE'
export const SET_CURRENT_ACTIVE_ITEM = 'SET_CURRENT_ACTIVE_ITEM'
export const SAVE_MODEL_FORM_DATA = 'SAVE_MODEL_FORM_DATA'
export const SAVE_MODEL_FORM_DATA_SUCCESS = 'SAVE_MODEL_FORM_DATA_SUCCESS'
export const CREATE_MODEL_FORM_DATA = 'CREATE_MODEL_FORM_DATA'
export const CREATE_MODEL_FORM_DATA_SUCCESS = 'CREATE_MODEL_FORM_DATA_SUCCESS'
export const CLEAR_FORM_DATA = 'CLEAR_FORM_DATA'
export const MERGE_GRID_CONFIG = "MERGE_GRID_CONFIG"
export const RESET_GRID_DATA_ONLY = "RESET_GRID_DATA_ONLY"
export const SHOW_LOADER = 'SHOW_LOADER'
export const HIDE_LOADER = 'HIDE_LOADER'

export const COLUMNTYPE = {
    BOOL:"bool",
    DATE:"date",
    ACTIONS:"actions",
    LINK:"link"
}

export const ACTIONBUTTON = {
    VIEW:"view",
    EDIT:"edit",
    DELETE: "delete",
    CREATE:"create",
}

export const ABCHECKBOXTYPE = {
    TOGGLE:"toggle",
    SLIDER:"slider"
}

export const ABSELECT = {
    START:"start",
    ANY:"any"
}

export const GRID_DATA_SOURCE_TYPE = {
    MODEL:"model",
    METHOD:"method"
}

export const FORM_CONDITION_TYPE={
    "DEPENDENCY":"dependency",
    "VISIBILITY":"visibility"
}

export const FORM_CONDITION_ACTION={
    "HIDDEN":"hidden",
    "DISABLED":"disabled",
    "ENABLED":"enabled"
}

export const SELECT_MULTI_OPTIONS={
    ARRAY:"ARRAY",
    STRING:"STRING"
}

export const  FORM_DATA_SOURCE_OPTIONS_TYPE={
    CREATE:"CREATE",
    EDIT:"EDIT",
    LOAD:"LOAD"
}

export  const FORM_UTILS = {
    DELIMITER:','
}