import {
  MODELS_DATA_REQUEST_SUCCESS,
  SET_MODEL_INITIAL_STATE,
  SET_CURRENT_ACTIVE_ITEM,
  SAVE_MODEL_FORM_DATA_SUCCESS,
  CREATE_MODEL_FORM_DATA_SUCCESS,
  CLEAR_FORM_DATA,
  ACTIONBUTTON,
  MERGE_GRID_CONFIG,
  RESET_GRID_DATA_ONLY,
  SHOW_LOADER,
  HIDE_LOADER
} from "./constants";
import _ from 'underscore';

const initialState = {
  "results": [],
  "recordCount": 0,
  "currentPage": 0,
  "maxPages": 0,
  "resultsPerPage": 10,
  "externalSortColumn": "createdAt",
  "externalSortAscending": true,
  "externalFilter": "",
  "advanceFilter": {},
  "advanceFilterConfig": [],
  "filterCols": [],
  "includes": [],
  "form": {
    "currentActiveItem": {},
    "action": ACTIONBUTTON.CREATE
  },
  "showLoader": false,
  "exportData": false,
  "exportConfig": null
}

// ------------------------------------
// Reducer handler functions
// ------------------------------------

const showHideLoader = (state, action, loaderDisplayStatus) => {
  let { payload } = action;
  if (payload && payload.AbModelName) {
    let { AbModelName } = payload
    return Object.assign({}, state, { [AbModelName]: { ...state[AbModelName], ...{ showLoader: loaderDisplayStatus } } });
  } else {
    return state;
  }
}

const setModelsData = (state, action) => {
  // const modelData = _.pluck(action.payload, ["results", "" ]);
  const { results, currentPage, maxPages, recordCount, resultsPerPage, externalSortColumn, externalSortAscending, externalFilter, modelName, filters, includes, advanceFilter } = action.payload;
  let modelData = {
    results: results,
    currentPage: currentPage,
    maxPages: maxPages,
    recordCount: recordCount,
    resultsPerPage: resultsPerPage,
    externalSortColumn: externalSortColumn,
    externalSortAscending: externalSortAscending,
    externalFilter: externalFilter,
    advanceFilter: advanceFilter,
    filterCols: filters,
    includes: includes
  }
  let finalModelData = { ...state[modelName], ...modelData };
  return Object.assign({}, state, { [modelName]: finalModelData });
}

const setModelIntialData = (state, action) => {
  if (state[action.payload.modelName]) {
    return state;
  } else {
    let { initialConfigData } = action.payload;
    return Object.assign({}, state, { [action.payload.modelName]: { ...initialState, ...initialConfigData } });
  }
}

const setCurrentActiveItem = (state, action) => {
  let currentStateData = state[action.payload.modelName];
  currentStateData.form = { ...currentStateData.form, ...action.payload.form }
  return Object.assign({}, state, { [action.payload.modelName]: { ...currentStateData } });
}

const saveModelFormDataSuccess = (state, action) => {
  const { modelName, data, saveCallback } = action.payload
  let latestData = _.map(state[modelName].results, function (item) {
    if (item.id === data.id) {
      return { ...item, ...data };
    } else {
      return item
    }
  });
  let finalstate = { ...state[modelName], ...{ results: latestData, ...{ form: { currentActiveItem: {}, action: ACTIONBUTTON.CREATE } } } }
  let actionType = state[modelName].form.action;
  // if(saveCallback){
  //   saveCallback(actionType,data);
  // }
  return Object.assign({}, state, { [modelName]: finalstate });

}

const createModelFormDataSuccess = (state, action) => {
  const { modelName, data, saveCallback } = action.payload;
  let finalstate = { ...state[modelName], ...{ form: { currentActiveItem: {}, action: ACTIONBUTTON.CREATE } } }
  let actionType = state[modelName].form.action;
  // if(saveCallback){
  //   saveCallback(actionType,data);
  // }
  return Object.assign({}, state, { [modelName]: finalstate });
}

const clearFormData = (state, action) => {
  const { modelName } = action.payload;
  let finalState = state[modelName];
  finalState.form.currentActiveItem = {};
  finalState.form.action = ACTIONBUTTON.CREATE
  return Object.assign({}, state, { [modelName]: finalState });
}


const mergeGridConfig = (state, action) => {
  const { modelName, data } = action.payload;
  let finalState = state[modelName];
  finalState = { ...finalState, ...data };
  return Object.assign({}, state, { [modelName]: { ...finalState } });
}

const resetGridInitialData = (state, action) => {
  const { modelName } = action.payload;
  if (state[modelName]) {
    let gridInitialData = { ...initialState };
    delete gridInitialData["form"];
    return Object.assign({}, state, { [modelName]: { ...state[modelName], ...gridInitialData } });
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [MODELS_DATA_REQUEST_SUCCESS]: (state, action) => setModelsData(state, action),
  [SET_MODEL_INITIAL_STATE]: (state, action) => setModelIntialData(state, action),
  [SET_CURRENT_ACTIVE_ITEM]: (state, action) => setCurrentActiveItem(state, action),
  [SAVE_MODEL_FORM_DATA_SUCCESS]: (state, action) => saveModelFormDataSuccess(state, action),
  [CREATE_MODEL_FORM_DATA_SUCCESS]: (state, action) => createModelFormDataSuccess(state, action),
  [CLEAR_FORM_DATA]: (state, action) => clearFormData(state, action),
  [MERGE_GRID_CONFIG]: (state, action) => mergeGridConfig(state, action),
  [RESET_GRID_DATA_ONLY]: (state, action) => resetGridInitialData(state, action),
  [SHOW_LOADER]: (state, action) => showHideLoader(state, action, true),
  [HIDE_LOADER]: (state, action) => showHideLoader(state, action, false)
}


// ------------------------------------
// Reducer
// ------------------------------------
export default function modelsReducer(state = {}, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}