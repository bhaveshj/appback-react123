import React, { PropTypes } from "react";
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { Grid, Input, Label, Segment, Button } from 'semantic-ui-react';

class GridFilterComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "results": []
        }
    }

    searchChange = (e, filter) => {
        let { changeFilter } = this.props;
        changeFilter(filter.value);
        // this.setState({ commonFilter: filter.value });
    }

    componentWillMount() {
        let { abModelData } = this.props;
        let { modelName } = this.context;
        if (abModelData && modelName) {
            let intialFilterConfig = abModelData[modelName]["advanceFilterConfig"];
            if (intialFilterConfig) {
                this.setState({
                    results: intialFilterConfig
                })
            }

        }
    }
    componentDidMount() {
        this.onFilterClick();
    }
    onFilterClick = (e, data) => {
        var advancedFilterObj = {};
        let { changeFilter } = this.props;
        let { results } = this.state
        let filters = results.map((filterCol, i) => {
            if (filterCol) {
                let { multiSelect, columnName, value } = filterCol;
                if (value && value.length > 0) {
                    if (multiSelect) {
                        advancedFilterObj[columnName] = {
                            "inq": value.split(",")
                        }
                    } else {
                        advancedFilterObj[columnName] = value;
                    }
                }
            }
        });
        changeFilter(advancedFilterObj);
    }

    onChange(val, index, advanceFilter) {
        let tempResults = this.state.results;
        if (!tempResults[index]) {
            advanceFilter["value"] = val
            tempResults[index] = advanceFilter
        } else {
            tempResults[index]["value"] = val;
        }
        this.setState(
            {
                results: [...tempResults]
            }
        )
        this.onFilterClick();
    }
    render() {
        let self = this;
        let { externalFilter, abModelData } = this.props;
        let advanceFilter = abModelData[this.context.modelName]["advanceFilterConfig"];
        let { results, commonFilter } = this.state;
        return (
            <Grid stackable style={{ "marginBottom": 5 }}>
                <Grid.Row columns={3}>
                    <Grid.Column>
                        <Input fluid icon='search' placeholder='Search...' onChange={self.searchChange.bind(this)} value={externalFilter} />
                    </Grid.Column>
                    {(advanceFilter && advanceFilter.length > 0) ? (
                        advanceFilter.map((filter, i) =>
                            <Grid.Column key={i}>
                                <Select multi={filter.multiSelect} clearable={filter.clearable} placeholder={filter.placeholder} options={filter.dataSet} simpleValue valueKey={filter.valueKey} labelKey={filter.labelKey} onChange={(val) => { self.onChange(val, i, filter); filter.onChange && filter.onChange(val) }} value={(results && results[i]) ? results[i].value : null} />
                            </Grid.Column>
                        )
                    ) : null}
                </Grid.Row>
            </Grid>
        )
    }
}

GridFilterComponent.contextTypes = {
    modelName: PropTypes.string
}
export default GridFilterComponent;

// {/*<Grid.Row>
//     {advanceFilter.map((filter, i) =>
//         <Grid.Column width={6} key={i}>
//             <Select multi={true} options={filter.dataSet} simpleValue clearable={true} valueKey="value" labelKey="label" onChange={(val) => self.onChange(val, i)} value={results[i]} />
//         </Grid.Column>
//     )}
// </Grid.Row>   */}


/*
    render() {
        let self = this;
        let { externalFilter, abModelData} = this.props;
        let advanceFilter = abModelData[this.context.modelName]["advanceFilterConfig"];
        let {results, commonFilter} = this.state;
        return (
            <Grid columns='equal' style={{ "marginBottom": 5 }}>
                <Grid.Row>
                    <Grid.Column width={3}>
                        <Input icon='search' placeholder='Search...' onChange={self.searchChange.bind(this)} value={externalFilter} />
                    </Grid.Column>
                </Grid.Row>
                {(advanceFilter && advanceFilter.length > 0) ? (<Grid.Row>
                    <Grid.Column>
                        <Segment>
                            <Label attached='top'>Advance Filter</Label>
                            <Grid columns='equal'>
                                <Grid.Row>
                                    {advanceFilter.map((filter, i) =>
                                        <Grid.Column width={6} key={i}>
                                            <Select multi={filter.multiSelect} clearable={filter.clearable} placeholder={filter.placeholder} options={filter.dataSet} simpleValue clearable={true} valueKey={filter.valueKey} labelKey={filter.labelKey} onChange={(val) => self.onChange(val, i, filter)} value={(results && results[i]) ? results[i].value : null} />
                                        </Grid.Column>
                                    )}
                                    <Grid.Column>
                                        <Button content='Filter' icon='filter' labelPosition='left' onClick={(e) => self.onFilterClick(e)} />
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>) : null}
            </Grid>
        )
    }*/