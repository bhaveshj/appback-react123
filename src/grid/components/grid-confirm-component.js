import React, { PropTypes } from "react";
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { Header, Button, Modal, Icon, Popup } from 'semantic-ui-react';

class GridConfirmComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "showModal": false
        }
        this.showModal = this.showModal.bind(this);
        this.confirmBtnCallback = this.confirmBtnCallback.bind(this);
        this.canceBtnCallback = this.canceBtnCallback.bind(this);
    }

    showModal() {
        this.setState({ showModal: true });
    }

    confirmBtnCallback() {
        const { confirmCallback, modalOptions } = this.props;
        confirmCallback();
        if (modalOptions) {
            let { confirmButton } = modalOptions;
            if (confirmButton && confirmButton.callback) {
                confirmButton.callback("confirm");
            }
        }
        this.setState({ showModal: false });
    }

    canceBtnCallback() {
        const { modalOptions } = this.props;
        if (modalOptions) {
            let { cancelButton } = modalOptions;
            if (cancelButton && cancelButton.callback) {
                cancelButton.callback("cancel");
            }
        }
        this.setState({ showModal: false });
    }

    render() {
        let self = this;
        const { index, triggerProps, modalOptions } = this.props;
        const { icon } = triggerProps;
        let yesBtnColor = 'green';
        let noBtnColor = 'red';
        let header = null;
        let content = null;
        let confirmButton = null;
        let cancelButton = null;        
        if (modalOptions) {
            header = modalOptions.header;
            content = modalOptions.content;
            confirmButton = modalOptions.confirmButton;
            cancelButton = modalOptions.cancelButton;
            yesBtnColor = modalOptions.yesBtnColor;
            noBtnColor = modalOptions.noBtnColor;
        }
        return (
            <Modal size='small' open={self.state.showModal} trigger={<Popup key={index}
                trigger={<Button icon={icon} onClick={self.showModal} />}
                content='Delete'
            />}>
                {
                    (header) && <Modal.Header>{header}</Modal.Header>
                }
                <Modal.Content>
                    {(content) ? <p>{content}</p> : <p>Are you sure want to delete it?</p>}
                </Modal.Content>
                <Modal.Actions>
                    <Button color={yesBtnColor} onClick={() => self.confirmBtnCallback()}>
                        <Icon name={(confirmButton && confirmButton.icon) ? confirmButton.icon : 'checkmark'} /> {(confirmButton && confirmButton.text) ? confirmButton.text : 'Yes'}
                    </Button>
                    <Button color={noBtnColor} onClick={() => self.canceBtnCallback()}>
                        <Icon name={(cancelButton && cancelButton.icon) ? cancelButton.icon : 'remove'} /> {(cancelButton && cancelButton.text) ? cancelButton.text : 'No'}
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

GridConfirmComponent.contextTypes = {
    triggerProps: PropTypes.object,
    index: PropTypes.any,
    confirmCallback: PropTypes.func,
    cancelCallback: PropTypes.func,
    modalOptions: PropTypes.object
}
export default GridConfirmComponent;
