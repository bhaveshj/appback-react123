import React, { Component } from 'react';
import { connect } from 'react-redux';
import GridFilterComponent from '../components/grid-filter-component';
import * as AbModelGridAction from '../../actions';
import { GRID_DATA_SOURCE_TYPE } from '../../constants';


const mapStateToProps = (state,ownProps) => {
  const { appbackApi, modelName} = ownProps;
  var mapped = {
      abModelData:state.abModels,
  };
  return mapped;
}
function mapDispatchToProps(dispatch,ownProps) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GridFilterComponent);