import React, { Component, PropTypes } from 'react';
import { Label, Button, Segment, Grid } from 'semantic-ui-react';
import './ArrayField.css';
export default function CompactArrayFieldTemplate(props) {
    let { theme } = props;
    const ArrayFieldItem = (element) => {
        let segmentTitle = element.index + 1;
        let { children } = element;
        if (children) {
            if (children.props && children.props.schema && children.props.schema.segmentTitle) {
                segmentTitle = children.props.schema.segmentTitle;
            }
        }
        return <Grid columns="equal" key={element.index}>
            <Grid.Row className='padding-0'>
                <Grid.Column>
                    <div>{element.children}</div>
                </Grid.Column>
                {(element.hasRemove || element.hasMoveDown || element.hasMoveUp) && <Grid.Column width="1" textAlign='right' className='margin-top-4'>
                    {element.hasRemove &&
                        <Button onClick={element.onDropIndexClick(element.index)} basic size="small" type='button' icon='trash' color="red" />}
                    <Button.Group basic size='small'>
                        {element.hasMoveDown &&
                            <Button onClick={element.onReorderClick(element.index, element.index + 1)} type='button' icon="arrow down" />}
                        {element.hasMoveUp &&
                            <Button onClick={element.onReorderClick(element.index, element.index - 1)} type='button' icon="arrow up" />}
                    </Button.Group>
                </Grid.Column>}
            </Grid.Row>
        </Grid>
    }

    return (
        <div className={props.className}>
            <label className='padding-bottom-10'>{props.title}</label>
            {props.items && props.items.map(ArrayFieldItem)}
            {props.canAdd && <Button onClick={props.onAddClick} color={(theme.color) ? theme.color : null} basic icon="plus" content="Add" size="tiny" className='margin-top-4' />}
        </div>
    );
}