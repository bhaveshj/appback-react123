import React, { Component } from 'react';
import { connect } from 'react-redux';
import AbImportWidget from '../ab-import';
import * as AbModelAction from '../../../actions';

const mapStateToProps = (state, ownProps) => {
//   const { appbackApi, importModel} = ownProps.options;
  var mapped = {
    // AbModelInstance: appbackApi.createModelRest(importModel, null)
  };
  return mapped;
}
function mapDispatchToProps(dispatch, ownProps) {
  return {
    importFile: function (payloadData) {
    //   const { importModel } = ownProps.options;
    //   payloadData["AbModelName"] = importModel;
      return dispatch(AbModelAction.importFile(payloadData));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AbImportWidget);