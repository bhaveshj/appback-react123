
import { EventEmitter } from 'events';
import React from 'react';
import PropTypes from 'prop-types';
import { Progress, Image } from 'semantic-ui-react';
import './css/ab-fileuploader.css';
import moment from 'moment';
import { compareFileWithMimeType } from '../form-helper'
import { xlsFile, pdfFile, docFile, pptFile } from '../../base64images'
const mime = require('mime-types')

const getFileCategory = (filePath) => {
  let selectedFileCategory = mime.lookup(filePath);
  if (selectedFileCategory) {
    let selectedFileCategoryArray = selectedFileCategory.split('/')[0];
    if (selectedFileCategoryArray === 'application') {
      selectedFileCategoryArray = filePath.substring(filePath.lastIndexOf('.') + 1, filePath.length) || filePath;
    }
    return selectedFileCategoryArray;
  }
  return null
}

class AbFileUploaderWidget extends React.Component {
  constructor(props) {
    super(props);
    let { value } = props;
    let fileCategory = null;

    this.proxy = new EventEmitter();
    this.state = {
      progress: -1,
      hasError: false,
      selectedFile: null,
      selectedFileCategory: null,
      signedUrlObject: null,
      previewImgUrl: null,
      hasOtherError: false,
      errorMsg: null
    };
    this.onFileSelect = this.onFileSelect.bind(this);
  }
  componentDidMount() {
    if (this.props.value) {
      let fileCategory = getFileCategory(this.props.value);
      this.setState({
        selectedFileCategory: fileCategory
      })
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value && this.props.value !== null) {
      let fileCategory = getFileCategory(nextProps.value);
      this.setState({
        selectedFileCategory: fileCategory
      })
    }
  }
  onSubmit(e) {
    if (e) {
      e.preventDefault();
    }
    if (this.state.selectedFile === null) {
      return;
    }
    let { folderPath } = this.props.options;
    this.setState({
      progress: 0,
      hasError: false
    }, () => {
      this.getSignedUrl(this.state.selectedFile, folderPath, (error, data) => {
        if (data) {
          this.setState({ signedUrlObject: data });
          this._doUpload(data.signedRequest, this.state.selectedFile);
        }
      });
    });
  }

  onFileSelect(e) {
    let { id, options } = this.props;
    let { acceptFileType, typeErrorMsg, fileSize, fileSizeErrorMsg, startUpLoadOnSelect, onFileChange } = options;
    let selectedFile = e.target.files[0];
    let isError = false;
    this.setState({
      hasOtherError: false,
      errorMsg: null
    }, () => {
      if (acceptFileType.filter(compareFileWithMimeType(selectedFile.type)).length === 0) {
        isError = true;
        this.setState({
          hasOtherError: true,
          errorMsg: (typeErrorMsg) ? typeErrorMsg : `Valid file type are ${acceptFileType.join(',')}`
        })
      } else if (fileSize) {
        let fileSizeInMb = selectedFile.size / 1048576;
        if (fileSizeInMb > fileSize) {
          isError = true;
          this.setState({
            hasOtherError: true,
            errorMsg: (fileSizeErrorMsg) ? fileSizeErrorMsg : `Upload file having less then ${fileSize} MB`
          })
        }
      }
      if (!isError) {
        onFileChange(selectedFile)
        let fileType = selectedFile.type.split('/');
        if (fileType[0] === 'application') {
          fileType[0] = selectedFile.name.substring(selectedFile.name.lastIndexOf('.') + 1, selectedFile.name.length) || selectedFile.name;
        }
        this.setState({
          selectedFile: selectedFile,
          selectedFileCategory: fileType[0],
          previewImgUrl: window.URL.createObjectURL(selectedFile)
        }, () => {
          if (fileType[0] === 'video') {
            this.refs[`video_${id}`].load();
          }
          if (startUpLoadOnSelect) {
            this.onSubmit();
          }
        });
      }
    })
  }

  getSignedUrl(file, folderPath, callback) {
    let { appbackApi } = this.props.options;
    const params = {
      key: `${folderPath}/${file.name.substr(0, file.name.lastIndexOf('.') - 1)}_${moment(Date.now()).format('YYYY-MM-DDTHH:mm:ss')}${file.name.substr(file.name.lastIndexOf('.'))}`,
      extension: file.name.substr(file.name.lastIndexOf('.')),
      contentType: file.type
    };
    appbackApi.getUploadUrl(params)
      .then(data => {
        if (data && data.signedRequest) {
          callback(null, data);
        } else {
          callback(data, null);
        }
      })
      .catch(error => {
        console.error('Error from getUploadUrl', error);
        callback(error, null);
      });
  }

  cancelUpload() {
    this.proxy.emit('abort');
    this.setState({
      progress: -1,
      hasError: false
    });
  }



  progressRenderer(progress, hasError, cancelHandler) {
    if (hasError || progress > -1) {
      if (hasError) {
        let message = (<span style={{ color: '#a94442' }}>Failed to upload ...</span>);
      }
      return (
        <Progress percent={progress} indicating={progress !== 100} progress error={(hasError) ? true : false} color='violet' success={progress === 100} />
      );
    }
    return '';
  }
  render() {
    let { previewImgUrl, hasOtherError, errorMsg, selectedFileCategory, selectedFile } = this.state;
    let { value, options, id } = this.props;
    let { inputRef, buttonRef, showProgress = true, acceptFileType } = options;
    // const formElement = this.props.formRenderer(this.onSubmit.bind(this), this.onFileSelect);
    let progressElement = null;
    if (showProgress) {
      progressElement = this.progressRenderer(this.state.progress, this.state.hasError, this.cancelUpload.bind(this));
    }


    return (
      <div className='form-control customClass'>
        <input type="file" name="file" onChange={this.onFileSelect} ref={inputRef} accept={acceptFileType.join(',')} />
        <input type="button" style={{ display: 'none' }} ref={buttonRef} onClick={this.onSubmit.bind(this)} />
        {hasOtherError && <div>
          <ul className="error-detail bs-callout bs-callout-info"><li className="text-danger">{errorMsg}</li></ul>
        </div>}
        <div id="progress-container">
          {progressElement}
        </div>
        <div>
          {((value || previewImgUrl) && selectedFileCategory === 'image') && <Image src={(value && !previewImgUrl) ? `${value}` : previewImgUrl} size='tiny' shape='rounded' />}
          {((value || previewImgUrl) && selectedFileCategory === 'video') && <video id="video1" width="200" ref={`video_${id}`}>
            <source src={(value && !previewImgUrl) ? `${value}` : previewImgUrl} type="video/mp4" />
            Your browser does not support HTML5 video.
           </video>}
          {((value || previewImgUrl) && selectedFileCategory === 'pdf') && <Image src={pdfFile} size='tiny' shape='rounded' />}
          {((value || previewImgUrl) && (selectedFileCategory === 'xlsx' || selectedFile === 'xls' || selectedFileCategory === 'csv')) && <Image src={xlsFile} size='tiny' shape='rounded' />}
          {((value || previewImgUrl) && (selectedFileCategory === 'docx' || selectedFile === 'doc')) && <Image src={docFile} size='tiny' shape='rounded' />}
          {((value || previewImgUrl) && (selectedFileCategory === 'pptx' || selectedFile === 'ppt')) && <Image src={pptFile} size='tiny' shape='rounded' />}
          {(value && !previewImgUrl) && <div><a href={`${value}`} target="_blank">Download</a></div>}
        </div>
      </div>
    );
  }

  _getFormData() {
    if (this.props.formGetter) {
      return this.props.formGetter();
    }
    return this.state.selectedFile;
  }

  _doUpload(url, fileObj) {
    const form = this._getFormData();
    const req = new XMLHttpRequest();
    req.open('PUT', url);
    req.setRequestHeader('X-Amz-ACL', 'public-read');
    req.setRequestHeader('Content-Type', fileObj.type);
    let { options } = this.props

    req.addEventListener('load', (e) => {
      this.proxy.removeAllListeners(['abort']);
      const newState = { progress: 100 };
      if (req.status >= 200 && req.status <= 299) {
        let { onChange, } = this.props;
        options.onUpload(this.state.signedUrlObject.url)
        onChange(this.state.signedUrlObject.url)
        this.setState(newState, () => {
          options.onLoad(e, req);
        });
      } else {
        newState.hasError = true;
        this.setState(newState, () => {
          options.onError(e, req);
        });
      }
    }, false);

    req.addEventListener('error', (e) => {
      this.setState({
        hasError: true
      }, () => {
        options.onError(e, req);
      });
    }, false);

    req.upload.addEventListener('progress', (e) => {
      let progress = 0;
      if (e.total !== 0) {
        progress = parseInt((e.loaded / e.total) * 100, 10);
      }
      this.setState({
        progress
      }, () => {
        options.onProgress(e, req, progress);
      });
    }, false);

    req.addEventListener('abort', (e) => {
      this.setState({
        progress: -1
      }, () => {
        options.onAbort(e, req);
      });
    }, false);

    this.proxy.once('abort', () => {
      req.abort();
    });

    this.props.beforeSend(req)
      .send(form);
  }
}

AbFileUploaderWidget.propTypes = {
  url: PropTypes.string,
  formGetter: PropTypes.func,
  formRenderer: PropTypes.func,
  progressRenderer: PropTypes.func,
  formCustomizer: PropTypes.func,
  beforeSend: PropTypes.func,
  onProgress: PropTypes.func,
  onLoad: PropTypes.func,
  onError: PropTypes.func,
  onAbort: PropTypes.func,
  onChange: PropTypes.func
};

AbFileUploaderWidget.defaultProps = {
  formRenderer: (onSubmit, onFileSelect) => (
    <form className="_react_fileupload_form_content" ref="form" method="post" onSubmit={onSubmit}>
      <div>
        <input type="file" name="file" onChange={onFileSelect} ref={ref => this.inputRef = ref} />
      </div>
      <input type="submit" />
    </form>
  ),
  formCustomizer: (form) => form,
  beforeSend: (request) => request,
  options: {
    onProgress: (e, request, progress) => { },
    onLoad: (e, request) => { },
    onError: (e, request) => { },
    onAbort: (e, request) => { },
    onUpload: (url) => { },
    onFileChange: (file) => { },
    folderPath: 'other',
    acceptFileType: ['image/*'],
    fileSize: 2,
    startUpLoadOnSelect: true
  }
};

export default AbFileUploaderWidget;
