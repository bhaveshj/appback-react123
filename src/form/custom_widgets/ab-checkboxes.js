import React, { PropTypes } from "react";
import { Checkbox } from 'semantic-ui-react';
import { ABCHECKBOXTYPE } from '../../constants';

function selectValue(value, selected, all) {
    const at = all.indexOf(value);
    const updated = selected.slice(0, at).concat(value, selected.slice(at));
    // As inserting values at predefined index positions doesn't work with empty
    // arrays, we need to reorder the updated selection to match the initial order
    return updated.sort((a, b) => all.indexOf(a) > all.indexOf(b));
}

function deselectValue(value, selected) {
    return selected.filter(v => v !== value);
}

function AbCheckboxesWidget(props) {
    const { id, disabled, options, value, autofocus, onChange } = props;
    const { enumOptions, inline, checkboxType, radio = false } = options;
    return (
        <div className={(inline) ? 'fields inline' : 'fields grouped'} id={id}>{
            enumOptions.map((option, index) => {
                const checked = value.indexOf(option.value) !== -1;
                const checkbox = (
                    <div key={index} className={"field"}>
                        <Checkbox
                            id={`${id}_${index}`}
                            checked={checked}
                            radio={radio}
                            slider={(ABCHECKBOXTYPE.SLIDER === checkboxType)}
                            toggle={(ABCHECKBOXTYPE.TOGGLE === checkboxType)}
                            label={<label>{option.label}</label>}
                            disabled={disabled}
                            autoFocus={autofocus && index === 0}
                            onChange={(event, data) => {
                                const all = enumOptions.map(({ value }) => value);
                                if (data.checked) {
                                    onChange(selectValue(option.value, value, all));
                                } else {
                                    onChange(deselectValue(option.value, value));
                                }
                            }} />
                    </div>
                );
                return checkbox
            })
        }</div>
    );
}

AbCheckboxesWidget.defaultProps = {
    autofocus: false,
    options: {
        inline: false
    },
};

AbCheckboxesWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    options: PropTypes.shape({
        enumOptions: PropTypes.array,
        inline: PropTypes.bool,
    }).isRequired,
    value: PropTypes.any,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    multiple: PropTypes.bool,
    autofocus: PropTypes.bool,
    onChange: PropTypes.func,
};


export default AbCheckboxesWidget;
