import React, { PropTypes } from "react";
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import _ from 'lodash';
import { ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_UTILS } from '../../constants';

class AbSyncSelectWidget extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "showLoader": false,
            "results": []
        }
        this.customFilterOption = this.customFilterOption.bind(this)
    }

    componentWillMount() {
        this.loadOptions(this.props);
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEqual(this.props.options, nextProps.options)) {
            this.loadOptions(nextProps);
        }
    }
    customFilterOption(option, filter) {
        const { filterOnFields } = this.props.options
        let resultValue = false;
        if (filter.toString().length > 0) {
            _.map(filterOnFields, function (fieldName) {
                if (option[fieldName] && option[fieldName].toString().toLowerCase().includes(filter)) {
                    resultValue = true;
                }
            })
            return resultValue;
        } else {
            resultValue = true;
            return resultValue;
        }
    }

    loadOptions(props) {
        let self = this;
        self.setState({
            showLoader: true
        })
        const { AbModelInstance, getDataFromServer, options } = props;
        const { customFilter, labelKey, matchInputFrom, dataSourceType, dataSourceOptions } = options;
        let filter = {};
        if (customFilter) {
            filter = { ...customFilter }
        }
        if (dataSourceType && dataSourceOptions && dataSourceType === GRID_DATA_SOURCE_TYPE.METHOD) {
            const { methodName, data, params, options } = dataSourceOptions;
            let finalParams = { "filter": filter };
            if (params) {
                finalParams = { ...finalParams, ...params };
            }
            AbModelInstance.custom(methodName, data, finalParams, options)
                .then((response) => {
                    if (response) {
                        if (response instanceof Array) {
                            self.setState({
                                results: response,
                                showLoader: false
                            })
                        } else {
                            if (response instanceof Object && response.data) {
                                self.setState({
                                    results: response.data,
                                    showLoader: false
                                })
                            } else {
                                self.setState({
                                    showLoader: false
                                })
                            }
                        }
                    }

                }, (error) => {
                    console.log("error", error);
                    // errorCallback(error);
                    // return { result: "fail", data: error };
                });
        } else {
            getDataFromServer({ AbModelInstance, filter }).then((response) => {
                if (response) {
                    self.setState({
                        results: response,
                        showLoader: false
                    })
                }
                // return { options: response };
            }, (error) => {
                console.log("error", error);
                // return { options: [] };
            });
        }
    }


    render() {
        let self = this;
        const { id, value, onChange, disabled, options, required } = this.props;
        const { showLoader, results } = this.state;
        const { backspaceRemoves, labelKey, multi, valueKey, searchable, clearable, noResultsText, selectPlaceholder, searchPromptText, autofocus, customFilter, optionRenderer, valueRenderer, filterOnFields, creatable, delimiter } = options;
        const valueChanged = (formData) => {
            const sendData = (formData == null || formData.toString().length === 0) ? undefined : formData
            onChange(sendData)
        }
        if (filterOnFields && filterOnFields.length > 0) {
            if (creatable && multi) {
                return (
                    <div style={required && !searchable ? { height: '36px' } : {}}>
                        <Select.Creatable key={id} multi={multi} options={results} simpleValue clearable={clearable} disabled={disabled} valueKey={valueKey} labelKey={labelKey} value={value} onChange={valueChanged} searchable={searchable} noResultsText={noResultsText} placeholder={selectPlaceholder} searchPromptText={searchPromptText} autofocus={autofocus} required={required} isLoading={showLoader} optionRenderer={optionRenderer} valueRenderer={valueRenderer} filterOption={self.customFilterOption} delimiter={delimiter} />
                        {required && !searchable && <input tabIndex={-1} value={value} required={true} style={{ width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' }} />}
                    </div>
                );
            } else {
                return (
                    <div style={required && !searchable ? { height: '36px' } : {}}>
                        <Select key={id} multi={multi} options={results} simpleValue clearable={clearable} disabled={disabled} valueKey={valueKey} labelKey={labelKey} value={value} onChange={valueChanged} searchable={searchable} noResultsText={noResultsText} placeholder={selectPlaceholder} searchPromptText={searchPromptText} autofocus={autofocus} required={required} isLoading={showLoader} optionRenderer={optionRenderer} valueRenderer={valueRenderer} filterOption={self.customFilterOption} delimiter={delimiter} />
                        {required && !searchable && <input tabIndex={-1} value={value} required={true} style={{ width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' }} />}
                    </div>
                );
            }
        } else {
            if (creatable && multi) {
                return (
                    <div style={required && !searchable ? { height: '36px' } : {}}>
                        <Select.Creatable key={id} multi={multi} options={results} simpleValue clearable={clearable} disabled={disabled} valueKey={valueKey} labelKey={labelKey} value={value} onChange={valueChanged} searchable={searchable} noResultsText={noResultsText} placeholder={selectPlaceholder} searchPromptText={searchPromptText} autofocus={autofocus} required={required} isLoading={showLoader} optionRenderer={optionRenderer} valueRenderer={valueRenderer} delimiter={delimiter} />
                        {required && !searchable && <input tabIndex={-1} value={value} required={true} style={{ width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' }} />}
                    </div>
                );
            } else {
                return (
                    <div style={required && !searchable ? { height: '36px' } : {}}>
                        <Select key={id} multi={multi} options={results} simpleValue clearable={clearable} disabled={disabled} valueKey={valueKey} labelKey={labelKey} value={value} onChange={valueChanged} searchable={searchable} noResultsText={noResultsText} placeholder={selectPlaceholder} searchPromptText={searchPromptText} autofocus={autofocus} required={required} isLoading={showLoader} optionRenderer={optionRenderer} valueRenderer={valueRenderer} delimiter={delimiter} />
                        {required && !searchable && <input tabIndex={-1} value={value} required={true} style={{ width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' }} />}
                    </div>
                );
            }
        }
    }
}



AbSyncSelectWidget.defaultProps = {
    options: {
        multi: false,
        clearable: true,
        searchable: false,
        backspaceRemoves: true,
        noResultsText: "No results found",
        selectPlaceholder: "Select ...",
        searchPromptText: "Type to search",
        autofocus: false,
        customFilter: {},
        optionRenderer: null,
        valueRenderer: null,
        creatable: false,
        delimiter: FORM_UTILS.DELIMITER
    },
};

AbSyncSelectWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.any,
    required: PropTypes.bool,
    autofocus: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    options: PropTypes.shape({
        labelKey: PropTypes.string.required,
        valueKey: PropTypes.string.required,
        multi: PropTypes.bool,
        clearable: PropTypes.bool,
        searchable: PropTypes.bool,
        backspaceRemoves: PropTypes.bool,
        noResultsText: PropTypes.string,
        selectPlaceholder: PropTypes.string,
        searchPromptText: PropTypes.string,
        autofocus: PropTypes.bool,
        customFilter: PropTypes.object,
        optionRenderer: PropTypes.func,
        valueRenderer: PropTypes.func,
        filterOnFields: PropTypes.array,
        creatable: PropTypes.bool,
        delimiter: PropTypes.string
    }).isRequired,
};
export default AbSyncSelectWidget;
