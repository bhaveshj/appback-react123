
import React, { PropTypes } from "react";
import { Checkbox } from 'semantic-ui-react';
import { ABCHECKBOXTYPE } from '../../constants';
import Datetime from 'react-datetime';
import './css/ab-datetimpicker.css';
import moment from 'moment';


const AbDateTimePickerWidget = (props) => {
    const { id, value, onChange, disabled, options, readonly, placeholder, required } = props;
    const { targetTimeZone = null, displayDateFormat = "MMM DD YYYY", displayTimeFormat = "hh:mm A", className, minDate = null, maxDate = null, clearable } = options;
    let defaultFormat = null; // = "MMM DD YYYY hh:mm A";
    if (displayDateFormat !== false) {
        defaultFormat = displayDateFormat;
    }
    if (displayTimeFormat !== false) {
        if (defaultFormat !== null) {
            defaultFormat = defaultFormat + " " + displayTimeFormat
        } else {
            defaultFormat = displayTimeFormat;
        }
    }
    var finalDate;
    if (value) {
        var dateValue = moment(value);
        var parsedDate;
        if (targetTimeZone) {
            finalDate = moment(value).utcOffset(targetTimeZone).format(defaultFormat);
        } else {
            finalDate = dateValue.format(defaultFormat);

        }
    }
    const parseDate = (dateMomentObj) => {
        let finalDateObj = _.cloneDeep(dateMomentObj);
        if (!dateMomentObj) {
            onChange(undefined)
            return
        }
        if (typeof dateMomentObj === 'string' || dateMomentObj instanceof String) {
            if (moment(dateMomentObj, defaultFormat, true).isValid()) {
                finalDateObj = moment(dateMomentObj, defaultFormat);
            } else {
                return false
            }
        }
        if (targetTimeZone) {
            onChange(moment(finalDateObj.utcOffset(targetTimeZone, true)).toString());
        } else {
            onChange(moment(finalDateObj).toString());
        }
    }

    const valid = function (current) {
        let flag = 1
        if (minDate && current < moment(minDate).startOf('day')) {
            flag = 0
        }
        if (maxDate && current > maxDate) {
            flag = 0
        }
        return flag
    };
    const onBlur = (dateMomentObj) => {
        if (typeof dateMomentObj === 'string' || dateMomentObj instanceof String) {
            if (!moment(dateMomentObj, defaultFormat, true).isValid()) {
                onChange(dateMomentObj)
            }
        }
    }
    return (
        <Datetime value={finalDate} onChange={parseDate} onBlur={onBlur} isValidDate={valid} input={true} clearable={clearable} dateFormat={displayDateFormat} timeFormat={displayTimeFormat} className={className} inputProps={{ required: required, readOnly: readonly, disabled: disabled, className: "form-control", placeholder }} />
    );
};

AbDateTimePickerWidget.defaultProps = {
};

AbDateTimePickerWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    options: PropTypes.shape({
        targetTimeZone: PropTypes.string,
        displayDateFormat: PropTypes.any,
        displayTimeFormat: PropTypes.any,
        minDate: PropTypes.Date,
        maxDate: PropTypes.Date
    }).isRequired,
};

export default AbDateTimePickerWidget;