import React, { PropTypes } from "react";
import { Input } from 'semantic-ui-react';
import Cleave from 'cleave.js/react'
import './ab-maskedinput.css'

export class AbMaskedInputWidget extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { id, value, onChange, disabled, placeholder, options, readonly, required } = this.props
        const type = this.props.schema.type
        const { icon = null, iconPosition = 'left', cleaveOptions } = options

        const initCleave = (cleave) => {
            cleave.setRawValue(value)
        }

        return (
            <div className={`ui ${icon ? iconPosition : ''} icon input`}>
                <i aria-hidden={true} className={`${icon} icon`} style={{ zIndex: 9999 }}></i>
                {_.isEmpty(cleaveOptions) ?
                    <Input className={icon && iconPosition == "left" ? "masked-input" : ""} value={value} id={id} required={required} readOnly={readonly} disabled={disabled}
                        type={type == 'string' ? 'text' : type} placeholder={placeholder} onChange={(event, data) => onChange(data.value)} />
                    : <Cleave className={!_.isEmpty(icon) && iconPosition == "left" ? "cleave-input" : ""} value={value} id={id} required={required} readOnly={readonly} disabled={disabled}
                        options={cleaveOptions} onChange={(e) => onChange(e.target.rawValue)} placeholder={placeholder} onInit={initCleave} />
                }
            </div>
        );
    };
}

AbMaskedInputWidget.defaultProps = {

};

AbMaskedInputWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.any,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    placeholder: PropTypes.string
};

export default AbMaskedInputWidget;