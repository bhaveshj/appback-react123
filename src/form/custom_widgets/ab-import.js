import React, { PropTypes } from "react";
import _ from 'lodash';
import { FORM_UTILS } from '../../constants';

const AbImportWidget = (props) => {
    const { importUrl, successCallback, errorCallback } = props;
    const processFile = (files) => {
        const f = files[0];
        return new Promise((resolve, reject) => {
            // const reader = new FileReader();
            // reader.onload = (event) => {
            //     let dataURL = event.target.result;
            //     dataURL = dataURL.replace(";base64", ";name=" + f.name + ";base64");
            //     resolve(dataURL);
            // }
            // reader.readAsDataURL(f);
            f
        });
    }
    const importFile = (files) => {
        const { importFile, successCallback, errorCallback } = props;
        const file = files[0];
        importFile({ file, importUrl, successCallback, errorCallback })
    }
    return (
        <input type="file"
            accept="text/csv"
            onChange={(event) => importFile(event.target.files)} />
    );
};

AbImportWidget.propTypes = {
    importUrl: PropTypes.string.isRequired,
    successCallback: PropTypes.func,
    errorCallback: PropTypes.func
};
export default AbImportWidget;
