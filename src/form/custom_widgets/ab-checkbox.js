import React, { PropTypes } from "react";
import { Checkbox } from 'semantic-ui-react';
import { ABCHECKBOXTYPE } from '../../constants';


const AbCheckboxWidget = (props) => {
    const { id, value, onChange, disabled, options } = props;
    const { label, checkboxType, radio = false } = options;
    return (
        <Checkbox
            slider={(ABCHECKBOXTYPE.SLIDER === checkboxType)}
            toggle={(ABCHECKBOXTYPE.TOGGLE === checkboxType)}
            id={props.id}
            label={<label>{label}</label>}
            disabled={disabled}
            checked={value}
            radio={radio}
            onChange={
                (event, data) => {
                    onChange(data.checked)
                }} />

    );
};

AbCheckboxWidget.defaultProps = {
    checkboxType: ""
};

AbCheckboxWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.bool,
    required: PropTypes.bool,
    autofocus: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    checkboxType: PropTypes.string
};

export default AbCheckboxWidget;