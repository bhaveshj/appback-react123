import React, { PropTypes } from "react";
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_UTILS } from '../../constants';

const AbSelectWidgetComponent = (props) => {
    const {id, value, onChange, disabled, options, required, loadOptions } = props;
    const { autoload, backspaceRemoves, labelKey, multi, valueKey, async, selectOptions, searchable, clearable, noResultsText, selectPlaceholder, searchPromptText, autofocus, optionRenderer, valueRenderer, cache, delimiter } = options;
    const valueChanged = (formData) => {
        const sendData = (formData == null || formData.toString().length === 0) ? undefined : formData
        onChange(sendData)
    }
    return (
        <div style={required && !searchable ? {height: '36px'} : {}}>
            <Select.Async multi={multi} simpleValue value={value} clearable={clearable} disabled={disabled} onChange={valueChanged} valueKey={valueKey} labelKey={labelKey} loadOptions={loadOptions} backspaceRemoves={backspaceRemoves} autoload={autoload} searchable={searchable} noResultsText={noResultsText} placeholder={selectPlaceholder} searchPromptText={searchPromptText} autofocus={autofocus} required={required} optionRenderer={optionRenderer} valueRenderer={valueRenderer} cache={cache} delimiter={delimiter}/>
            {required && !searchable && <input tabIndex={-1} value={value} required={true} style={{width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px'}} />}
        </div>
    );


};

class AbAsyncSelectWidget extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "initialDataLoaded": false,
            "results": []
        }
    }


    loadOptions(input) {
        const {AbModelInstance, getDataFromServer, options } = this.props;
        const {customFilter, labelKey, matchInputFrom, filterOnFields, dataSourceType, dataSourceOptions} = options;
        let filter = {};
        if (filterOnFields && filterOnFields.length > 0) {
            let filterList = _.map(filterOnFields, function (filterCol) {
                if (matchInputFrom === ABSELECT.START) {
                    return { [filterCol]: { "regexp": "/^" + input + "/i" } };
                } else {
                    return { [filterCol]: { "regexp": "/" + input + "/i" } };
                }
            });

            let where = {
                "or": filterList
            }
            filter = where.or.length ? where : {};
        } else {
            if (matchInputFrom === ABSELECT.START) {
                filter = { [labelKey]: { "regexp": "/^" + input + "/i" } };
            } else {
                filter = { [labelKey]: { "regexp": "/" + input + "/i" } };
            }
        }
        if (customFilter) {
            filter = { ...filter, ...customFilter }
        }
        if (dataSourceType && dataSourceOptions && dataSourceType === GRID_DATA_SOURCE_TYPE.METHOD) {
            const { methodName, data, options} = dataSourceOptions;
            return AbModelInstance.custom(methodName, data, { "filter": filter }, options)
                .then((response) => {
                    if (response) {
                        if (response instanceof Array) {
                            return { options: response };
                        } else {
                            if (response instanceof Object && response.data) {
                                return { options: response.data };
                            } else {
                                return { options: [] };
                            }
                        }
                    } else {
                        return { options: [] };
                    }
                }, (error) => {
                    console.log("error", error);
                    return { options: [] };
                });
        } else {
            return getDataFromServer({ AbModelInstance, filter }).then((response) => {
                return { options: response };
            }, (error) => {
                console.log("error", error);
                return { options: [] };
            });
        }
    }



    render() {
        let selectProps = { ...this.props };
        const { options } = this.props;
        const { async } = options;
        selectProps["loadOptions"] = (input, callback) => this.loadOptions(input, callback);
        return (
            <AbSelectWidgetComponent {...selectProps} />
        )
    }
}


AbAsyncSelectWidget.defaultProps = {
    options: {
        multi: false,
        clearable: true,
        searchable: false,
        autoload: true,
        backspaceRemoves: true,
        async: false,
        noResultsText: "No results found",
        selectPlaceholder: "Select ...",
        searchPromptText: "Type to search",
        autofocus: false,
        customFilter: {},
        matchInputFrom: ABSELECT.ANY,
        optionRenderer: null,
        valueRenderer: null,
        filterOnFields: [],
        cache: true,
        delimiter:FORM_UTILS.DELIMITER
    },
};

AbAsyncSelectWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.string,
    required: PropTypes.bool,
    autofocus: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    options: PropTypes.shape({
        selectOptions: PropTypes.array.required,
        labelKey: PropTypes.string.required,
        valueKey: PropTypes.string.required,
        multi: PropTypes.bool,
        clearable: PropTypes.bool,
        searchable: PropTypes.bool,
        autoload: PropTypes.bool,
        backspaceRemoves: PropTypes.bool,
        async: PropTypes.bool,
        noResultsText: PropTypes.string,
        selectPlaceholder: PropTypes.string,
        searchPromptText: PropTypes.string,
        autofocus: PropTypes.bool,
        appbackApi: PropTypes.object.required,
        optionModel: PropTypes.string.required,
        customFilter: PropTypes.object,
        matchInputFrom: PropTypes.string,
        optionRenderer: PropTypes.func,
        valueRenderer: PropTypes.func,
        filterOnFields: PropTypes.array,
        cache: PropTypes.bool,
        delimiter:PropTypes.string
    }).isRequired,
};
export default AbAsyncSelectWidget;
