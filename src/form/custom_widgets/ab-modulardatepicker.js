import React, { Component, PropTypes } from "react";
import './css/ab-modulardatepicker.css';
// import PropTypes from "prop-types";

// import { shouldRender, parseDateString, toDateString, pad } from "../../utils";

function shouldRender(comp, nextProps, nextState) {
    const { props, state } = comp;
    return !deepEquals(props, nextProps) || !deepEquals(state, nextState);
}

function parseDateString(dateString, includeTime = true, showDay, showMonth, showYear) {
    if (!dateString) {
        return {
            ...showYear && { year: -1 },
            ...showMonth && { month: -1 },
            ...showDay && { day: -1 },
            hour: includeTime ? -1 : 0,
            minute: includeTime ? -1 : 0,
            second: includeTime ? -1 : 0
        };
    }
    const date = new Date(dateString);
    if (Number.isNaN(date.getTime())) {
        throw new Error("Unable to parse date " + dateString);
    }
    return {
        ...showYear && { year: date.getUTCFullYear() },
        ...showMonth && { month: date.getUTCMonth() + 1 }, // oh you, javascript.
        ...showDay && { day: date.getUTCDate() },
        hour: includeTime ? date.getUTCHours() : 0,
        minute: includeTime ? date.getUTCMinutes() : 0,
        second: includeTime ? date.getUTCSeconds() : 0
    };
}


function toDateString(
    {
        year = 1972,
        month = 1,
        day = 0,
        hour = 0,
        minute = 0,
        second = 0
    },
    time = true
) {
    const utcTime = Date.UTC(year, month - 1, day, hour, minute, second);
    const datetime = new Date(utcTime).toJSON();
    return time ? datetime : datetime.slice(0, 10);
}


function pad(num, size) {
    let s = String(num);
    while (s.length < size) {
        s = "0" + s;
    }
    return s;
}

function deepEquals(a, b, ca = [], cb = []) {
    // Partially extracted from node-deeper and adapted to exclude comparison
    // checks for functions.
    // https://github.com/othiym23/node-deeper
    if (a === b) {
        return true;
    } else if (typeof a === "function" || typeof b === "function") {
        // Assume all functions are equivalent
        // see https://github.com/mozilla-services/react-jsonschema-form/issues/255
        return true;
    } else if (typeof a !== "object" || typeof b !== "object") {
        return false;
    } else if (a === null || b === null) {
        return false;
    } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime();
    } else if (a instanceof RegExp && b instanceof RegExp) {
        return a.source === b.source &&
            a.global === b.global &&
            a.multiline === b.multiline &&
            a.lastIndex === b.lastIndex &&
            a.ignoreCase === b.ignoreCase;
    } else if (isArguments(a) || isArguments(b)) {
        if (!(isArguments(a) && isArguments(b))) {
            return false;
        }
        let slice = Array.prototype.slice;
        return deepEquals(slice.call(a), slice.call(b), ca, cb);
    } else {
        if (a.constructor !== b.constructor) {
            return false;
        }

        let ka = Object.keys(a);
        let kb = Object.keys(b);
        // don't bother with stack acrobatics if there's nothing there
        if (ka.length === 0 && kb.length === 0) {
            return true;
        }
        if (ka.length !== kb.length) {
            return false;
        }

        let cal = ca.length;
        while (cal--) {
            if (ca[cal] === a) {
                return cb[cal] === b;
            }
        }
        ca.push(a);
        cb.push(b);

        ka.sort();
        kb.sort();
        for (var j = ka.length - 1; j >= 0; j--) {
            if (ka[j] !== kb[j]) {
                return false;
            }
        }

        let key;
        for (let k = ka.length - 1; k >= 0; k--) {
            key = ka[k];
            if (!deepEquals(a[key], b[key], ca, cb)) {
                return false;
            }
        }

        ca.pop();
        cb.pop();

        return true;
    }
}

function isArguments(object) {
    return Object.prototype.toString.call(object) === "[object Arguments]";
}

function rangeOptions(start, stop) {
    let options = [];
    for (let i = start; i <= stop; i++) {
        options.push({ value: i, label: pad(i, 2) });
    }
    return options;
}

function readyForChange(state) {
    return Object.keys(state).every(key => state[key] !== -1);
}

function DateElement(props) {
    const {
    type,
        range,
        value,
        select,
        rootId,
        disabled,
        readonly,
        autofocus,
        registry,
        onBlur,
  } = props;
    const id = rootId + "_" + type;
    const { SelectWidget } = registry.widgets;
    return (
        <SelectWidget
            schema={{ type: "integer" }}
            id={id}
            className="form-control"
            options={{ enumOptions: rangeOptions(range[0], range[1]) }}
            placeholder={type}
            value={value}
            disabled={disabled}
            readonly={readonly}
            autofocus={autofocus}
            onChange={value => select(type, value)}
            onBlur={onBlur}
        />
    );
}

class AbModularDatePickerWidget extends Component {
    static defaultProps = {
        time: false,
        disabled: false,
        readonly: false,
        autofocus: false,
    };

    constructor(props) {
        super(props);
        const { showDay = true, showMonth = true, showYear = true } = props.options;
        this.state = parseDateString(props.value, props.time, showDay, showMonth, showYear);
        this.dateElementProps = this.dateElementProps.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { showDay = true, showMonth = true, showYear = true } = nextProps.options;
        this.setState(parseDateString(nextProps.value, nextProps.time, showDay, showMonth, showYear));
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shouldRender(this, nextProps, nextState);
    }

    onChange = (property, value) => {
        this.setState(
            { [property]: typeof value === "undefined" ? -1 : value },
            () => {
                // Only propagate to parent state if we have a complete date{time}
                if (readyForChange(this.state)) {
                    this.props.onChange(toDateString(this.state, this.props.time));
                }
            }
        );
    };

    setNow = event => {
        event.preventDefault();
        const { time, disabled, readonly, onChange, options } = this.props;
        const { showDay = true, showMonth = true, showYear = true } = options;
        if (disabled || readonly) {
            return;
        }
        const nowDateObj = parseDateString(new Date().toJSON(), time, showDay, showMonth, showYear);
        this.setState(nowDateObj, () => onChange(toDateString(this.state, time)));
    };

    clear = event => {
        event.preventDefault();
        const { time, disabled, readonly, onChange, options } = this.props;
        const { showDay = true, showMonth = true, showYear = true } = options;

        if (disabled || readonly) {
            return;
        }
        this.setState(parseDateString("", time, showDay, showMonth, showYear), () => onChange(undefined));
    };

    dateElementProps() {
        const { time } = this.props;
        const { year, month, day, hour, minute, second } = this.state;
        const data = [
            { type: "year", range: [1900, 2020], value: year },
            { type: "month", range: [1, 12], value: month },
            { type: "day", range: [1, 31], value: day },
        ];
        if (time) {
            data.push(
                { type: "hour", range: [0, 23], value: hour },
                { type: "minute", range: [0, 59], value: minute },
                { type: "second", range: [0, 59], value: second }
            );
        }
        return data;
    }

    render() {
        const { id, disabled, readonly, autofocus, registry, onBlur, options } = this.props;
        const { showDay = true, showMonth = true, showYear = true } = options;
        const data = this.dateElementProps();
        return (
            <ul className="list-inline">
                {data.map((elemProps, i) => {
                    return (
                        ((elemProps.type === "day" && showDay) || (elemProps.type === "month" && showMonth) || (elemProps.type === "year" && showYear)) ?
                            <li key={i}>
                                <DateElement
                                    rootId={id}
                                    select={this.onChange}
                                    {...elemProps}
                                    disabled={disabled}
                                    readonly={readonly}
                                    registry={registry}
                                    onBlur={onBlur}
                                    autofocus={autofocus && i === 0}
                                />
                            </li>
                            : null
                    )
                })}
                <li>
                    <a
                        href="#"
                        className="btn btn-warning btn-clear"
                        onClick={this.clear}>
                        Clear
                    </a>
                </li>
            </ul>
        );
    }
}

// if (process.env.NODE_ENV !== "production") {
AbModularDatePickerWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.string,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    readonly: PropTypes.bool,
    autofocus: PropTypes.bool,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    time: PropTypes.bool,
    showDay: PropTypes.bool,
    showMonth: PropTypes.bool,
    showYear: PropTypes.bool,
};
// }

export default AbModularDatePickerWidget;