import React, { Component } from 'react';
import { connect } from 'react-redux';
import AbModelForm from '../components/form-component';
import * as AbModelFormAction from '../../actions';



const mapStateToProps = (state, ownProps) => {
  const { appbackApi, modelName} = ownProps;
  var mapped = {
    abModelData: state.abModels[modelName],
    formData: (state.abModels[modelName]) ? state.abModels[modelName].form.currentActiveItem : {},
    formAction: (state.abModels[modelName]) ? state.abModels[modelName].form.action : null,
    filterCols: (state.abModels[modelName]) ? state.abModels[modelName].filterCols : [],
    includes: (state.abModels[modelName]) ? state.abModels[modelName].includes : [],
    AbModelInstance: appbackApi.createModelRest(modelName, null)
  };
  return mapped;
}
function mapDispatchToProps(dispatch, ownProps) {
  dispatch(AbModelFormAction.setModelInitialData(ownProps.modelName));
  return {
    saveModelFormData: function (payloadData) {
      const { modelName, errorCallback} = ownProps;
      payloadData["AbModelName"] = modelName;
      payloadData["errorCallback"] = errorCallback;
      return dispatch(AbModelFormAction.saveModelFormData(payloadData));
    },
    createModelFormData: function (payloadData) {
      const { modelName, errorCallback} = ownProps;
      payloadData["AbModelName"] = ownProps.modelName;
      payloadData["errorCallback"] = errorCallback;
      return dispatch(AbModelFormAction.createModelFormData(payloadData));
    },
    clearFormData: function () {
      dispatch(AbModelFormAction.clearFormData({ modelName: ownProps.modelName }));
    },
    getPreLoadFormData: function(loadData){
      const {filter, AbModelInstance,saveCallback,errorCallback} = loadData;
      dispatch(AbModelFormAction.getPreLoadFormData({ AbModelName: ownProps.modelName,...{...loadData}}));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(AbModelForm);