Appback-React
=========================

Appback-React provide ready to use components (Grid, Form, and other small components)

**To Run Code**

* clone repo
* npm install at root
* cd example/simple
* npm install
* Start mongodb and appback before running appback-react
* npm start 


Appback-React has two major component support

1. **AbModelGrid**
- It is wrapper upon [Griddle](https://griddlegriddle.github.io/v0-docs/)

2. **AbModelForm**
- It is wrapper upon [react-jsonschema-form](https://github.com/mozilla-services/react-jsonschema-form)

Other component support

1. **AbSelectWidget** 
2. **AbSyncSelectWidget**
3. **AbAsyncSelectWidget**
- All of above three components are wrapper upon [react-select](https://github.com/JedWatson/react-select)

4. **AbDateTimePickerWidget**
- It is wrapper upon [react-datetime](https://github.com/YouCanBookMe/react-datetime)

5. **AbInputWidget**
6. **AbRadioWidget**
7. **AbCheckboxWidget**
8. **AbCheckboxesWidget**

- All above four components are wrapper upon equivalent component of [semantic-ui-react](https://react.semantic-ui.com/)


An opinionated setup I plan to use for my libraries.

It has CommonJS and UMD builds via Babel and Webpack, ESLint, and Mocha.  
It also has React-friendly examples folder with library code mapped to the sources.

If you use this, make sure to grep for “library-boilerplate” and replace every occurrence.
See `package.json` in the root and the example folder for the list of the available commands.

Note that this is an *opinionated* boilerplate. You might want to:

* Set `stage` to `2` in `.babelrc` so you don’t depend on language features that might be gone tomorrow;
* Remove `loose: ["all"]` from `.babelrc` so the behavior is spec-compliant.

You have been warned.

* Removed from package.json 
"build:umd": "webpack src/index.js dist/appback-react.js && NODE_ENV=production webpack src/index.js dist/appback-react.min.js",
"lint": "eslint src test examples",
"test": "NODE_ENV=test mocha",
"test:watch": "NODE_ENV=test mocha --watch",
"test:cov": "babel-node ./node_modules/.bin/isparta cover ./node_modules/.bin/_mocha",
"prepublish": "npm run clean && npm run build && npm run build:umd"